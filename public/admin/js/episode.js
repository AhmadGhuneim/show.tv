const vars = {
    program_id:'.program_id',
    broadcast_day_id:'.broadcast_day_id'
};
const ajaxOperations = function(){
    function setBroadCastDaysTemplate(broadCastDays) {
        let options = '';
        for(let day in broadCastDays){
            options +=`<option value="${day}">${broadCastDays[day]}</option>`
        }
        return options;
    }
  function getProgramList(id) {
      $.ajax({
          url:'/showTvAdmin/programs/getProgramBroadcastTimes/'+id,
          type:'GET',
          success:function (res) {
              if(res.valid){
                 $(vars.broadcast_day_id).html(setBroadCastDaysTemplate(res.broadCastDays));
              }
          },
          error:function (exception) {
              console.log(exception)
          }
      })
  }
  return {
      getProgramList
  }
};
$(document).on('change',vars.program_id,function () {
   let getList = new ajaxOperations();
   getList.getProgramList($(this).val());
});
