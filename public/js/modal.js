const modalConstants = {
    gloablModal:'#global-modal',
    modalBody:'.modal-body',
    callGlobalModal:'.call-global-modal',
    submitFormAjax:'.submitFormAjax',
    main_error_message_in_form:'.main_error_message_in_form',
    errorFormMessages:'.error-form-messages',
};

const ModalOperations = function(){
    function loadUrl(){
        $(modalConstants.gloablModal+' '+modalConstants.modalBody).empty();
        $(modalConstants.gloablModal).modal('show');
        if(this.url) {
            $.ajax({
                url:this.url,
                dataType:'JSON',
               success:function (res) {
                   $(modalConstants.gloablModal + ' ' + modalConstants.modalBody)
                       .html(res.view);
               },
                error:function (exception) {
                    console.log(exception);
                }
            });

        }
    }
    return {
        url:'',
        loadUrl
    }
};

$(document).on('click',modalConstants.callGlobalModal,function (e) {
    e.preventDefault();

    const modalOperation = new ModalOperations();
    modalOperation.url = $(this).data('link')?$(this).data('link'):$(this).attr('href');
    modalOperation.loadUrl();

});
$(document).on('submit',modalConstants.submitFormAjax,function (e) {
   e.preventDefault();

    const ajaxOperation = new AjaxOperations();
    ajaxOperation.form = $(this);
    ajaxOperation.submitModal();

});
