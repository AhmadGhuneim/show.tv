const vars = {
    slick_container_hover_effect:'.slick_container_hover_effect',
    paragraph_hidden_slick:'.paragraph_hidden_slick',
    slick_main_title:'.slick_main_title',
    btn_change_inner_html:'.btn_change_inner_html',
    go_to_link:'.go_to_link',
};
const mediaObject = function(){
    function PlayNotificationAudio(){
        if (typeof Audio === 'function') {
            const audio = new Audio($('#toastrAudio').val());
            audio.play();
        }
    }
    return {
        PlayNotificationAudio
    }
};

const AjaxOperations = function(){
    function submitModal(){
        const form = this.form;
        form.ajaxSubmit({
            beforeSend:function(){
                $(modalConstants.errorFormMessages).empty();
                $(modalConstants.main_error_message_in_form).empty();
            },
            dataType:'JSON',
            accept:'application/json',
            success:function (res) {
                location.reload();
            },
            error:function (exception) {
                appendErrors(exception.responseJSON,form);
            }
        })
    }
    function updateAttrs(){
        const url = this.url;
        const el = this.el;
        $.ajax({
            url:url,
            data:this.data,
            dataType:'JSON',
            accept:'application/json',
            success:function (res) {
                if(!res.valid) {
                    if (res.link) {
                        const modalOperation = new ModalOperations();
                        modalOperation.url = res.link;
                        modalOperation.loadUrl();
                    }
                    return;
                }
                updateTemplate(el,res.attrs);
                changeOtherClasses(el);
            },
            error:function (exception) {
            }
        })
    }
    function changeOtherClasses(el) {
        if(el.data('remove-class')) {
            el.parent()
                .find('.' + el.data('remove-class'))
                .removeClass(el.data('remove-class'))
                .addClass(el.data('add-class'));
        }
    }
    function updateTemplateChangeAttrs(el,change,src,return_after_first_success=false) {
        if(change[2]) {
            el.find(change[0])[change[1]](change[2], src);
            if(return_after_first_success){
                return;
            }
        }
        if(change[1]){
            el.find(change[0])[change[1]](src);
            if(return_after_first_success){
                return;
            }
        }
    }
    function updateTemplate(el,attrs) {
        if(el.data('plus-change')){
            const plus_change =el.data('plus-change').split('.');
            updateTemplateChangeAttrs(el,plus_change,attrs.plus_src);
        }
        if(el.data('change')){
            const change =el.data('change').split('.');
            updateTemplateChangeAttrs(el,change,attrs.src,true);
        }
    }
    function appendErrors(errors,form) {
        form.find(modalConstants.main_error_message_in_form).html(errors.message);
        const single_error_messages = errors.errors;
        for(let error in single_error_messages){
            $('#error_form_'+error).html(single_error_messages[error]);
        }
    }
    return {
        form:'',
        url:'',
        el:'',
        data:{},
        submitModal,
        updateAttrs
    }
};

$(document).on('mouseover',vars.slick_container_hover_effect,function () {
    $(this).find(vars.slick_main_title).hide();
    $(this).find('.txt-change-color').removeClass('text-title-white').addClass('text-title-black');

    $(this).find(vars.paragraph_hidden_slick).css('visibility','visible');
});
$(document).on('mouseout',vars.slick_container_hover_effect,function () {
    $(this).find(vars.slick_main_title).show();
    $(this).find('.txt-change-color').addClass('text-title-white').removeClass('text-title-black');
    $(this).find(vars.paragraph_hidden_slick).css('visibility','hidden');
});
$(document).on('click',vars.btn_change_inner_html,function (e) {
   e.preventDefault();
   const ajaxOperation = new AjaxOperations();
   ajaxOperation.el = $(this);
   ajaxOperation.url = $(this).data('link')?$(this).data('link'):$(this).attr('href');
   ajaxOperation.updateAttrs();
});
$(document).on('click',vars.go_to_link,function () {
    window.location = $(this).data('link')?$(this).data('link'):$(this).attr('href');
});
