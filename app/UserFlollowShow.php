<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserFlollowShow extends Model
{
    protected $table = 'user_follow_shows';
    protected $fillable =['user_id','program_id'];

    public function program(){
        return $this->hasMany(
            Program::class,
            'program_id',
            'id'
        );
    }
    public function user(){
        return $this->hasMany(
            User::class,
            'user_id',
            'id'
        );
    }
    public static function mostFollowedProgram(){
        return UserFlollowShow::select(DB::raw('count(program_id) as max_id')
            ,DB::raw('programs.title as title'),'program_id')
            ->join('programs','program_id','=','programs.id')
            ->orderBy('max_id','desc')
            ->groupBy('program_id')
            ->first();

    }
    public static function followedPrograms(){
        return UserFlollowShow::select(DB::raw('count(program_id) as max_id')
            ,DB::raw('programs.title as title'),'program_id')
            ->join('programs','program_id','=','programs.id')
            ->orderBy('max_id','desc')
            ->groupBy('program_id')
            ->get();
    }
}
