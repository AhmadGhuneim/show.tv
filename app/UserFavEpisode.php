<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserFavEpisode extends Model
{
    protected $table = 'user_fav_episodes';
    protected $fillable =[
        'user_id',
        'episode_id',
        'like_status'
    ];
    public static function mostLikedProgram($status){
        return UserFavEpisode::select(DB::raw('count(episode_id) as max_id')
                    ,DB::raw('program_episodes.title as title')
                    ,DB::raw('programs.title as program_title'),
                    'episode_id')
            ->join('program_episodes','episode_id','=','program_episodes.id')
            ->join('programs','program_episodes.program_id','=','programs.id')
            ->where('like_status',$status)
            ->groupBy('episode_id')
            ->orderBy('max_id','desc')
            ->first();
    }
    public static function likedPrograms(){
        return UserFavEpisode::select(
                    DB::raw('COUNT(CASE WHEN like_status =1 THEN 1 ELSE NULL END) as liked'),
                    DB::raw('COUNT(CASE WHEN like_status =0 THEN 1 ELSE NULL END) as disliked')
                    ,DB::raw('program_episodes.title as title')
                    ,DB::raw('programs.title as program_title'),
                    'episode_id')
            ->join('program_episodes','episode_id','=','program_episodes.id')
            ->join('programs','program_episodes.program_id','=','programs.id')
            ->groupBy('episode_id')
            ->orderBy('liked','desc')
            ->get();
    }

}
