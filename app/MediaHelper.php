<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MediaHelper extends Model
{
    public static function getDuration($full_video_path)
    {
        $getID3 = new \getID3;
        $file = $getID3->analyze($full_video_path);
        if(!empty($file)) {
            $playtime_seconds = $file['playtime_seconds'];
            $duration = date('H:i:s', $playtime_seconds);

            return $duration;
        }
        return NULL;
    }
    public static function changeNameForSave($media){
        $out_put_file = preg_replace('/\\.[^.\\s]{3,4}$/', '', $media->getClientOriginalName());
        return  $out_put_file . preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', microtime()).rand(1,1000).'.'. $media->getClientOriginalExtension();
    }
}
