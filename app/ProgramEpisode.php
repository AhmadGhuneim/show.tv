<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgramEpisode extends Model
{
    protected $table = 'program_episodes';

    public function program(){
        return $this->belongsTo(
            Program::class,
            'program_id',
            'id'
        );
    }
    public function broadCastDay(){
        return $this->belongsTo(
            ProgramBroadcastDay::class,
            'broadcast_day_id',
            'id'
        );
    }
}
