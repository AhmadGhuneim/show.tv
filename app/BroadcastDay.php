<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BroadcastDay extends Model
{
    protected $table = 'broadcast_days';
    protected $fillable = [
        'broadcast_day',
    ];
    public static function days(){
        return [
            1 => 'Sunday',
            2 => 'Monday',
            3 => 'Tuesday',
            4 => 'Wednesday',
            5 => 'Thursday',
            6 => 'Friday',
            7 => 'Saturday',
        ];
    }
}
