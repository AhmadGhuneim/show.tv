<?php

namespace App\Rules;

use App\ProgramEpisode;
use Illuminate\Contracts\Validation\Rule;

class EpisodeNoRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($formParams,$id)
    {
        $this->formParams = $formParams;
        $this->id = $id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $params = $this->formParams;
        $programEpisodes = ProgramEpisode::where('program_id',$params['program_id'])
            ->where('episode_no',$value);

        if(!empty($this->id)){

            $programEpisodes = $programEpisodes->where('id','!=',$this->id);
        }
        return $programEpisodes->count() > 0?false:true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'There is an episode with that no.';
    }
}
