<?php

namespace App\Rules;

use App\BroadcastDay;
use App\ProgramBroadcastDay;
use App\ProgramEpisode;
use Illuminate\Contracts\Validation\Rule;

class BraodCastDateRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($formParams,$id)
    {
        $this->formParams = $formParams;
        $this->id = $id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        $params = $this->formParams;

        return (strtolower(date('D', strtotime($params['broadcast_date']))) == strtolower(substr(BroadcastDay::find($this->formParams['broadcast_day_id'])->broadcast_day,0,3)));
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
            return 'The Date selected is not '. BroadcastDay::find($this->formParams['broadcast_day_id'])->broadcast_day;
    }
}
