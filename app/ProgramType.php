<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgramType extends Model
{
    protected $table = 'program_types';
    protected $fillable = ['programs_type'];

    public function program(){
        return $this->hasMany(
            Program::class,
            'program_type_id',
            'id'
        );
    }
    public function getUrlParams(){
        return [
            str_replace(' ',
                '-',
                $this->programs_type
                ),
            'program_type'=>$this->id
        ];
    }
    public function modifyTypeToUrl(){
        return preg_replace('/\s+/', '-', $this->programs_type);
    }
}
