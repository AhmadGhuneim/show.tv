<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    protected $table = 'programs';
    protected $fillable = [
        'program_type_id',
        'title',
        'description',
        'broadcast_start_date',
        'broadcast_start_time',
        'broadcast_end_time',
        'on_air',
        'active',
        'main_image'
    ];
    public function programType(){
        return $this->belongsTo(
            ProgramType::class,
            'program_type_id',
            'id'
        );
    }
    public function broadCastDaysProgram(){
        return $this->belongsToMany(
            BroadcastDay::class,
            'program_broadcast_days',
            'program_id',
            'day_id'
        );
    }
    public function episodes(){
        return $this->hasMany(
            ProgramEpisode::class,
            'program_id',
            'id'
        );
    }

    public function getUrlParams(){
        return [
            $this->id,
            'title'=>str_replace(' ',
                '-',
                $this->title
            )
        ];
    }
    public function modifyTypeToUrl(){
        return preg_replace('/\s+/', '-', $this->title);
    }
    public static function boot() {
        parent::boot();
        static::deleting(function($program) {
            $program->broadCastDaysProgram()->delete();
            $program->episodes()->delete();
        });
    }
}
