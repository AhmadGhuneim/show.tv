<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgramBroadcastDay extends Model
{
    protected $table = 'program_broadcast_days';
    protected $fillable = [
        'program_id',
        'day_id'
    ];
    public function program(){
        return $this->belongsTo(
            Program::class,
            'program_id',
            'id'
        );
    }
    public function episode(){
        return $this->hasMany(
            ProgramEpisode::class,
            'broadcast_day_id',
            'id'
        );
    }
    public function broadcastDay(){
        return $this->belongsTo(
            BroadcastDay::class,
            'day_id',
            'id'
        );
    }
    public static function days(){
        return [
            1 => 'Sunday',
            2 => 'Monday',
            3 => 'Tuesday',
            4 => 'Wednesday',
            5 => 'Thursday',
            6 => 'Friday',
            7 => 'Saturday',
        ];
    }
}
