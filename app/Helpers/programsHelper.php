<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 23/06/20
 * Time: 11:16 م
 */
function arrangeProgramShowingDays($program){
    $programShowingDays = $program->broadCastDaysProgram->sortBy('id')->pluck('broadcast_day','id')->toArray();

    reset($programShowingDays);
    $programShowingfirstId = key($programShowingDays);

    end($programShowingDays);
    $programShowingLastId = key($programShowingDays);

    $txt ='';
    $in_order_program = true;
    $start_day = $programShowingDays[$programShowingfirstId];
    $endDay = $programShowingDays[$programShowingLastId];
    for($i=$programShowingfirstId;$i<=$programShowingLastId;$i++){
        if(!isset($programShowingDays[$i])){
                $in_order_program = false;
                if(isset($programShowingDays[$i-1])){
                    $endDay = $programShowingDays[$i-1];
                    $txt .= arrangeShowingTimeText($start_day,$endDay);
                }
        }else{
            if(!$in_order_program){
                $in_order_program = true;
                $start_day = $programShowingDays[$i];
            }
            $endDay = $programShowingDays[$i];
        }
    }
    if($in_order_program){
        $txt .= arrangeShowingTimeText($start_day,$endDay);
    }
    return $txt;
}
function arrangeShowingTimeText($startDay,$endDay){
    if($startDay == $endDay){
        return ' '. substr($startDay,0,3);
    }
    return ' '. substr($startDay,0,3) .'-'.substr($endDay,0,3);
}
