<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
use App\ProgramType;

class ProgramTypeComposer
{
    /**
     * The user repository implementation.
     *
     * @var ProgramType
     */
    protected $programTypes;

    /**
     * Create a new profile composer.
     *
     * @param  ProgramType  $programTypes
     * @return void
     */
    public function __construct(ProgramType $programTypes)
    {
        $this->programTypes = $programTypes;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
            $view
                ->with('countProgramTypes', $this->programTypes->count())
                ->with('programTypes', $this->programTypes->get());
    }
}
