<?php

namespace App\Http\Controllers;

use App\UserFlollowShow;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserFlollowShowController extends Controller
{


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Auth::check()){
            return response()->json(
                [
                    'valid'=>false,
                    'link'=>route('noAuthView')
                ]);
        }
        $userFollowShow = UserFlollowShow::where('program_id',$request->program_id)
            ->where('user_id',auth()->user()->id)->first();
        if(!empty($userFollowShow) && $userFollowShow->count()){
            if($this->destroy($userFollowShow)){
                return response()->json([
                   'valid'=>true,
                    'attrs'=>
                        [
                            'src'=>asset('images/heart copy 2.svg')
                        ]
                ]);
            }
        }
        UserFlollowShow::create([
           'user_id'=>auth()->user()->id,
           'program_id'=>$request->program_id
        ]);
        return response()->json([
            'valid'=>true,
            'attrs'=>
                [
                    'src'=>asset('images/Heart icon.png')
                ]

        ]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserFlollowShow  $userFlollowShow
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserFlollowShow $userFlollowShow)
    {
        $userFlollowShow->delete();

        return true;
    }
}
