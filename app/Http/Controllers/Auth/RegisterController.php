<?php

namespace App\Http\Controllers\Auth;

use App\MediaHelper;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'user_image' => ['required', 'mimes:jpeg,jpg,png'],
        ]);
    }
    public function showRegistrationForm(Request $request)
    {
        if($request->expectsJson()) {
            return
                response()->json(['view'=>view('auth.registerForm')->render()]);
        }
        return view('auth.register');
    }

    public function register(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return response()
                ->json([
                    'message'=>'There were some errors',
                    'errors'=>$validator->errors()
                ],431);
        }

        $user = $this->create($request->all());
        auth()->login($user);
        Session::flash('info','You registered successfully');
        if($request->expectsJson())
            return response()->json(array('valid' => true));
        return redirect()->back();
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user_image = '';

        if(!empty($data['user_image']))
        {
            $photo = $data['user_image'];
            $photo_new_name
                = MediaHelper::changeNameForSave($photo);
            $photo->storeAs('images',$photo_new_name);
            $user_image = 'images/'.$photo_new_name;
        }
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'user_image' => $user_image,
        ]);
    }
}
