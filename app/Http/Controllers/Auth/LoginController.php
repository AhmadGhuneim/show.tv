<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function showLoginForm(Request $request)
    {
        if($request->expectsJson()) {
            return
                response()->json(['view'=>view('auth.loginForm')->render()]);
        }
        return view('auth.login');
    }

    public function login(Request $request)
    {

        if(Auth::attempt(['email' => $request->email, 'password' => $request->password],$request->remember)){

            $response = array('success' => true);
            Session::flash('success','You are logged in');
            if($request->expectsJson())
                return response()->json($response);
            return redirect()->intended();
        }
        else{

            $response = array('success' => false, 'message' => 'Invalid login credentials');
            Session::flash('error','Something went wrong');
            if($request->expectsJson())
                return response()->json($response,431);
            return redirect()->back();
        }
    }
    public function noAuthView(Request $request){
        if($request->expectsJson()){
            return response()->json(['view'=>view('auth.noAuthBody')->render()]);
        }
    }
}
