<?php

namespace App\Http\Controllers;

use App\Program;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')
        ->except('index');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $TvShows = Program::inRandomOrder()->take(10)
            ->where('program_type_id',1)->get();
        $series = Program::inRandomOrder()->take(10)
            ->where('program_type_id',2)->get();
        return view('home')
            ->with('TvShows',$TvShows)
            ->with('series',$series);
    }
}
