<?php

namespace App\Http\Controllers;

use App\ProgramEpisode;
use Illuminate\Http\Request;

class ProgramEpisodeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProgramEpisode  $programEpisode
     * @return \Illuminate\Http\Response
     */
    public function show(ProgramEpisode $programEpisode)
    {
        return view('episodes.show')
            ->with('episode',$programEpisode);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProgramEpisode  $programEpisode
     * @return \Illuminate\Http\Response
     */
    public function edit(ProgramEpisode $programEpisode)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProgramEpisode  $programEpisode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProgramEpisode $programEpisode)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProgramEpisode  $programEpisode
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProgramEpisode $programEpisode)
    {
        //
    }
}
