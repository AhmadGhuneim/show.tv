<?php

namespace App\Http\Controllers;

use App\UserFavEpisode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserFavEpisodeController extends Controller
{
    public function likeDislikeEpisode(Request $request){
        if(!Auth::check()){
            return response()->json(
                [
                    'valid'=>false,
                    'link'=>route('noAuthView')
                ]);
        }
        $userFavEpisode = UserFavEpisode::where('episode_id',$request->episode_id)
            ->where('user_id',auth()->user()->id)
            ->first();
        if(!empty($userFavEpisode) && $userFavEpisode->count()){
            if($userFavEpisode->like_status == $request->like_status) {
                if ($this->destroy($userFavEpisode)) {
                    return response()->json([
                        'valid' => true,
                        'attrs'=>   $this->returnThumbIcon($request->like_status,'deleted')
                    ]);
                }
            }else{
                $userFavEpisode->update([
                   'like_status'=>$request->like_status
                ]);
                return response()->json([
                    'valid' => true,
                    'attrs'=>$this->returnThumbIcon($request->like_status)
                ]);
            }
        }
        UserFavEpisode::create([
            'user_id'=>auth()->user()->id,
            'episode_id'=>$request->episode_id,
            'like_status'=>$request->like_status
        ]);
        return response()->json([
            'valid'=>true,
            'attrs'=>$this->returnThumbIcon($request->like_status)
        ]);
    }
    protected function returnThumbIcon($status,$crudStatus=''){
        $src ='';
        $plus_src='';
        if($crudStatus=='deleted'){
            if($status==0){
                $src='fa-thumbs-o-down';
                $plus_src = 'fa-thumbs-down';
            }
            if($status==1){
                $src='fa-thumbs-o-up';
                $plus_src = 'fa-thumbs-up';
            }
        }else{
            if($status==0){
                $src = 'fa-thumbs-down';
                $plus_src='fa-thumbs-o-down';
            }
            if($status==1){
                $src = 'fa-thumbs-up';
                $plus_src='fa-thumbs-o-up';
            }
        }
        return
                ['src'=>$src, 'plus_src'=>$plus_src];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserFavEpisode  $userFavEpisode
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserFavEpisode $userFavEpisode)
    {
        $userFavEpisode->delete();
        return true;
    }
}
