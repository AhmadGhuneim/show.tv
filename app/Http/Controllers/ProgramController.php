<?php

namespace App\Http\Controllers;

use App\Program;
use App\ProgramEpisode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type,Request $request)
    {

        $programs = Program::where('program_type_id',$request->program_type)
            ->orderBy('id','desc')
            ->paginate(12);

        return view('programs.index')
            ->with('programs',$programs)
            ->with('type',$type);
    }
    public function search(Request $request){

        session()->flashInput($request->input());

        list($programs,$episodes) = $this->searchQuery($request);

        return view('programs.search')
            ->with('programs',$programs)
            ->with('episodes',$episodes);
    }
    public function searchQuery($request){
        $txt = $request->searched_text;

        $programs =  Program::where('title','like', "%{$txt}%")
            ->orWhere('description','like',"%{$txt}%")->paginate(8);

        $episodes = ProgramEpisode::
            where('title','like', "%{$txt}%")
            ->orWhere('description','like',"%{$txt}%")->paginate(8, ['*'], 'episode_page');



        return  [$programs,$episodes];

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function show(Program $program,Request $request)
    {
        $episodes = $program->episodes()->paginate(8);
        return view('programs.show')
            ->with('program',$program)
            ->with('episodes',$episodes);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function edit(Program $program)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Program $program)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function destroy(Program $program)
    {
        //
    }
}
