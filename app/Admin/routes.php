<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('home');
    $router->resource('users', UserController::class,[
        'only' => ['index', 'show']
    ]);
    $router->get('programs/getProgramBroadcastTimes/{program}'
        ,'ProgramController@getProgramBroadcastTimes');

    $router->resource('programs', ProgramController::class);
    $router->resource('program-episodes', ProgramEpisodeController::class);
});
