<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Program;
use App\ProgramEpisode;
use App\UserFavEpisode;
use App\UserFlollowShow;
use Encore\Admin\Controllers\Dashboard;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Encore\Admin\Widgets\InfoBox;
use Encore\Admin\Widgets\Table;


class HomeController extends Controller
{
    public function index(Content $content)
    {
        return $content
            ->title('Dashboard')
            ->description('Description...')
            ->row(function (Row $row) {

                $row->column(4, function (Column $column) {
                    $mostFollowedProgram = UserFlollowShow::mostFollowedProgram();
                    if(!empty($mostFollowedProgram)) {
                        $infoBox = new InfoBox('Most followed program',
                            'product-hunt',
                            'purple', '/showTvAdmin/programs/' . $mostFollowedProgram->program_id, $mostFollowedProgram->title . ' ' . $mostFollowedProgram->max_id);
                    }else{
                        $infoBox = new InfoBox('Most followed program',
                            'product-hunt',
                            'purple', '', 0);
                    }
                    $column->append($infoBox->render());
                });
                $row->column(4, function (Column $column) {
                    $mostFavEpisode = UserFavEpisode::mostLikedProgram(1);

                    if(!empty($mostFavEpisode)) {
                        $infoBox = new InfoBox('Most liked program',
                            'codiepie',
                            'purple', '/showTvAdmin/programs/' . $mostFavEpisode->episode_id, $mostFavEpisode->program_title . ' ' . $mostFavEpisode->title . ' ' . $mostFavEpisode->max_id);
                    }else{
                        $infoBox = new InfoBox('Most liked program',
                            'codiepie',
                            'purple', '', 0);
                    }
                    $column->append($infoBox->render());
                });
                $row->column(4, function (Column $column) {
                    $mostFavEpisode = UserFavEpisode::mostLikedProgram(0);

                    if(!empty($mostFavEpisode)) {
                        $infoBox = new InfoBox('Most disliked program',
                            'codiepie',
                            'purple', '/showTvAdmin/programs/' . $mostFavEpisode->episode_id, $mostFavEpisode->program_title . ' ' . $mostFavEpisode->title . ' ' . $mostFavEpisode->max_id);
                    }else{
                        $infoBox = new InfoBox('Most liked program',
                            'codiepie',
                            'purple', '', 0);
                    }
                    $column->append($infoBox->render());
                });
            })->row(function (Row $row) {

                $row->column(4, function (Column $column) {
                    $infoBox = new InfoBox('Total programs',
                        'product-hunt',
                        'purple', '/showTvAdmin/programs/', Program::count());
                    $column->append($infoBox->render());
                });
                $row->column(4, function (Column $column) {
                    $infoBox = new InfoBox('Total Episodes',
                        'codiepie',
                        'purple', '/showTvAdmin/programs/', ProgramEpisode::count());
                    $column->append($infoBox->render());
                });
                $row->column(4, function (Column $column) {
                    $infoBox = new InfoBox('Total Users',
                        'users',
                        'purple', '/showTvAdmin/programs/', ProgramEpisode::count());
                    $column->append($infoBox->render());
                });
            })->row(function (Row $row) {

                $row->column(12, function (Column $column) {
                    $followedPrograms = UserFlollowShow::followedPrograms();
                    $headers = ['Id', 'Title', 'follow count'];
                    $rows = [
                    ];
                    if(!empty($followedPrograms)){
                        foreach ($followedPrograms as $followedProgram) {
                            $rows[] = [
                                $followedProgram->program_id,
                                $followedProgram->title,
                                $followedProgram->max_id];
                        }
                    }

                    $table = new Table($headers, $rows,['background-white']);
                    $column->append($table->render());
                });
            })->row(function (Row $row) {

                $row->column(12, function (Column $column) {
                    $likedEpisodes = UserFavEpisode::likedPrograms();
                    $headers = ['Id', 'Program','Episode title', 'liked','disliked'];
                    $rows = [
                    ];
                    if(!empty($likedEpisodes)){
                        foreach ($likedEpisodes as $likedEpisode) {
                            $rows[] = [
                                $likedEpisode->episode_id,
                                $likedEpisode->program_title,
                                $likedEpisode->title,
                                $likedEpisode->liked,
                                $likedEpisode->disliked,
                                ];
                        }
                    }

                    $table = new Table($headers, $rows,['background-white']);
                    $column->append($table->render());
                });
            });
    }
}
