<?php

namespace App\Admin\Controllers;

use App\BroadCastDay;
use App\Program;
use App\ProgramBroadcastDay;
use App\ProgramType;
use App\Rules\CheckProgramTimeValid;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class ProgramController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Program';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Program());

        $grid->column('id', __('Id'));
        $grid->column('programType.programs_type', __('program Type'));
        $grid->column('title', __('Title'));
        $grid->column('description', __('Description'))->limit(50);
        $grid->column('broadcast_start_date', __('Broadcast start date'));
        $grid->column('broadcast_start_time', __('Broadcast start time'));
        $grid->column('broadcast_end_time', __('Broadcast end time'));
        $grid->column('on_air', __('On air'))->bool([
            1=>'Yes',
            0=>'No'
        ]);
        $grid->column('active', __('Active'))
        ->bool([
            1=>'Yes',
            0=>'No'
        ]);
        $grid->column('main_image', __('Main image'))->image();
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Program::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('programType.programs_type','program type');
        $show->field('title', __('Title'));
        $show->field('description', __('Description'));
        $show->field('broadcast_start_date', __('Broadcast start date'));
        $show->field('broadcast_start_time', __('Broadcast start time'));
        $show->field('broadcast_end_time', __('Broadcast end time'));
        $show->field('on_air', __('On air'));
        $show->field('active', __('Active'));
        $show->field('main_image', __('Main image'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        Admin::js('ads');
        $form = new Form(new Program());
        $form->radioCard('program_type_id',
            'program type')
        ->options(ProgramType::all()->pluck('programs_type','id')->toArray())
        ->required();

        $form->text('title', __('Title'))
        ->rules(['required','max:255']);
        $form->textarea('description', __('Description'))
            ->required();
        $form->date('broadcast_start_date', __('Broadcast start date'))
            ->default(date('Y-m-d'))
        ->required();
        $form->time('broadcast_start_time', __('Broadcast start time'))
            ->default(date('H:i'))
        ->required()->rules(['date_format:H:i:s']);
        $form->time('broadcast_end_time', __('Broadcast end time'))
            ->default(date('H:i'))
            ->required()->rules(['date_format:H:i:s']);

        $form->checkboxCard('broadCastDaysProgram')
            ->options(ProgramBroadcastDay::days())
            ->setScript('
                $(document).on("change",".broadCastDaysProgram",function(){
                    let vals = $(".broadCastDaysProgram:checked").map((key,val)=>val.value);
                    console.log(vals);
                });
            ')
            ->rules(['required']);

        $form->switch('on_air', __('On air'))->default(1);
        $form->switch('active', __('Active'))->default(1);
        $form->image('main_image', __('Main image'));

        return $form;
    }
    public function getProgramBroadcastTimes(Program $program){
        return response()->json([
            'valid'=>true,
            'broadCastDays'=>$program->broadCastDaysProgram->pluck('broadcast_day','id')->toArray()
        ]);
    }
}
