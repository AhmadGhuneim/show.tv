<?php

namespace App\Admin\Controllers;

use App\Program;
use App\ProgramEpisode;
use App\MediaHelper;
use App\Rules\BraodCastDateRule;
use App\Rules\EpisodeNoRule;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProgramEpisodeController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'ProgramEpisode';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ProgramEpisode());

        $grid->column('id', __('Id'));
        $grid->column('program.title', __('Program id'));
        $grid->column('Broadcast day', __('Broadcast day'))
        ->display(function ($day){
            if(!empty($this->broadCastDay) && !empty($this->broadCastDay->broadcastDay))
                return $this->broadCastDay->broadcastDay->broadcast_day;
            return '';
        });
        $grid->column('broadcast_date', __('Broadcast date'));
        $grid->column('episode_no', __('Episode no'));
        $grid->column('title', __('Title'));
        $grid->column('description', __('Description'))->limit(50);
        $grid->column('duration', __('Duration'));
        $grid->column('thumbnail', __('Thumbnail'))->image();
        $grid->column('video', __('Video'))->downloadable();
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ProgramEpisode::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('program_id', __('Program'));
        $show->field('broadcast_day_id', __('Broadcast day id'));
        $show->field('broadcast_date', __('Broadcast date'));
        $show->field('episode_no', __('Episode no'));
        $show->field('title', __('Title'));
        $show->field('description', __('Description'));
        $show->field('duration', __('Duration'));
        $show->field('thumbnail', __('Thumbnail'));
        $show->field('video', __('Video'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    public function update($id)
    {
        return $this->form($id)->update($id);
    }
    public function edit($id,Content $content)
    {
        return $content
            ->title($this->title())
            ->description($this->description['edit'] ?? trans('admin.edit'))
            ->body($this->form($id)->edit($id));
    }
    protected function form($id = null)
    {
        $request = Request::capture();

        $form = new Form(new ProgramEpisode());

        $form->select('program_id', __('Program'))
        ->options(Program::all()->pluck('title','id')->toArray())
        ->addElementClass('program_id')
            ->required();


        $form
            ->select('broadcast_day_id', __('Broadcast day'))
            ->required()
        ->options(function (){
            if(!empty($this->program)){
             return  $this->program
                    ->broadCastDaysProgram->pluck('broadcast_day','id')->toArray();
            }
            return [];
        });

        $form->date('broadcast_date', __('Broadcast date'))
            ->default(date('Y-m-d'))
            ->rules([new BraodCastDateRule($request->all(),$id)])
            ->required();

        $form->number('episode_no', __('Episode no'))
            ->required()
        ->rules([new EpisodeNoRule($request->all(),$id)]);

        $form->text('title', __('Title'))->required();

        $form->textarea('description', __('Description'))
        ->required();

        $form->image('thumbnail', __('Thumbnail'))->required();

        $form->file('video', __('Video'))
        ->required()->rules(['mimes:mp4,mov,ogg,qt,mkv']);

        $form->saved(function ($savedForm){
            $videoPath  = Storage::disk('public')->getDriver()->getAdapter()->applyPathPrefix($savedForm->model()->video);
            $savedForm->model()->duration = MediaHelper::getDuration($videoPath);
            $savedForm->model()->save();
        });

        return $form;
    }
}
