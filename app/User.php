<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','user_image'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function followingPrograms(){
        return $this->hasMany(
            UserFlollowShow::class,
            'user_id',
            'id'
        );
    }
    public function favEpisodes(){
        return $this->hasMany(
            UserFavEpisode::class,
            'user_id',
            'id'
        );
    }
    public function checkLikedEpisode($episode_id,$status){
        return $this->favEpisodes()
            ->where('episode_id',$episode_id)
            ->where('like_status',$status)
            ->count();
    }
    public function checkFollowingProgram($program_id){
        return $this->followingPrograms()->where('program_id',$program_id)
            ->count();
    }
}
