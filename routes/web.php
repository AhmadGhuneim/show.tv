<?php
use \Illuminate\Support\Facades\Route;
use Illuminate\Routing\Router;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::get('program/search','ProgramController@search')
    ->name('program.search');
Route::get('program/{slug?}','ProgramController@index')
    ->name('program.list');

Route::resource('programs','ProgramController');

Route::get('addToFavourite','UserFlollowShowController@store')
    ->name('addToFavourite');

Route::get('likeDislikeEpisode','UserFavEpisodeController@likeDislikeEpisode')
    ->name('likeDislikeEpisode');

Route::get('noAuthView','Auth\LoginController@noAuthView')
    ->name('noAuthView');

Route::group([
//    'prefix'        => config('admin_new.route.prefix'),
//    'namespace'     => config('admin_new.route.namespace'),
    'middleware'    => ['auth'],
], function (Router $router) {
    $router->get('episode/{programEpisode}', 'ProgramEpisodeController@show')
        ->name('episode.show');
});

