<?php

use Illuminate\Database\Seeder;

class BroadcastDaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (\App\BroadcastDay::days() as $day) {
            \App\BroadcastDay::create([
                'broadcast_day'=>$day
            ]);
        }
    }
}
