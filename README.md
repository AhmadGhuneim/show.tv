# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
    Please run these commands first
        - php artisan migrate
        - php artisan db:seed
        - and please empty the db and import show_tv.sql, 
        i know it's bad approach it must be a (seeder), but let's say it is just for this assignment, to deliver it as quickly as possible. 
       
* How to run tests
* Deployment instructions
    Please run these commands
    - composer update
    - php artisan storage:link
    - php artisan admin:install
      
 Note: i upload storage folder just to show the content (images & videos)
  
  to access admin 
  http://{websiteUrl}/showTvAdmin
  
  user name : admin
  password : admin

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
