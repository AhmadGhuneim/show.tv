<script src="{{ asset('js/app.js') }}" ></script>
<script src="https://kit.fontawesome.com/21d57e38a8.js" crossorigin="anonymous"></script>
<script src="{{ asset('js/jqueryForms.js') }}" defer></script>
<script src="{{asset('js/bootstrap-filestyle.js')}}"></script>
<script src="{{ asset('js/toastr.min.js') }}" ></script>
<script src="{{ asset('js/site.js') }}" ></script>
<script src="{{ asset('js/modal.js') }}" ></script>
@include('layouts.toastr')
@yield('scripts')
