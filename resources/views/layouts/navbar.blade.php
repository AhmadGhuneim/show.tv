
<nav class="navbar navbar-expand-md  shadow-sm">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            <img src="{{asset('images/logo.png')}}" />
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
                <li class="nav-item menu_paragraphs">
                    <a class="nav-link {{request()->is('/')?'active':''}}"
                       href="/">Home</a>
                </li>
                @if(!empty($programTypes))
                    @foreach($programTypes as $programType)
                        <li class="nav-item menu_paragraphs">
                                <a class="nav-link {{request()->is('program/'.$programType->modifyTypeToUrl().'*')?'active':''}}"
                                   href="{{route('program.list',$programType->getUrlParams())}}">{{$programType->programs_type}}</a>
                        </li>
                    @endforeach
                @endif

            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <form method="get" action="{{route('program.search')}}">
                        <div class="form-group form-group-search">
                        <input type="text"
                               class="search_website"
                               name="searched_text"
                               value="{{old('searched_text')}}" />
                        <i class="fas fa-search" style="color:white"></i>
                        </div>
                    </form>
                </li>
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item menu_paragraphs">
                        <a class="nav-link call-global-modal active"

                           href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item menu_paragraphs">
                            <a class="nav-link call-global-modal active"

                               href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown menu_paragraphs">
                        <a class="nav-link active"

                           href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
