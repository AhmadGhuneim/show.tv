<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link href="{{ asset('css/toastr.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/site.css') }}" rel="stylesheet">
@yield('styles')
