<script>
    @if(Session::has('success'))

    toastr.success("{{ Session::get('success') }}");
    const playAudio = new mediaObject();
    playAudio.PlayNotificationAudio();
    @endif
    @if(Session::has('info'))

    toastr.info("{{ Session::get('info') }}");
    const playAudio = new mediaObject();
    playAudio.PlayNotificationAudio();
    @endif
    @if(Session::has('error'))

    toastr.error("{{ Session::get('error') }}");
    const playAudio = new mediaObject();
    playAudio.PlayNotificationAudio();
    @endif
</script>
