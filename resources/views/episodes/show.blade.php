@extends('layouts.app')
@section('content')

    <div class="container" style="padding: 25px;background: white">
        <div class="row">
            <div class="col-md-8">
                <p class="text-title-episode text-purple">
                    {{str_limit($episode->title,50)}}
                    Episode : {{$episode->episode_no}}
                </p>
            </div>
            <div class="col-md-4">
                <p class="text-title-episode text-purple text-right">
                    <button data-link="{{route('likeDislikeEpisode',['episode_id'=>$episode->id,'like_status'=>1])}}"
                            data-change="i.addClass"
                            data-plus-change="i.removeClass"
                            data-remove-class="fa-thumbs-down"
                            data-add-class="fa-thumbs-o-down"
                            class="btn_change_inner_html btn-no-background-border text-purple font-size-25">
                        @if(!Auth::guest() && auth()->user()->checkLikedEpisode($episode->id,1))
                            <i class="fa fa-thumbs-up
                        like-dislike-txt text-purple font-size-25" aria-hidden="true"></i>
                        @else
                            <i class="fa fa-thumbs-o-up
                        like-dislike-txt text-purple font-size-25" aria-hidden="true"></i>
                        @endif
                        Like

                    </button>
                    <button data-link="{{route('likeDislikeEpisode',['episode_id'=>$episode->id,'like_status'=>0])}}"
                            data-change="i.addClass"
                            data-plus-change="i.removeClass"
                            data-remove-class="fa-thumbs-up"
                            data-add-class="fa-thumbs-o-up"
                            class="btn_change_inner_html btn-no-background-border text-purple font-size-25">
                        @if(!Auth::guest() && auth()->user()->checkLikedEpisode($episode->id,0))
                            <i class="fa fa-thumbs-down
                        like-dislike-txt text-purple font-size-25" aria-hidden="true"></i>
                        @else
                            <i class="fa fa-thumbs-o-down
                        like-dislike-txt text-purple font-size-25" aria-hidden="true"></i>
                        @endif
                        Dislike
                    </button>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <video style="width: 100%;" controls>
                    <source src="{{asset('storage/'.$episode->video)}}" type="video/mp4">
                    <source src="{{asset('storage/'.$episode->video)}}" type="video/ogg">
                    Your browser does not support HTML video.
                </video>
            </div>
        </div>
        <div class="row">
            <div class="col-10">
                <p class="font-size-18 text-left">
                {{$episode->description}}
                </p>
            </div>
            <div class="col-2">
                <p class="font-size-18 text-right">
                {{$episode->duration}}
                </p>
            </div>
        </div>

    </div>

@endsection
