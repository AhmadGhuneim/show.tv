@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <p class="text-center title-text">
                    Search
                </p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <p class="text-left title-text">
                    Programs
                </p>
            </div>
        </div>
        <div class="row">
            @if(!empty($programs))
                @foreach($programs as $program)
                    <div class="col-12 col-sm-6 col-md-3 slick_show">
                        @include('programs.common.series')
                    </div>
                @endforeach
            @endif
        </div>
        <div class="row">
            <div class="col-lg-12">
                {{ $programs
                ->appends(array_except(Request::query(), 'page'))
                ->links()}}
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 title-text text-white">
                Episodes
            </div>
        </div>
        <div class="row">
            @if(!empty($episodes))
                @foreach($episodes as $episode)
                    <div class="col-12 col-sm-6 col-md-3">
                        @include('programs.common.episodes')
                    </div>
                @endforeach
            @endif
        </div>
        <div class="row">
            <div class="col-lg-12">
                {{ $episodes
                ->appends(array_except(Request::query(), 'episode_page'))
                ->links() }}
            </div>
        </div>
    </div>
@endsection
