@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-12">
                <p class="text-center title-text">
                    {{str_replace('-', ' ', $program->programType->programs_type)}}
                </p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div style="background-image: url('{{asset('storage/'. $program->main_image)}}')" class="img-slick program_show_background img-responsive img-fluid">
                </div>
                {{--<img src="{{asset('storage/'.$program->main_image)}}" class="img-responsive img-fluid" />--}}
            </div>
            <div class="col-md-6">
                <div class="row border_bottom_purple">
                    <div class="col-md-6">
                        <p class="text-white font-size-25 text-left">{{$program->title}}</p>
                    </div>
                    <div class="col-md-6 text-right">
                        <button data-link="{{route('addToFavourite',['program_id'=>$program->id])}}"
                                data-change="img.attr.src"
                                class="btn btn-no-background-border btn_img_heart btn_change_inner_html text-white font-size-18">
                            @if(!Auth::guest() && auth()->user()->checkFollowingProgram($program->id))
                                <img src="{{asset('images/Heart icon.png')}}" class="img-responsive heart_btn" />
                            @else
                                <img src="{{asset('images/heart copy 2.svg')}}" class="img-responsive heart_btn" />
                            @endif
                            Follow
                        </button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <p class="text-purple font-size-18 text-left">{{arrangeProgramShowingDays($program)}}</p>
                    </div>
                    <div class="col-md-6">
                        <p class="text-purple font-size-18 text-right">{{$program->broadcast_start_time}}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <p class="text-white">
                            {{str_limit($program->description,1500)}}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container margin-top-20">
        <div class="row">
            <div class="col-12 title-text text-white">
                Episodes
            </div>
        </div>
        <div class="row">
            @if(!empty($episodes))
                @foreach($episodes as $episode)
                    <div class="col-12 col-sm-6 col-md-3">
                        @include('programs.common.episodes')
                    </div>
                @endforeach
            @endif
        </div>
        <div class="row">
            <div class="col-lg-12">
                {{ $episodes
                    ->appends(request()->except('page'))
                    ->onEachSide(5)->links() }}
            </div>
        </div>
    </div>

@endsection
