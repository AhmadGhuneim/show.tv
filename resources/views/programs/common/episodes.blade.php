<a href="{{route('episode.show',[$episode->id])}}" class="no_descoration_href">
    <div class="row">
        <div class="col-md-12">
        <p class="text-title-episode text-white">
            {{str_limit($episode->title,10)}}
            Episode : {{$episode->episode_no}}
        </p>
        </div>
    </div>
        <div style="background-image: url('{{asset('storage/'. (!empty($episode->image)?$episode->image:$episode->thumbnail))}}')" class="img-slick episodes_bacground"></div>
        <div class="row">
            <div class="col-md-7">
                <p class="text-purple text-left margin-top-10">
                    {{substr($episode->broadCastDay->broadcastDay->broadcast_day,0,3)}}
                    {{date('y/m/d',strtotime($episode->broadcast_date))}}
                </p>
            </div>
            <div class="col-md-5">
                <p class="text-right margin-top-10">
                    <button data-link="{{route('likeDislikeEpisode',['episode_id'=>$episode->id,'like_status'=>1])}}"
                       data-change="i.addClass"
                       data-plus-change="i.removeClass"
                       data-remove-class="fa-thumbs-down"
                       data-add-class="fa-thumbs-o-down"
                       class="btn_change_inner_html btn-no-background-border ">
                        @if(!Auth::guest() && auth()->user()->checkLikedEpisode($episode->id,1))
                            <i class="fa fa-thumbs-up
                        like-dislike-txt" aria-hidden="true"></i>
                        @else
                            <i class="fa fa-thumbs-o-up
                        like-dislike-txt" aria-hidden="true"></i>
                        @endif

                    </button>
                    <button data-link="{{route('likeDislikeEpisode',['episode_id'=>$episode->id,'like_status'=>0])}}"
                       data-change="i.addClass"
                       data-plus-change="i.removeClass"
                       data-remove-class="fa-thumbs-up"
                       data-add-class="fa-thumbs-o-up"
                       class="btn_change_inner_html btn-no-background-border">
                        @if(!Auth::guest() && auth()->user()->checkLikedEpisode($episode->id,0))
                            <i class="fa fa-thumbs-down
                        like-dislike-txt" aria-hidden="true"></i>
                        @else
                            <i class="fa fa-thumbs-o-down
                        like-dislike-txt" aria-hidden="true"></i>
                        @endif
                    </button>
                </p>
            </div>

        </div>
</a>
