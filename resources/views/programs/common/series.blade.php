<a href="{{route('programs.show',$program->getUrlParams())}}">
    <div class="slick_container_hover_effect">
        <p class="slick_main_title">
            {{$program->title}}
        </p>
        <div style="background-image: url('{{asset('storage/'. (!empty($program->image)?$program->image:$program->main_image))}}')" class="img-slick series_background">
            @include('programs.common.heartDesign')

        </div>
        <div class="paragraph_hidden_slick">
            <p class="text-title-black">
                {{$program->title}}
            </p>
            <p class="title-text">
                {{arrangeProgramShowingDays($program)}} {{date('H:i',strtotime($program->broadcast_start_time))}}
            </p>
        </div>

    </div>
</a>
