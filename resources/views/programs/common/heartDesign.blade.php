<div class="follow_btns">
    @auth
        <button
                data-link="{{route('addToFavourite',['program_id'=>$program->id])}}"
                data-change="img.attr.src"
                class="btn btn-rounded btn_img_heart btn_change_inner_html">
            @if(!Auth::guest() && auth()->user()->checkFollowingProgram($program->id))
                <img src="{{asset('images/Heart icon.png')}}" class="img-responsive heart_btn" />
            @else
                <img src="{{asset('images/heart copy 2.svg')}}" class="img-responsive heart_btn" />
            @endif

        </button>
    @else
        <button
                data-link="{{route('noAuthView')}}"
                class="btn btn-rounded call-global-modal btn_img_heart">
            <img src="{{asset('images/heart copy 2.svg')}}" class="img-responsive heart_btn" />
        </button>
    @endauth
</div>
