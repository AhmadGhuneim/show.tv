@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <p class="text-center title-text">
                    {{str_replace('-', ' ', $type)}}
                </p>
            </div>
        </div>
        <div class="row">
            @if(!empty($programs))
                @foreach($programs as $program)
                    <div class="col-12 col-sm-6 col-md-3 slick_show">
                        @include('programs.common.series')
                    </div>
                @endforeach
            @endif
        </div>
        <div class="row">
            <div class="col-lg-12">
                {{ $programs
                    ->appends(request()->except('page'))
                    ->onEachSide(5)->links() }}
            </div>
        </div>
    </div>
@endsection
