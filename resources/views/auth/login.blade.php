@extends('layouts.app')
@section('content')
    <section class="container login_form" style="background: white">
        @include('auth.loginForm')
    </section>
@endsection
