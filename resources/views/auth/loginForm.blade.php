<div class="container login_form">
    <div class="row justify-content-center">
        <div class="col-4 modal-title-div">
            <p class="modal-title-custom">
                Log In
            </p>
        </div>
        <form method="POST"
              class="submitFormAjax" action="{{ route('login') }}">
            @csrf
            <div class="main_error_message_in_form">

            </div>
            <div class="form-group row">
                <label for="email"
                       class="col-md-12 col-form-label text-md-left">{{ __('E-Mail Address') }}</label>
                <div class="col-md-12">
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                    <div id="error_form_email" class="error-form-messages"></div>
                </div>
            </div>
            <div class="form-group row">
                <label for="password" class="col-md-12
                col-form-label text-md-left">{{ __('Password') }}</label>
                <div class="col-md-12">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                    <div id="error_form_password" class="error-form-messages"></div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label class="form-check-label" for="remember">
                            {{ __('Remember Me') }}
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group row mb-0">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-rounded btn-submit">
                        {{ __('Login') }}
                    </button>
                </div>
            </div>
        </form>
        <div class="col-12 margin-top-20">
            <p class="login-register-text text-center">
                New Member?
                <a href="{{route('register')}}"
                   class="purple-color call-global-modal">Sign Up</a>
            </p>
        </div>
    </div>
</div>
