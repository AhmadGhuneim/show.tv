<div class="container">
    <div class="row">
        <div class="col-12">
            <img src="{{asset('images/register icon.png')}}" class="img_no_auth" />
        </div>
        <div class="col-12">
            <p class="auth_message">
            You cannot complete this action,you have to register first
            </p>
        </div>
    </div>
    <div class="row margin-top-20">
        <div class="col-6">
            <p class="login-register-text text-center">
                <a href="{{route('login')}}"
                   class="btn btn-rounded btn-submit call-global-modal">Log In</a>
            </p>
            <p class="text-center">
                Have account?
            </p>
        </div>
        <div class="col-6">
            <p class="login-register-text text-center">
                <a href="{{route('register')}}"
                   class="btn btn-rounded btn-submit call-global-modal">Sign UP</a>
            </p>
            <p class="text-center">
                New Member?
            </p>
        </div>
    </div>
</div>
