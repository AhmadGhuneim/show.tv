<div class="container login_form">
    <div class="row justify-content-center">
        <div class="col-4 modal-title-div">
            <p class="modal-title-custom">
                Register
            </p>
        </div>
        <form method="POST"
              class="submitFormAjax"
              action="{{ route('register') }}">
                        @csrf

            <div class="main_error_message_in_form">

            </div>
            <div class="form-group row">
                <label for="user_image" class="col-md-12 col-form-label text-md-left">Profile Pic</label>
                <div class="col-md-12">
                    <input id="user_image"
                           type="file"
                           style="padding: 0"
                           class="form-control @error('user_image') is-invalid @enderror" name="user_image" required>
                    <div id="error_form_user_image" class="error-form-messages"></div>
                </div>
            </div>
            <div class="form-group row">
                <label for="name" class="col-md-12 col-form-label text-md-left">{{ __('Name') }}</label>
                <div class="col-md-12">
                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                    <div id="error_form_name" class="error-form-messages"></div>
                </div>
            </div>

            <div class="form-group row">
                <label for="email" class="col-md-12 col-form-label text-md-left">{{ __('E-Mail Address') }}</label>
                <div class="col-md-12">
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                    <div id="error_form_email" class="error-form-messages"></div>
                </div>
            </div>
            <div class="form-group row">
                <label for="password" class="col-md-12 col-form-label text-md-left">{{ __('Password') }}</label>
                <div class="col-md-12">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                    <div id="error_form_password" class="error-form-messages"></div>
                </div>
            </div>
            <div class="form-group row">
                <label for="password-confirm" class="col-md-12 col-form-label text-md-left">{{ __('Confirm Password') }}</label>
                <div class="col-md-12">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                </div>
            </div>
            <div class="form-group row mb-0">
                <div class="col-12 text-center">
                    <button type="submit" class="btn btn-rounded btn-submit">
                        {{ __('Register') }}
                    </button>
                </div>
            </div>
        </form>
        <div class="col-12 margin-top-20">
            <p class="login-register-text text-center">
                Already have an account?
                <a href="{{route('login')}}"
                   class="purple-color call-global-modal">Log In</a>
            </p>
        </div>
    </div>
</div>
