@extends('layouts.app')
@section('styles')
    <link href="{{asset('slider/ninja-slider.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('slider/thumbnail-slider.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('slickCarousel/slick.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('slickCarousel/slick-theme.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('slider/sliderCustom.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    @include('mainSlider')
    <section>
    <div class="container margin-top-50">
        <div class="row " >
            <div class="col-sm-12">
                <p class="text-center title-text">
                    Series for you
                </p>
            </div>
        </div>
            <ul class="nav slick_show">
                @if(!empty($series))
                    @foreach($series as $program)
                        <li >
                            @include('programs.common.series')
                        </li>
                    @endforeach

                @endif
            </ul>
    </div>
        <div class="container margin-top-50">
        <div class="row " >
            <div class="col-sm-12">
                <p class="text-center title-text">
                    TV Shows for you
                </p>
            </div>
        </div>
            <ul class="nav slick_show">
                @if(!empty($TvShows))
                    @foreach($TvShows as $program)
                        <li >
                            @include('programs.common.tvShows')
                        </li>
                    @endforeach
                @endif
            </ul>
    </div>
    </section>
@endsection
@section('scripts')
    <script src="{{asset('slider/ninja-slider.js')}}"></script>

    <script src="{{asset('slider/thumbnail-slider.js')}}"
            ></script>
    <script src="{{asset('slickCarousel/slick.min.js')}}"  type="text/javascript" charset="utf-8"></script>
    <script src="{{asset('js/homePage.js')}}"></script>
@endsection
