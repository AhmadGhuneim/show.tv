<section>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12" style="padding: 0">
                <div class="slider_area">
                    <div id="ninja-slider" style="float:left;">
                        <div class="slider-inner">
                            <ul>
                                @if(!empty($TvShows))
                                    @foreach($TvShows->take(5) as $tvShow)
                                        <li>
                                            <a class="ns-img"
                                               href="{{asset('storage/'.$tvShow->main_image)}}">

                                                <div class="slider-text-container">
                                                    <div class="slider-text-content">
                                                        <p class="slider-text-title">
                                                            {{$tvShow->title}}
                                                        </p>
                                                        <p class="title-text">
                                                            {{arrangeProgramShowingDays($tvShow)}} {{date('H:i',strtotime($tvShow->broadcast_start_time))}}
                                                        </p>
                                                        <p>
                                                            <button
                                                                    data-link="{{route('programs.show',$tvShow->getUrlParams())}}}}"
                                                                    class="btn btn-rounded btn_watch_now go_to_link">
                                                                Watch now
                                                            </button>
                                                            @auth
                                                                <button
                                                                        data-link="{{route('addToFavourite',['program_id'=>$tvShow->id])}}"
                                                                        data-change="img.attr.src"
                                                                        class="btn btn-rounded btn_img_heart btn_change_inner_html">
                                                                    @if(!Auth::guest() &&  auth()->user()->checkFollowingProgram($tvShow->id))
                                                                        <img src="{{asset('images/Heart icon.png')}}" class="img-responsive heart_btn" />
                                                                    @else
                                                                        <img src="{{asset('images/heart copy 2.svg')}}" class="img-responsive heart_btn" />
                                                                    @endif
                                                                </button>
                                                            @else
                                                                <button
                                                                        data-link="{{route('noAuthView')}}"
                                                                        class="btn btn-rounded call-global-modal btn_img_heart">
                                                                    <img src="{{asset('images/heart copy 2.svg')}}" class="img-responsive heart_btn" />
                                                                </button>
                                                            @endauth
                                                        </p>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                    <div id="thumbnail-slider" style="float:left;">
                        <div class="inner">
                            <ul>
                                @if(!empty($TvShows))
                                    @foreach($TvShows->take(5) as $tvShow)
                                        <li>
                                            <a class="thumb"
                                               href="{{asset('storage/'.$tvShow->main_image)}}"></a>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                    <div style="clear:both;"></div>
                </div>
            </div>
        </div>
    </div>
</section>
