-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 27, 2020 at 02:56 PM
-- Server version: 5.7.30-0ubuntu0.18.04.1
-- PHP Version: 7.3.19-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `show.tv`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_menu`
--

CREATE TABLE `admin_menu` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uri` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permission` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_menu`
--

INSERT INTO `admin_menu` (`id`, `parent_id`, `order`, `title`, `icon`, `uri`, `permission`, `created_at`, `updated_at`) VALUES
(1, 0, 1, 'Dashboard', 'fa-bar-chart', '/', NULL, NULL, NULL),
(2, 0, 2, 'Admin', 'fa-tasks', '', NULL, NULL, NULL),
(3, 2, 3, 'Users', 'fa-users', 'auth/users', NULL, NULL, NULL),
(4, 2, 4, 'Roles', 'fa-user', 'auth/roles', NULL, NULL, NULL),
(5, 2, 5, 'Permission', 'fa-ban', 'auth/permissions', NULL, NULL, NULL),
(6, 2, 6, 'Menu', 'fa-bars', 'auth/menu', NULL, NULL, NULL),
(7, 2, 7, 'Operation log', 'fa-history', 'auth/logs', NULL, NULL, NULL),
(8, 0, 8, 'users', 'fa-user', 'users', '*', '2020-06-23 06:01:40', '2020-06-23 06:01:54'),
(9, 0, 9, 'programs', 'fa-product-hunt', 'programs', '*', '2020-06-23 06:02:11', '2020-06-23 06:02:17'),
(10, 0, 10, 'Episodes', 'fa-codiepie', 'program-episodes', '*', '2020-06-23 06:03:12', '2020-06-23 06:03:18');

-- --------------------------------------------------------

--
-- Table structure for table `admin_operation_log`
--

CREATE TABLE `admin_operation_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `input` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_operation_log`
--

INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin/auth/login', 'GET', '127.0.0.1', '[]', '2020-06-22 18:25:14', '2020-06-22 18:25:14'),
(2, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-06-22 18:25:14', '2020-06-22 18:25:14'),
(3, 1, 'admin/programs/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:27:56', '2020-06-22 18:27:56'),
(4, 1, 'admin/programs', 'GET', '127.0.0.1', '[]', '2020-06-22 18:27:59', '2020-06-22 18:27:59'),
(5, 1, 'admin/programs/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-22 18:28:01', '2020-06-22 18:28:01'),
(6, 1, 'admin/programs/create', 'GET', '127.0.0.1', '[]', '2020-06-22 18:28:39', '2020-06-22 18:28:39'),
(7, 1, 'admin/programs/create', 'GET', '127.0.0.1', '[]', '2020-06-22 18:28:41', '2020-06-22 18:28:41'),
(8, 1, 'admin/programs/create', 'GET', '127.0.0.1', '[]', '2020-06-22 18:28:53', '2020-06-22 18:28:53'),
(9, 1, 'admin/programs', 'POST', '127.0.0.1', '{\"title\":\"sasda\",\"description\":\"addsa\",\"broadcast_start_date\":\"2020-06-22\",\"broadCastDaysProgram\":[\"2\",null],\"broadcast_start_time\":\"21:28:00\",\"broadcast_end_time\":\"21:28:00\",\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"Vy9fRpsqnRqhl9NHABiRrSf1hdMRHnnyStoz7gtG\"}', '2020-06-22 18:29:06', '2020-06-22 18:29:06'),
(10, 1, 'admin/programs/create', 'GET', '127.0.0.1', '[]', '2020-06-22 18:29:06', '2020-06-22 18:29:06'),
(11, 1, 'admin/programs', 'POST', '127.0.0.1', '{\"title\":\"sasda\",\"description\":\"addsa\",\"broadcast_start_date\":\"2020-06-22\",\"broadCastDaysProgram\":[\"2\",null],\"broadcast_start_time\":\"21:28:00\",\"broadcast_end_time\":\"21:28:00\",\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"Vy9fRpsqnRqhl9NHABiRrSf1hdMRHnnyStoz7gtG\"}', '2020-06-22 18:29:44', '2020-06-22 18:29:44'),
(12, 1, 'admin/programs/create', 'GET', '127.0.0.1', '[]', '2020-06-22 18:29:44', '2020-06-22 18:29:44'),
(13, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-22 18:29:58', '2020-06-22 18:29:58'),
(14, 1, 'admin/programs', 'GET', '127.0.0.1', '[]', '2020-06-22 18:30:02', '2020-06-22 18:30:02'),
(15, 1, 'admin/programs/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-22 18:30:03', '2020-06-22 18:30:03'),
(16, 1, 'admin/programs', 'POST', '127.0.0.1', '{\"title\":\"ssdadsa\",\"description\":\"sdadsadassda\",\"broadcast_start_date\":\"2020-06-22\",\"broadCastDaysProgram\":[\"2\",null],\"broadcast_start_time\":\"21:30:00\",\"broadcast_end_time\":\"21:30:00\",\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"Vy9fRpsqnRqhl9NHABiRrSf1hdMRHnnyStoz7gtG\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/programs\"}', '2020-06-22 18:30:09', '2020-06-22 18:30:09'),
(17, 1, 'admin/programs/create', 'GET', '127.0.0.1', '[]', '2020-06-22 18:30:10', '2020-06-22 18:30:10'),
(18, 1, 'admin/programs', 'POST', '127.0.0.1', '{\"title\":\"ssdadsa\",\"description\":\"sdadsadassda\",\"broadcast_start_date\":\"2020-06-22\",\"broadCastDaysProgram\":[\"2\",\"3\",null],\"broadcast_start_time\":\"21:30:00\",\"broadcast_end_time\":\"21:30:00\",\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"Vy9fRpsqnRqhl9NHABiRrSf1hdMRHnnyStoz7gtG\"}', '2020-06-22 18:30:31', '2020-06-22 18:30:31'),
(19, 1, 'admin/programs', 'GET', '127.0.0.1', '[]', '2020-06-22 18:30:31', '2020-06-22 18:30:31'),
(20, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-22 18:30:42', '2020-06-22 18:30:42'),
(21, 1, 'admin/programs/4', 'PUT', '127.0.0.1', '{\"title\":\"ssdadsa\",\"description\":\"sdadsadassda\",\"broadcast_start_date\":\"2020-06-22\",\"broadCastDaysProgram\":[\"4\",null],\"broadcast_start_time\":\"21:30:00\",\"broadcast_end_time\":\"21:30:00\",\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"Vy9fRpsqnRqhl9NHABiRrSf1hdMRHnnyStoz7gtG\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/programs\"}', '2020-06-22 18:30:57', '2020-06-22 18:30:57'),
(22, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:30:58', '2020-06-22 18:30:58'),
(23, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:30:58', '2020-06-22 18:30:58'),
(24, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:30:58', '2020-06-22 18:30:58'),
(25, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:30:58', '2020-06-22 18:30:58'),
(26, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:30:58', '2020-06-22 18:30:58'),
(27, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:30:58', '2020-06-22 18:30:58'),
(28, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:30:58', '2020-06-22 18:30:58'),
(29, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:30:59', '2020-06-22 18:30:59'),
(30, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:30:59', '2020-06-22 18:30:59'),
(31, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:30:59', '2020-06-22 18:30:59'),
(32, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:30:59', '2020-06-22 18:30:59'),
(33, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:30:59', '2020-06-22 18:30:59'),
(34, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:30:59', '2020-06-22 18:30:59'),
(35, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:30:59', '2020-06-22 18:30:59'),
(36, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:30:59', '2020-06-22 18:30:59'),
(37, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:31:00', '2020-06-22 18:31:00'),
(38, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:31:00', '2020-06-22 18:31:00'),
(39, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:31:00', '2020-06-22 18:31:00'),
(40, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:31:00', '2020-06-22 18:31:00'),
(41, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:31:00', '2020-06-22 18:31:00'),
(42, 1, 'admin/programs/4', 'PUT', '127.0.0.1', '{\"title\":\"ssdadsa\",\"description\":\"sdadsadassda\",\"broadcast_start_date\":\"2020-06-22\",\"broadCastDaysProgram\":[\"4\",null],\"broadcast_start_time\":\"21:30:00\",\"broadcast_end_time\":\"21:30:00\",\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"Vy9fRpsqnRqhl9NHABiRrSf1hdMRHnnyStoz7gtG\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/programs\"}', '2020-06-22 18:31:04', '2020-06-22 18:31:04'),
(43, 1, 'admin/programs/4', 'GET', '127.0.0.1', '[]', '2020-06-22 18:31:04', '2020-06-22 18:31:04'),
(44, 1, 'admin/programs', 'GET', '127.0.0.1', '[]', '2020-06-22 18:31:11', '2020-06-22 18:31:11'),
(45, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-22 18:31:14', '2020-06-22 18:31:14'),
(46, 1, 'admin/programs', 'GET', '127.0.0.1', '[]', '2020-06-22 18:31:14', '2020-06-22 18:31:14'),
(47, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-22 18:31:44', '2020-06-22 18:31:44'),
(48, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:31:49', '2020-06-22 18:31:49'),
(49, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:37:12', '2020-06-22 18:37:12'),
(50, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:37:15', '2020-06-22 18:37:15'),
(51, 1, 'admin/programs/4', 'PUT', '127.0.0.1', '{\"title\":\"ssdadsa\",\"description\":\"sdadsadassda\",\"broadcast_start_date\":\"2020-06-22\",\"broadCastDaysProgram\":[\"2\",\"3\",\"4\",null],\"broadcast_start_time\":\"21:30:00\",\"broadcast_end_time\":\"21:30:00\",\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"Vy9fRpsqnRqhl9NHABiRrSf1hdMRHnnyStoz7gtG\",\"_method\":\"PUT\"}', '2020-06-22 18:37:18', '2020-06-22 18:37:18'),
(52, 1, 'admin/programs', 'GET', '127.0.0.1', '[]', '2020-06-22 18:37:18', '2020-06-22 18:37:18'),
(53, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-22 18:37:20', '2020-06-22 18:37:20'),
(54, 1, 'admin/programs/4', 'PUT', '127.0.0.1', '{\"title\":\"ssdadsa\",\"description\":\"sdadsadassda\",\"broadcast_start_date\":\"2020-06-22\",\"broadCastDaysProgram\":[\"2\",\"3\",\"4\",null],\"broadcast_start_time\":\"21:30:00\",\"broadcast_end_time\":\"21:30:00\",\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"Vy9fRpsqnRqhl9NHABiRrSf1hdMRHnnyStoz7gtG\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/programs\"}', '2020-06-22 18:37:28', '2020-06-22 18:37:28'),
(55, 1, 'admin/programs', 'GET', '127.0.0.1', '[]', '2020-06-22 18:37:28', '2020-06-22 18:37:28'),
(56, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-22 18:37:30', '2020-06-22 18:37:30'),
(57, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:42:00', '2020-06-22 18:42:00'),
(58, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:42:04', '2020-06-22 18:42:04'),
(59, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:42:22', '2020-06-22 18:42:22'),
(60, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:42:33', '2020-06-22 18:42:33'),
(61, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:42:44', '2020-06-22 18:42:44'),
(62, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:42:45', '2020-06-22 18:42:45'),
(63, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:42:45', '2020-06-22 18:42:45'),
(64, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:42:46', '2020-06-22 18:42:46'),
(65, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:42:56', '2020-06-22 18:42:56'),
(66, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:42:57', '2020-06-22 18:42:57'),
(67, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:42:57', '2020-06-22 18:42:57'),
(68, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:42:57', '2020-06-22 18:42:57'),
(69, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:42:57', '2020-06-22 18:42:57'),
(70, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:42:57', '2020-06-22 18:42:57'),
(71, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:43:10', '2020-06-22 18:43:10'),
(72, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:43:17', '2020-06-22 18:43:17'),
(73, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:44:02', '2020-06-22 18:44:02'),
(74, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:44:11', '2020-06-22 18:44:11'),
(75, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:44:12', '2020-06-22 18:44:12'),
(76, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:44:12', '2020-06-22 18:44:12'),
(77, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:44:12', '2020-06-22 18:44:12'),
(78, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:44:12', '2020-06-22 18:44:12'),
(79, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:44:12', '2020-06-22 18:44:12'),
(80, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:44:25', '2020-06-22 18:44:25'),
(81, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:44:56', '2020-06-22 18:44:56'),
(82, 1, 'admin/programs/4', 'PUT', '127.0.0.1', '{\"title\":\"ssdadsa\",\"description\":\"sdadsadassda\",\"broadcast_start_date\":\"2020-06-22\",\"broadCastDaysProgram\":[\"2\",\"3\",\"4\",null],\"broadcast_start_time\":\"21:30:00\",\"broadcast_end_time\":\"21:30:00\",\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"Vy9fRpsqnRqhl9NHABiRrSf1hdMRHnnyStoz7gtG\",\"_method\":\"PUT\"}', '2020-06-22 18:45:01', '2020-06-22 18:45:01'),
(83, 1, 'admin/programs', 'GET', '127.0.0.1', '[]', '2020-06-22 18:45:01', '2020-06-22 18:45:01'),
(84, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-22 18:45:04', '2020-06-22 18:45:04'),
(85, 1, 'admin/programs/4', 'PUT', '127.0.0.1', '{\"title\":\"ssdadsa\",\"description\":\"sdadsadassda\",\"broadcast_start_date\":\"2020-06-22\",\"broadCastDaysProgram\":[null],\"broadcast_start_time\":\"21:30:00\",\"broadcast_end_time\":\"21:30:00\",\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"Vy9fRpsqnRqhl9NHABiRrSf1hdMRHnnyStoz7gtG\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/programs\"}', '2020-06-22 18:45:12', '2020-06-22 18:45:12'),
(86, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:45:12', '2020-06-22 18:45:12'),
(87, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:46:19', '2020-06-22 18:46:19'),
(88, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:46:39', '2020-06-22 18:46:39'),
(89, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:47:07', '2020-06-22 18:47:07'),
(90, 1, 'admin/programs/4', 'PUT', '127.0.0.1', '{\"title\":\"ssdadsa\",\"description\":\"sdadsadassda\",\"broadcast_start_date\":\"2020-06-22\",\"broadcast_start_time\":\"21:30:00\",\"broadcast_end_time\":\"21:30:00\",\"broadCastDaysProgram\":[null],\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"Vy9fRpsqnRqhl9NHABiRrSf1hdMRHnnyStoz7gtG\",\"_method\":\"PUT\"}', '2020-06-22 18:47:11', '2020-06-22 18:47:11'),
(91, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:47:11', '2020-06-22 18:47:11'),
(92, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:47:41', '2020-06-22 18:47:41'),
(93, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:48:25', '2020-06-22 18:48:25'),
(94, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:48:58', '2020-06-22 18:48:58'),
(95, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:49:13', '2020-06-22 18:49:13'),
(96, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:50:34', '2020-06-22 18:50:34'),
(97, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:50:48', '2020-06-22 18:50:48'),
(98, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:51:14', '2020-06-22 18:51:14'),
(99, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:51:25', '2020-06-22 18:51:25'),
(100, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:53:38', '2020-06-22 18:53:38'),
(101, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:53:47', '2020-06-22 18:53:47'),
(102, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:54:17', '2020-06-22 18:54:17'),
(103, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:54:26', '2020-06-22 18:54:26'),
(104, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:54:40', '2020-06-22 18:54:40'),
(105, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:54:49', '2020-06-22 18:54:49'),
(106, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:55:01', '2020-06-22 18:55:01'),
(107, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:55:26', '2020-06-22 18:55:26'),
(108, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:55:43', '2020-06-22 18:55:43'),
(109, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:55:57', '2020-06-22 18:55:57'),
(110, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:56:15', '2020-06-22 18:56:15'),
(111, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:56:20', '2020-06-22 18:56:20'),
(112, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:56:53', '2020-06-22 18:56:53'),
(113, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:57:04', '2020-06-22 18:57:04'),
(114, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:58:29', '2020-06-22 18:58:29'),
(115, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:58:45', '2020-06-22 18:58:45'),
(116, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:59:00', '2020-06-22 18:59:00'),
(117, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:59:23', '2020-06-22 18:59:23'),
(118, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 18:59:47', '2020-06-22 18:59:47'),
(119, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 19:00:04', '2020-06-22 19:00:04'),
(120, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 19:00:32', '2020-06-22 19:00:32'),
(121, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 19:01:16', '2020-06-22 19:01:16'),
(122, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 19:04:32', '2020-06-22 19:04:32'),
(123, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 19:05:12', '2020-06-22 19:05:12'),
(124, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-22 19:08:15', '2020-06-22 19:08:15'),
(125, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-06-23 05:57:57', '2020-06-23 05:57:57'),
(126, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 06:01:13', '2020-06-23 06:01:13'),
(127, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"users\",\"icon\":\"fa-user\",\"uri\":\"users\",\"roles\":[\"1\",null],\"permission\":\"*\",\"_token\":\"u71AIhvLXjmW0DBAU5TMfiUGJBxxp21VJsOxDqJL\"}', '2020-06-23 06:01:40', '2020-06-23 06:01:40'),
(128, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-06-23 06:01:40', '2020-06-23 06:01:40'),
(129, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"_token\":\"u71AIhvLXjmW0DBAU5TMfiUGJBxxp21VJsOxDqJL\",\"_order\":\"[{\\\"id\\\":1},{\\\"id\\\":2,\\\"children\\\":[{\\\"id\\\":3},{\\\"id\\\":4},{\\\"id\\\":5},{\\\"id\\\":6},{\\\"id\\\":7}]},{\\\"id\\\":8}]\"}', '2020-06-23 06:01:53', '2020-06-23 06:01:53'),
(130, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 06:01:54', '2020-06-23 06:01:54'),
(131, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"programs\",\"icon\":\"fa-product-hunt\",\"uri\":\"programs\",\"roles\":[\"1\",null],\"permission\":\"*\",\"_token\":\"u71AIhvLXjmW0DBAU5TMfiUGJBxxp21VJsOxDqJL\"}', '2020-06-23 06:02:10', '2020-06-23 06:02:10'),
(132, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-06-23 06:02:11', '2020-06-23 06:02:11'),
(133, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"_token\":\"u71AIhvLXjmW0DBAU5TMfiUGJBxxp21VJsOxDqJL\",\"_order\":\"[{\\\"id\\\":1},{\\\"id\\\":2,\\\"children\\\":[{\\\"id\\\":3},{\\\"id\\\":4},{\\\"id\\\":5},{\\\"id\\\":6},{\\\"id\\\":7}]},{\\\"id\\\":8},{\\\"id\\\":9}]\"}', '2020-06-23 06:02:17', '2020-06-23 06:02:17'),
(134, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 06:02:17', '2020-06-23 06:02:17'),
(135, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"Episodes\",\"icon\":\"fa-codiepie\",\"uri\":\"program-episodes\",\"roles\":[\"1\",null],\"permission\":\"*\",\"_token\":\"u71AIhvLXjmW0DBAU5TMfiUGJBxxp21VJsOxDqJL\"}', '2020-06-23 06:03:11', '2020-06-23 06:03:11'),
(136, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-06-23 06:03:12', '2020-06-23 06:03:12'),
(137, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"_token\":\"u71AIhvLXjmW0DBAU5TMfiUGJBxxp21VJsOxDqJL\",\"_order\":\"[{\\\"id\\\":1},{\\\"id\\\":2,\\\"children\\\":[{\\\"id\\\":3},{\\\"id\\\":4},{\\\"id\\\":5},{\\\"id\\\":6},{\\\"id\\\":7}]},{\\\"id\\\":8},{\\\"id\\\":9},{\\\"id\\\":10}]\"}', '2020-06-23 06:03:18', '2020-06-23 06:03:18'),
(138, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 06:03:18', '2020-06-23 06:03:18'),
(139, 1, 'admin/programs', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 06:03:20', '2020-06-23 06:03:20'),
(140, 1, 'admin/programs', 'GET', '127.0.0.1', '[]', '2020-06-23 06:09:21', '2020-06-23 06:09:21'),
(141, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 06:09:23', '2020-06-23 06:09:23'),
(142, 1, 'admin/programs', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 06:10:00', '2020-06-23 06:10:00'),
(143, 1, 'admin/programs', 'GET', '127.0.0.1', '[]', '2020-06-23 06:10:10', '2020-06-23 06:10:10'),
(144, 1, 'admin/programs', 'GET', '127.0.0.1', '[]', '2020-06-23 06:11:31', '2020-06-23 06:11:31'),
(145, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 06:12:22', '2020-06-23 06:12:22'),
(146, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 06:13:29', '2020-06-23 06:13:29'),
(147, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 06:13:39', '2020-06-23 06:13:39'),
(148, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 06:14:02', '2020-06-23 06:14:02'),
(149, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 06:14:09', '2020-06-23 06:14:09'),
(150, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 06:14:19', '2020-06-23 06:14:19'),
(151, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 06:14:20', '2020-06-23 06:14:20'),
(152, 1, 'admin/programs/4', 'PUT', '127.0.0.1', '{\"programType\":\"2\",\"title\":\"ssdadsa\",\"description\":\"sdadsadassda\",\"broadcast_start_date\":\"2020-06-22\",\"broadcast_start_time\":\"21:30:00\",\"broadcast_end_time\":\"21:30:00\",\"broadCastDaysProgram\":[\"2\",\"3\",\"4\",null],\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"u71AIhvLXjmW0DBAU5TMfiUGJBxxp21VJsOxDqJL\",\"_method\":\"PUT\"}', '2020-06-23 06:14:24', '2020-06-23 06:14:24'),
(153, 1, 'admin/programs', 'GET', '127.0.0.1', '[]', '2020-06-23 06:14:24', '2020-06-23 06:14:24'),
(154, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 06:14:28', '2020-06-23 06:14:28'),
(155, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 06:15:01', '2020-06-23 06:15:01'),
(156, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 06:15:02', '2020-06-23 06:15:02'),
(157, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 06:15:21', '2020-06-23 06:15:21'),
(158, 1, 'admin/programs/4', 'PUT', '127.0.0.1', '{\"program_type_id\":\"2\",\"title\":\"ssdadsa\",\"description\":\"sdadsadassda\",\"broadcast_start_date\":\"2020-06-22\",\"broadcast_start_time\":\"21:30:00\",\"broadcast_end_time\":\"21:30:00\",\"broadCastDaysProgram\":[\"2\",\"3\",\"4\",null],\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"u71AIhvLXjmW0DBAU5TMfiUGJBxxp21VJsOxDqJL\",\"_method\":\"PUT\"}', '2020-06-23 06:15:24', '2020-06-23 06:15:24'),
(159, 1, 'admin/programs', 'GET', '127.0.0.1', '[]', '2020-06-23 06:15:25', '2020-06-23 06:15:25'),
(160, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 06:15:28', '2020-06-23 06:15:28'),
(161, 1, 'admin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 06:17:05', '2020-06-23 06:17:05'),
(162, 1, 'admin/program-episodes', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 06:17:07', '2020-06-23 06:17:07'),
(163, 1, 'admin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 06:17:50', '2020-06-23 06:17:50'),
(164, 1, 'admin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 06:19:07', '2020-06-23 06:19:07'),
(165, 1, 'admin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 06:20:06', '2020-06-23 06:20:06'),
(166, 1, 'admin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 06:24:53', '2020-06-23 06:24:53'),
(167, 1, 'admin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 06:26:28', '2020-06-23 06:26:28'),
(168, 1, 'admin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 06:28:44', '2020-06-23 06:28:44'),
(169, 1, 'admin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 06:30:45', '2020-06-23 06:30:45'),
(170, 1, 'admin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 06:31:31', '2020-06-23 06:31:31'),
(171, 1, 'admin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 06:32:12', '2020-06-23 06:32:12'),
(172, 1, 'admin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 06:34:52', '2020-06-23 06:34:52'),
(173, 1, 'admin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 06:36:29', '2020-06-23 06:36:29'),
(174, 1, 'admin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 06:36:50', '2020-06-23 06:36:50'),
(175, 1, 'admin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 06:37:34', '2020-06-23 06:37:34'),
(176, 1, 'admin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-23 06:37:36', '2020-06-23 06:37:36'),
(177, 1, 'admin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 06:39:02', '2020-06-23 06:39:02'),
(178, 1, 'admin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-23 06:40:03', '2020-06-23 06:40:03'),
(179, 1, 'admin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 06:41:20', '2020-06-23 06:41:20'),
(180, 1, 'admin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-23 06:41:22', '2020-06-23 06:41:22'),
(181, 1, 'admin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 06:41:57', '2020-06-23 06:41:57'),
(182, 1, 'admin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-23 06:41:59', '2020-06-23 06:41:59'),
(183, 1, 'admin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 06:42:08', '2020-06-23 06:42:08'),
(184, 1, 'admin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-23 06:42:09', '2020-06-23 06:42:09'),
(185, 1, 'admin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 06:42:14', '2020-06-23 06:42:14'),
(186, 1, 'admin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-23 06:42:17', '2020-06-23 06:42:17'),
(187, 1, 'admin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 06:42:50', '2020-06-23 06:42:50'),
(188, 1, 'admin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-23 06:42:52', '2020-06-23 06:42:52'),
(189, 1, 'admin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 06:42:57', '2020-06-23 06:42:57'),
(190, 1, 'admin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-23 06:42:59', '2020-06-23 06:42:59'),
(191, 1, 'admin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 06:43:27', '2020-06-23 06:43:27'),
(192, 1, 'admin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-23 06:43:29', '2020-06-23 06:43:29'),
(193, 1, 'admin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 06:43:45', '2020-06-23 06:43:45'),
(194, 1, 'admin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-23 06:43:47', '2020-06-23 06:43:47'),
(195, 1, 'admin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 06:43:59', '2020-06-23 06:43:59'),
(196, 1, 'admin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-23 06:44:00', '2020-06-23 06:44:00'),
(197, 1, 'admin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 06:45:28', '2020-06-23 06:45:28'),
(198, 1, 'admin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-23 06:45:30', '2020-06-23 06:45:30'),
(199, 1, 'admin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 06:45:53', '2020-06-23 06:45:53'),
(200, 1, 'admin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-23 06:45:54', '2020-06-23 06:45:54'),
(201, 1, 'admin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 06:46:03', '2020-06-23 06:46:03'),
(202, 1, 'admin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-23 06:46:04', '2020-06-23 06:46:04'),
(203, 1, 'admin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 06:47:48', '2020-06-23 06:47:48'),
(204, 1, 'admin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-23 06:47:50', '2020-06-23 06:47:50'),
(205, 1, 'admin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 06:51:39', '2020-06-23 06:51:39'),
(206, 1, 'admin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-23 06:51:43', '2020-06-23 06:51:43'),
(207, 1, 'admin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 06:52:34', '2020-06-23 06:52:34'),
(208, 1, 'admin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-23 06:52:37', '2020-06-23 06:52:37'),
(209, 1, 'admin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 07:02:31', '2020-06-23 07:02:31'),
(210, 1, 'admin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 07:08:40', '2020-06-23 07:08:40'),
(211, 1, 'admin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-23 07:08:43', '2020-06-23 07:08:43'),
(212, 1, 'admin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"3\",\"broadcast_date\":\"2020-06-30\",\"episode_no\":\"1\",\"title\":\"Test\",\"description\":\"adsadsda\\r\\nsdasddsa\\r\\ndsasad\\r\\nda\\r\\ndas\",\"_token\":\"u71AIhvLXjmW0DBAU5TMfiUGJBxxp21VJsOxDqJL\"}', '2020-06-23 07:28:42', '2020-06-23 07:28:42'),
(213, 1, 'admin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-23 07:28:56', '2020-06-23 07:28:56'),
(214, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 07:28:59', '2020-06-23 07:28:59'),
(215, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 07:30:10', '2020-06-23 07:30:10'),
(216, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 07:30:25', '2020-06-23 07:30:25'),
(217, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 07:30:38', '2020-06-23 07:30:38'),
(218, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 07:30:50', '2020-06-23 07:30:50'),
(219, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 07:31:26', '2020-06-23 07:31:26'),
(220, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 07:32:05', '2020-06-23 07:32:05'),
(221, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 07:33:09', '2020-06-23 07:33:09'),
(222, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 07:33:57', '2020-06-23 07:33:57'),
(223, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 07:34:06', '2020-06-23 07:34:06'),
(224, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 07:34:31', '2020-06-23 07:34:31'),
(225, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 07:35:10', '2020-06-23 07:35:10'),
(226, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 07:45:34', '2020-06-23 07:45:34'),
(227, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 07:45:47', '2020-06-23 07:45:47'),
(228, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 07:46:59', '2020-06-23 07:46:59'),
(229, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 07:47:11', '2020-06-23 07:47:11'),
(230, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 07:47:17', '2020-06-23 07:47:17'),
(231, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 07:48:01', '2020-06-23 07:48:01'),
(232, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 07:48:06', '2020-06-23 07:48:06'),
(233, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 07:48:27', '2020-06-23 07:48:27'),
(234, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 07:48:39', '2020-06-23 07:48:39'),
(235, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 07:48:49', '2020-06-23 07:48:49'),
(236, 1, 'admin/program-episodes/1', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"3\",\"broadcast_date\":\"2020-06-30\",\"episode_no\":\"1\",\"title\":\"Test\",\"description\":\"adsadsda\\r\\nsdasddsa\\r\\ndsasad\\r\\nda\\r\\ndas\",\"_token\":\"u71AIhvLXjmW0DBAU5TMfiUGJBxxp21VJsOxDqJL\",\"_method\":\"PUT\"}', '2020-06-23 07:48:54', '2020-06-23 07:48:54'),
(237, 1, 'admin/program-episodes/1', 'GET', '127.0.0.1', '[]', '2020-06-23 07:50:18', '2020-06-23 07:50:18'),
(238, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 07:50:21', '2020-06-23 07:50:21'),
(239, 1, 'admin/program-episodes/1', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"3\",\"broadcast_date\":\"2020-06-30\",\"episode_no\":\"1\",\"title\":\"Test\",\"description\":\"adsadsda\\r\\nsdasddsa\\r\\ndsasad\\r\\nda\\r\\ndas\",\"_token\":\"u71AIhvLXjmW0DBAU5TMfiUGJBxxp21VJsOxDqJL\",\"_method\":\"PUT\"}', '2020-06-23 07:50:25', '2020-06-23 07:50:25'),
(240, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 07:51:05', '2020-06-23 07:51:05'),
(241, 1, 'admin/program-episodes/1', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"3\",\"broadcast_date\":\"2020-06-30\",\"episode_no\":\"1\",\"title\":\"Test\",\"description\":\"adsadsda\\r\\nsdasddsa\\r\\ndsasad\\r\\nda\\r\\ndas\",\"_token\":\"u71AIhvLXjmW0DBAU5TMfiUGJBxxp21VJsOxDqJL\",\"_method\":\"PUT\"}', '2020-06-23 07:51:07', '2020-06-23 07:51:07'),
(242, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 07:51:32', '2020-06-23 07:51:32'),
(243, 1, 'admin/program-episodes/1', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"3\",\"broadcast_date\":\"2020-06-30\",\"episode_no\":\"1\",\"title\":\"Test\",\"description\":\"adsadsda\\r\\nsdasddsa\\r\\ndsasad\\r\\nda\\r\\ndas\",\"_token\":\"u71AIhvLXjmW0DBAU5TMfiUGJBxxp21VJsOxDqJL\",\"_method\":\"PUT\"}', '2020-06-23 07:51:34', '2020-06-23 07:51:34'),
(244, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 07:52:09', '2020-06-23 07:52:09'),
(245, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 07:52:10', '2020-06-23 07:52:10'),
(246, 1, 'admin/program-episodes/1', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"3\",\"broadcast_date\":\"2020-06-30\",\"episode_no\":\"1\",\"title\":\"Test\",\"description\":\"adsadsda\\r\\nsdasddsa\\r\\ndsasad\\r\\nda\\r\\ndas\",\"_token\":\"u71AIhvLXjmW0DBAU5TMfiUGJBxxp21VJsOxDqJL\",\"_method\":\"PUT\"}', '2020-06-23 07:52:13', '2020-06-23 07:52:13'),
(247, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 07:52:31', '2020-06-23 07:52:31'),
(248, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 07:52:32', '2020-06-23 07:52:32'),
(249, 1, 'admin/program-episodes/1', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"3\",\"broadcast_date\":\"2020-06-30\",\"episode_no\":\"1\",\"title\":\"Test\",\"description\":\"adsadsda\\r\\nsdasddsa\\r\\ndsasad\\r\\nda\\r\\ndas\",\"_token\":\"u71AIhvLXjmW0DBAU5TMfiUGJBxxp21VJsOxDqJL\",\"_method\":\"PUT\"}', '2020-06-23 07:52:34', '2020-06-23 07:52:34'),
(250, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 07:53:51', '2020-06-23 07:53:51'),
(251, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 07:53:52', '2020-06-23 07:53:52'),
(252, 1, 'admin/program-episodes/1', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"3\",\"broadcast_date\":\"2020-06-30\",\"episode_no\":\"1\",\"title\":\"Test\",\"description\":\"adsadsda\\r\\nsdasddsa\\r\\ndsasad\\r\\nda\\r\\ndas\",\"_token\":\"u71AIhvLXjmW0DBAU5TMfiUGJBxxp21VJsOxDqJL\",\"_method\":\"PUT\"}', '2020-06-23 07:53:54', '2020-06-23 07:53:54'),
(253, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 07:54:14', '2020-06-23 07:54:14'),
(254, 1, 'admin/program-episodes/1', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"3\",\"broadcast_date\":\"2020-06-30\",\"episode_no\":\"1\",\"title\":\"Test\",\"description\":\"adsadsda\\r\\nsdasddsa\\r\\ndsasad\\r\\nda\\r\\ndas\",\"_token\":\"u71AIhvLXjmW0DBAU5TMfiUGJBxxp21VJsOxDqJL\",\"_method\":\"PUT\"}', '2020-06-23 07:54:16', '2020-06-23 07:54:16'),
(255, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 07:54:16', '2020-06-23 07:54:16'),
(256, 1, 'admin/program-episodes/1', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"3\",\"broadcast_date\":\"2020-06-30\",\"episode_no\":\"1\",\"title\":\"Test\",\"description\":\"adsadsda\\r\\nsdasddsa\\r\\ndsasad\\r\\nda\\r\\ndas\",\"_token\":\"u71AIhvLXjmW0DBAU5TMfiUGJBxxp21VJsOxDqJL\",\"_method\":\"PUT\"}', '2020-06-23 07:54:34', '2020-06-23 07:54:34'),
(257, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 07:54:49', '2020-06-23 07:54:49'),
(258, 1, 'admin/program-episodes/1', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"3\",\"broadcast_date\":\"2020-06-30\",\"episode_no\":\"1\",\"title\":\"Test\",\"description\":\"adsadsda\\r\\nsdasddsa\\r\\ndsasad\\r\\nda\\r\\ndas\",\"_token\":\"u71AIhvLXjmW0DBAU5TMfiUGJBxxp21VJsOxDqJL\",\"_method\":\"PUT\"}', '2020-06-23 07:55:00', '2020-06-23 07:55:00'),
(259, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 07:55:00', '2020-06-23 07:55:00'),
(260, 1, 'admin/program-episodes/1', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"3\",\"broadcast_date\":\"2020-06-30\",\"episode_no\":\"1\",\"title\":\"Test\",\"description\":\"adsadsda\\r\\nsdasddsa\\r\\ndsasad\\r\\nda\\r\\ndas\",\"_token\":\"u71AIhvLXjmW0DBAU5TMfiUGJBxxp21VJsOxDqJL\",\"_method\":\"PUT\"}', '2020-06-23 07:55:47', '2020-06-23 07:55:47'),
(261, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 07:55:55', '2020-06-23 07:55:55'),
(262, 1, 'admin/program-episodes/1', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"3\",\"broadcast_date\":\"2020-06-30\",\"episode_no\":\"1\",\"title\":\"Test\",\"description\":\"adsadsda\\r\\nsdasddsa\\r\\ndsasad\\r\\nda\\r\\ndas\",\"_token\":\"u71AIhvLXjmW0DBAU5TMfiUGJBxxp21VJsOxDqJL\",\"_method\":\"PUT\"}', '2020-06-23 07:55:57', '2020-06-23 07:55:57'),
(263, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 07:56:44', '2020-06-23 07:56:44'),
(264, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 07:56:45', '2020-06-23 07:56:45'),
(265, 1, 'admin/program-episodes/1', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"3\",\"broadcast_date\":\"2020-06-30\",\"episode_no\":\"1\",\"title\":\"Test\",\"description\":\"adsadsda\\r\\nsdasddsa\\r\\ndsasad\\r\\nda\\r\\ndas\",\"_token\":\"u71AIhvLXjmW0DBAU5TMfiUGJBxxp21VJsOxDqJL\",\"_method\":\"PUT\"}', '2020-06-23 07:56:47', '2020-06-23 07:56:47'),
(266, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 07:56:47', '2020-06-23 07:56:47'),
(267, 1, 'admin/program-episodes/1', 'GET', '127.0.0.1', '[]', '2020-06-23 07:57:18', '2020-06-23 07:57:18'),
(268, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 07:57:24', '2020-06-23 07:57:24'),
(269, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 07:57:24', '2020-06-23 07:57:24'),
(270, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 07:57:25', '2020-06-23 07:57:25'),
(271, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 07:57:26', '2020-06-23 07:57:26'),
(272, 1, 'admin/program-episodes/1', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"3\",\"broadcast_date\":\"2020-06-30\",\"episode_no\":\"1\",\"title\":\"Test\",\"description\":\"adsadsda\\r\\nsdasddsa\\r\\ndsasad\\r\\nda\\r\\ndas\",\"_token\":\"u71AIhvLXjmW0DBAU5TMfiUGJBxxp21VJsOxDqJL\",\"_method\":\"PUT\"}', '2020-06-23 07:57:28', '2020-06-23 07:57:28'),
(273, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 07:57:37', '2020-06-23 07:57:37'),
(274, 1, 'admin/program-episodes/1', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"3\",\"broadcast_date\":\"2020-06-30\",\"episode_no\":\"1\",\"title\":\"Test\",\"description\":\"adsadsda\\r\\nsdasddsa\\r\\ndsasad\\r\\nda\\r\\ndas\",\"_token\":\"u71AIhvLXjmW0DBAU5TMfiUGJBxxp21VJsOxDqJL\",\"_method\":\"PUT\"}', '2020-06-23 07:57:39', '2020-06-23 07:57:39'),
(275, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 07:58:16', '2020-06-23 07:58:16'),
(276, 1, 'admin/program-episodes/1', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"3\",\"broadcast_date\":\"2020-06-30\",\"episode_no\":\"1\",\"title\":\"Test\",\"description\":\"adsadsda\\r\\nsdasddsa\\r\\ndsasad\\r\\nda\\r\\ndas\",\"_token\":\"u71AIhvLXjmW0DBAU5TMfiUGJBxxp21VJsOxDqJL\",\"_method\":\"PUT\"}', '2020-06-23 07:58:18', '2020-06-23 07:58:18'),
(277, 1, 'admin/program-episodes/1', 'GET', '127.0.0.1', '[]', '2020-06-23 07:58:27', '2020-06-23 07:58:27'),
(278, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 07:59:19', '2020-06-23 07:59:19'),
(279, 1, 'admin/program-episodes/1', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"3\",\"broadcast_date\":\"2020-06-30\",\"episode_no\":\"1\",\"title\":\"Test\",\"description\":\"adsadsda\\r\\nsdasddsa\\r\\ndsasad\\r\\nda\\r\\ndas\",\"_token\":\"u71AIhvLXjmW0DBAU5TMfiUGJBxxp21VJsOxDqJL\",\"_method\":\"PUT\"}', '2020-06-23 07:59:23', '2020-06-23 07:59:23'),
(280, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 07:59:37', '2020-06-23 07:59:37'),
(281, 1, 'admin/program-episodes/1', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"3\",\"broadcast_date\":\"2020-06-30\",\"episode_no\":\"1\",\"title\":\"Test\",\"description\":\"adsadsda\\r\\nsdasddsa\\r\\ndsasad\\r\\nda\\r\\ndas\",\"_token\":\"u71AIhvLXjmW0DBAU5TMfiUGJBxxp21VJsOxDqJL\",\"_method\":\"PUT\"}', '2020-06-23 07:59:40', '2020-06-23 07:59:40'),
(282, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 08:00:36', '2020-06-23 08:00:36'),
(283, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 08:00:37', '2020-06-23 08:00:37'),
(284, 1, 'admin/program-episodes/1', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"3\",\"broadcast_date\":\"2020-06-30\",\"episode_no\":\"1\",\"title\":\"Test\",\"description\":\"adsadsda\\r\\nsdasddsa\\r\\ndsasad\\r\\nda\\r\\ndas\",\"_token\":\"u71AIhvLXjmW0DBAU5TMfiUGJBxxp21VJsOxDqJL\",\"_method\":\"PUT\"}', '2020-06-23 08:00:39', '2020-06-23 08:00:39'),
(285, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 08:01:41', '2020-06-23 08:01:41'),
(286, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 08:01:42', '2020-06-23 08:01:42'),
(287, 1, 'admin/program-episodes/1', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"3\",\"broadcast_date\":\"2020-06-30\",\"episode_no\":\"1\",\"title\":\"Test\",\"description\":\"adsadsda\\r\\nsdasddsa\\r\\ndsasad\\r\\nda\\r\\ndas\",\"_token\":\"u71AIhvLXjmW0DBAU5TMfiUGJBxxp21VJsOxDqJL\",\"_method\":\"PUT\"}', '2020-06-23 08:01:45', '2020-06-23 08:01:45'),
(288, 1, 'admin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-23 08:01:45', '2020-06-23 08:01:45'),
(289, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 08:02:15', '2020-06-23 08:02:15'),
(290, 1, 'admin/program-episodes/1', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"3\",\"broadcast_date\":\"2020-06-30\",\"episode_no\":\"1\",\"title\":\"Test\",\"description\":\"adsadsda\\r\\nsdasddsa\\r\\ndsasad\\r\\nda\\r\\ndas\",\"_token\":\"u71AIhvLXjmW0DBAU5TMfiUGJBxxp21VJsOxDqJL\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/program-episodes\"}', '2020-06-23 08:02:17', '2020-06-23 08:02:17'),
(291, 1, 'admin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-23 08:02:17', '2020-06-23 08:02:17'),
(292, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 08:02:50', '2020-06-23 08:02:50'),
(293, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 08:05:18', '2020-06-23 08:05:18'),
(294, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 08:05:32', '2020-06-23 08:05:32'),
(295, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 08:05:54', '2020-06-23 08:05:54'),
(296, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 08:06:07', '2020-06-23 08:06:07'),
(297, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 08:06:10', '2020-06-23 08:06:10'),
(298, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 08:06:30', '2020-06-23 08:06:30'),
(299, 1, 'admin/program-episodes/1', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"3\",\"broadcast_date\":\"2020-06-30\",\"episode_no\":\"1\",\"title\":\"Test\",\"description\":\"adsadsda\\r\\nsdasddsa\\r\\ndsasad\\r\\nda\\r\\ndas\",\"prv_video\":null,\"_token\":\"u71AIhvLXjmW0DBAU5TMfiUGJBxxp21VJsOxDqJL\",\"_method\":\"PUT\"}', '2020-06-23 08:06:32', '2020-06-23 08:06:32'),
(300, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 08:06:32', '2020-06-23 08:06:32'),
(301, 1, 'admin/program-episodes/1', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"3\",\"broadcast_date\":\"2020-06-30\",\"episode_no\":\"1\",\"title\":\"Test\",\"description\":\"adsadsda\\r\\nsdasddsa\\r\\ndsasad\\r\\nda\\r\\ndas\",\"prv_video\":null,\"_token\":\"u71AIhvLXjmW0DBAU5TMfiUGJBxxp21VJsOxDqJL\",\"_method\":\"PUT\"}', '2020-06-23 08:07:09', '2020-06-23 08:07:09'),
(302, 1, 'admin/program-episodes/1', 'GET', '127.0.0.1', '[]', '2020-06-23 08:07:09', '2020-06-23 08:07:09'),
(303, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 08:07:12', '2020-06-23 08:07:12'),
(304, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 08:07:22', '2020-06-23 08:07:22'),
(305, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 08:07:23', '2020-06-23 08:07:23'),
(306, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 08:07:24', '2020-06-23 08:07:24'),
(307, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 08:07:27', '2020-06-23 08:07:27'),
(308, 1, 'admin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 08:07:33', '2020-06-23 08:07:33'),
(309, 1, 'admin/program-episodes/1', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"3\",\"broadcast_date\":\"2020-06-30\",\"episode_no\":\"1\",\"title\":\"Test\",\"description\":\"adsadsda\\r\\nsdasddsa\\r\\ndsasad\\r\\nda\\r\\ndas\",\"_token\":\"u71AIhvLXjmW0DBAU5TMfiUGJBxxp21VJsOxDqJL\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/program-episodes\\/1\"}', '2020-06-23 08:07:38', '2020-06-23 08:07:38'),
(310, 1, 'admin/program-episodes/1', 'GET', '127.0.0.1', '[]', '2020-06-23 08:07:38', '2020-06-23 08:07:38'),
(311, 1, 'admin/program-episodes', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 08:07:44', '2020-06-23 08:07:44'),
(312, 1, 'admin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-23 08:11:38', '2020-06-23 08:11:38'),
(313, 1, 'admin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-23 08:12:13', '2020-06-23 08:12:13'),
(314, 1, 'admin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-23 08:12:28', '2020-06-23 08:12:28'),
(315, 1, 'admin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-23 08:12:33', '2020-06-23 08:12:33'),
(316, 1, 'admin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-23 08:13:02', '2020-06-23 08:13:02'),
(317, 1, 'admin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-23 08:13:52', '2020-06-23 08:13:52'),
(318, 1, 'admin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-23 08:13:57', '2020-06-23 08:13:57'),
(319, 1, 'admin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-23 08:14:14', '2020-06-23 08:14:14'),
(320, 1, 'admin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-23 08:14:33', '2020-06-23 08:14:33'),
(321, 1, 'admin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-23 08:14:41', '2020-06-23 08:14:41'),
(322, 1, 'admin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-23 08:15:07', '2020-06-23 08:15:07'),
(323, 1, 'admin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-23 08:16:08', '2020-06-23 08:16:08'),
(324, 1, 'admin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-23 08:16:18', '2020-06-23 08:16:18'),
(325, 1, 'admin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-23 08:16:28', '2020-06-23 08:16:28'),
(326, 1, 'admin/programs', 'GET', '127.0.0.1', '[]', '2020-06-23 09:24:58', '2020-06-23 09:24:58'),
(327, 1, 'admin/programs', 'GET', '127.0.0.1', '[]', '2020-06-23 09:25:12', '2020-06-23 09:25:12'),
(328, 1, 'admin/auth/login', 'GET', '127.0.0.1', '[]', '2020-06-23 09:25:25', '2020-06-23 09:25:25'),
(329, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-06-23 09:25:36', '2020-06-23 09:25:36'),
(330, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-23 09:26:14', '2020-06-23 09:26:14'),
(331, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '[]', '2020-06-23 09:26:18', '2020-06-23 09:26:18'),
(332, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '[]', '2020-06-23 09:26:21', '2020-06-23 09:26:21'),
(333, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '[]', '2020-06-23 09:26:22', '2020-06-23 09:26:22'),
(334, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '[]', '2020-06-23 09:26:52', '2020-06-23 09:26:52'),
(335, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '[]', '2020-06-23 09:26:53', '2020-06-23 09:26:53');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(336, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-23 09:27:31', '2020-06-23 09:27:31'),
(337, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 09:27:35', '2020-06-23 09:27:35'),
(338, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 09:27:36', '2020-06-23 09:27:36'),
(339, 1, 'ShowTvAdmin/programs/4/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 09:27:45', '2020-06-23 09:27:45'),
(340, 1, 'ShowTvAdmin/programs/4', 'PUT', '127.0.0.1', '{\"program_type_id\":\"2\",\"title\":\"ssdadsa\",\"description\":\"sdadsadassda\",\"broadcast_start_date\":\"2020-06-22\",\"broadcast_start_time\":\"21:30:00\",\"broadcast_end_time\":\"21:30:00\",\"broadCastDaysProgram\":[\"2\",\"3\",\"4\",null],\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"vOHXALsHCXQTk6oyolmVEQKsnYJctdaTmtCQLFr8\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/programs\"}', '2020-06-23 09:27:53', '2020-06-23 09:27:53'),
(341, 1, 'ShowTvAdmin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 09:27:54', '2020-06-23 09:27:54'),
(342, 1, 'ShowTvAdmin/programs/4', 'PUT', '127.0.0.1', '{\"program_type_id\":\"2\",\"title\":\"ssdadsa\",\"description\":\"sdadsadassda\",\"broadcast_start_date\":\"2020-06-22\",\"broadcast_start_time\":\"21:30:00\",\"broadcast_end_time\":\"21:30:00\",\"broadCastDaysProgram\":[\"2\",\"3\",\"4\",null],\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"vOHXALsHCXQTk6oyolmVEQKsnYJctdaTmtCQLFr8\",\"_method\":\"PUT\"}', '2020-06-23 09:28:03', '2020-06-23 09:28:03'),
(343, 1, 'ShowTvAdmin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 09:28:03', '2020-06-23 09:28:03'),
(344, 1, 'ShowTvAdmin/programs/4', 'PUT', '127.0.0.1', '{\"program_type_id\":\"2\",\"title\":\"ssdadsa\",\"description\":\"sdadsadassda\",\"broadcast_start_date\":\"2020-06-22\",\"broadcast_start_time\":\"21:29:00\",\"broadcast_end_time\":\"21:29:00\",\"broadCastDaysProgram\":[\"2\",\"3\",\"4\",null],\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"vOHXALsHCXQTk6oyolmVEQKsnYJctdaTmtCQLFr8\",\"_method\":\"PUT\"}', '2020-06-23 09:28:17', '2020-06-23 09:28:17'),
(345, 1, 'ShowTvAdmin/programs/4/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 09:28:17', '2020-06-23 09:28:17'),
(346, 1, 'ShowTvAdmin/programs/4', 'PUT', '127.0.0.1', '{\"program_type_id\":\"2\",\"title\":\"ssdadsa\",\"description\":\"sdadsadassda\",\"broadcast_start_date\":\"2020-06-22\",\"broadcast_start_time\":\"21:29:00\",\"broadcast_end_time\":\"21:29:00\",\"broadCastDaysProgram\":[\"2\",\"3\",\"4\",null],\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"vOHXALsHCXQTk6oyolmVEQKsnYJctdaTmtCQLFr8\",\"_method\":\"PUT\"}', '2020-06-23 09:28:46', '2020-06-23 09:28:46'),
(347, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '[]', '2020-06-23 09:28:47', '2020-06-23 09:28:47'),
(348, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 09:28:50', '2020-06-23 09:28:50'),
(349, 1, 'ShowTvAdmin/program-episodes/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 09:28:53', '2020-06-23 09:28:53'),
(350, 1, 'ShowTvAdmin/program-episodes/1', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"3\",\"broadcast_date\":\"2020-06-30\",\"episode_no\":\"1\",\"title\":\"Test\",\"description\":\"adsadsda\\r\\nsdasddsa\\r\\ndsasad\\r\\nda\\r\\ndas\",\"_token\":\"vOHXALsHCXQTk6oyolmVEQKsnYJctdaTmtCQLFr8\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-23 09:29:31', '2020-06-23 09:29:31'),
(351, 1, 'ShowTvAdmin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 09:29:32', '2020-06-23 09:29:32'),
(352, 1, 'ShowTvAdmin/program-episodes/1', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"3\",\"broadcast_date\":\"2020-06-30\",\"episode_no\":\"1\",\"title\":\"Test\",\"description\":\"adsadsda\\r\\nsdasddsa\\r\\ndsasad\\r\\nda\\r\\ndas\",\"_token\":\"vOHXALsHCXQTk6oyolmVEQKsnYJctdaTmtCQLFr8\",\"_method\":\"PUT\"}', '2020-06-23 09:30:59', '2020-06-23 09:30:59'),
(353, 1, 'ShowTvAdmin/program-episodes/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 09:31:28', '2020-06-23 09:31:28'),
(354, 1, 'ShowTvAdmin/program-episodes/1', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"3\",\"broadcast_date\":\"2020-06-30\",\"episode_no\":\"1\",\"title\":\"Test\",\"description\":\"adsadsda\\r\\nsdasddsa\\r\\ndsasad\\r\\nda\\r\\ndas\",\"_token\":\"vOHXALsHCXQTk6oyolmVEQKsnYJctdaTmtCQLFr8\",\"_method\":\"PUT\"}', '2020-06-23 09:31:34', '2020-06-23 09:31:34'),
(355, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-23 09:31:34', '2020-06-23 09:31:34'),
(356, 1, 'ShowTvAdmin/program-episodes/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 09:31:37', '2020-06-23 09:31:37'),
(357, 1, 'ShowTvAdmin/program-episodes/1', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"3\",\"broadcast_date\":\"2020-06-30\",\"episode_no\":\"1\",\"title\":\"Test\",\"description\":\"adsadsda\\r\\nsdasddsa\\r\\ndsasad\\r\\nda\\r\\ndas\",\"_token\":\"vOHXALsHCXQTk6oyolmVEQKsnYJctdaTmtCQLFr8\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-23 09:31:58', '2020-06-23 09:31:58'),
(358, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-23 09:31:59', '2020-06-23 09:31:59'),
(359, 1, 'ShowTvAdmin/program-episodes/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 09:32:04', '2020-06-23 09:32:04'),
(360, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 09:32:35', '2020-06-23 09:32:35'),
(361, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 09:32:36', '2020-06-23 09:32:36'),
(362, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 09:32:54', '2020-06-23 09:32:54'),
(363, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 09:33:01', '2020-06-23 09:33:01'),
(364, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 09:33:18', '2020-06-23 09:33:18'),
(365, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 09:33:33', '2020-06-23 09:33:33'),
(366, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 09:33:37', '2020-06-23 09:33:37'),
(367, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 09:33:44', '2020-06-23 09:33:44'),
(368, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 09:34:30', '2020-06-23 09:34:30'),
(369, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-23 09:34:32', '2020-06-23 09:34:32'),
(370, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"2\",\"broadcast_date\":\"2020-06-23\",\"episode_no\":\"3\",\"title\":\"add\",\"description\":\"aklanklas\\r\\nsajlsaklsa\",\"_token\":\"vOHXALsHCXQTk6oyolmVEQKsnYJctdaTmtCQLFr8\"}', '2020-06-23 09:35:00', '2020-06-23 09:35:00'),
(371, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-23 09:35:01', '2020-06-23 09:35:01'),
(372, 1, 'ShowTvAdmin/program-episodes/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 09:35:05', '2020-06-23 09:35:05'),
(373, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-23 13:38:53', '2020-06-23 13:38:53'),
(374, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 13:39:00', '2020-06-23 13:39:00'),
(375, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-23 13:39:08', '2020-06-23 13:39:08'),
(376, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-23 13:39:12', '2020-06-23 13:39:12'),
(377, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 13:39:53', '2020-06-23 13:39:53'),
(378, 1, 'ShowTvAdmin/programs/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 13:40:00', '2020-06-23 13:40:00'),
(379, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-23 16:38:24', '2020-06-23 16:38:24'),
(380, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 16:38:30', '2020-06-23 16:38:30'),
(381, 1, 'ShowTvAdmin/programs/4/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 16:38:39', '2020-06-23 16:38:39'),
(382, 1, 'ShowTvAdmin/programs/4', 'PUT', '127.0.0.1', '{\"program_type_id\":\"2\",\"title\":\"\\u0628\\u0631\\u0648\\u0643\\u0627\\u0631\",\"description\":\"\\u0645\\u0633\\u0644\\u0633\\u0644 \\u0628\\u0631\\u0648\\u0643\\u0627\\u0631\",\"broadcast_start_date\":\"2020-06-22\",\"broadcast_start_time\":\"12:00:00\",\"broadcast_end_time\":\"13:00:00\",\"broadCastDaysProgram\":[\"0\",\"1\",\"2\",\"3\",\"4\",null],\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"m3rXRSmKWQ36Y7csxQvllbXzcG5FkwSE8lOMB9jG\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/programs\"}', '2020-06-23 16:41:09', '2020-06-23 16:41:09'),
(383, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '[]', '2020-06-23 16:41:10', '2020-06-23 16:41:10'),
(384, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 16:41:18', '2020-06-23 16:41:18'),
(385, 1, 'ShowTvAdmin/program-episodes/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 16:41:25', '2020-06-23 16:41:25'),
(386, 1, 'ShowTvAdmin/program-episodes/1', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"2\",\"broadcast_date\":\"2020-06-22\",\"episode_no\":\"1\",\"title\":\"\\u0627\\u0644\\u062d\\u0644\\u0642\\u0629 \\u0627\\u0644\\u0627\\u0648\\u0644\\u0649\",\"description\":\"\\u0639\\u0631\\u0648\\u0636 \\u0643\\u0648\\u0645\\u064a\\u062f\\u064a\\u0627 \\u0625\\u0631\\u062a\\u062c\\u0627\\u0644\\u064a\\u0629 \\u064a\\u0624\\u062f\\u064a\\u0647\\u0627 \\u0639\\u0634\\u0631\\u0648\\u0646 \\u0643\\u0648\\u0645\\u064a\\u062f\\u064a\\u0627\\u064b \\u0639\\u0631\\u0628\\u064a\\u0627\\u064b \\u0645\\u0646 \\u062c\\u0645\\u064a\\u0639 \\u0623\\u0646\\u062d\\u0627\\u0621 \\u0627\\u0644\\u0639\\u0627\\u0644\\u0645\\u060c \\u0648\\u064a\\u0645\\u0646\\u062d \\\"\\u0645\\u0646\\u0627 \\u0648\\u0641\\u064a\\u0646\\u0627\\\" \\u0643\\u0644\\u0627\\u064b \\u0645\\u0646\\u0647\\u0645 \\u0641\\u0631\\u0635\\u0629 \\u0645\\u0645\\u064a\\u0632\\u0629 \\u0644\\u0644\\u062a\\u0623\\u0644\\u0642 \\u062e\\u0644\\u0627\\u0644 \\u0639\\u0631\\u0636\\u0647 \\u0627\\u0644\\u0630\\u064a \\u064a\\u0633\\u062a\\u0645\\u0631 \\u0644\\u0645\\u062f\\u0629 \\u0646\\u0635\\u0641 \\u0633\\u0627\\u0639\\u0629.\",\"_token\":\"m3rXRSmKWQ36Y7csxQvllbXzcG5FkwSE8lOMB9jG\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-23 16:42:23', '2020-06-23 16:42:23'),
(387, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-23 16:42:23', '2020-06-23 16:42:23'),
(388, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 16:51:43', '2020-06-23 16:51:43'),
(389, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 16:52:52', '2020-06-23 16:52:52'),
(390, 1, 'ShowTvAdmin/programs/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 16:52:53', '2020-06-23 16:52:53'),
(391, 1, 'ShowTvAdmin/programs', 'POST', '127.0.0.1', '{\"program_type_id\":\"1\",\"title\":\"\\u0627\\u0644\\u0627\\u062e\\u0644\\u0628\\u0627\\u0631\",\"description\":\"\\u0627\\u062e\\u0628\\u0627\\u0631 \\u0627\\u0644\\u0633\\u0627\\u0639\\u0629\",\"broadcast_start_date\":\"2020-06-23\",\"broadcast_start_time\":\"21:00:00\",\"broadcast_end_time\":\"22:00:00\",\"broadCastDaysProgram\":[\"0\",\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",null],\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"m3rXRSmKWQ36Y7csxQvllbXzcG5FkwSE8lOMB9jG\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/programs\"}', '2020-06-23 16:53:48', '2020-06-23 16:53:48'),
(392, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '[]', '2020-06-23 16:53:48', '2020-06-23 16:53:48'),
(393, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '[]', '2020-06-23 17:04:49', '2020-06-23 17:04:49'),
(394, 1, 'ShowTvAdmin/programs/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 17:07:45', '2020-06-23 17:07:45'),
(395, 1, 'ShowTvAdmin/programs', 'POST', '127.0.0.1', '{\"program_type_id\":\"2\",\"title\":\"Sefrien Kizi\",\"description\":\"\\u0639\\u0631\\u0648\\u0636 \\u0643\\u0648\\u0645\\u064a\\u062f\\u064a\\u0627 \\u0625\\u0631\\u062a\\u062c\\u0627\\u0644\\u064a\\u0629 \\u064a\\u0624\\u062f\\u064a\\u0647\\u0627 \\u0639\\u0634\\u0631\\u0648\\u0646 \\u0643\\u0648\\u0645\\u064a\\u062f\\u064a\\u0627\\u064b \\u0639\\u0631\\u0628\\u064a\\u0627\\u064b \\u0645\\u0646 \\u062c\\u0645\\u064a\\u0639 \\u0623\\u0646\\u062d\\u0627\\u0621 \\u0627\\u0644\\u0639\\u0627\\u0644\\u0645\\u060c \\u0648\\u064a\\u0645\\u0646\\u062d \\\"\\u0645\\u0646\\u0627 \\u0648\\u0641\\u064a\\u0646\\u0627\\\" \\u0643\\u0644\\u0627\\u064b \\u0645\\u0646\\u0647\\u0645 \\u0641\\u0631\\u0635\\u0629 \\u0645\\u0645\\u064a\\u0632\\u0629 \\u0644\\u0644\\u062a\\u0623\\u0644\\u0642 \\u062e\\u0644\\u0627\\u0644 \\u0639\\u0631\\u0636\\u0647 \\u0627\\u0644\\u0630\\u064a \\u064a\\u0633\\u062a\\u0645\\u0631 \\u0644\\u0645\\u062f\\u0629 \\u0646\\u0635\\u0641 \\u0633\\u0627\\u0639\\u0629.\",\"broadcast_start_date\":\"2020-06-24\",\"broadcast_start_time\":\"10:00:00\",\"broadcast_end_time\":\"11:00:00\",\"broadCastDaysProgram\":[\"0\",\"1\",\"2\",\"3\",\"4\",null],\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"m3rXRSmKWQ36Y7csxQvllbXzcG5FkwSE8lOMB9jG\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/programs\"}', '2020-06-23 17:08:54', '2020-06-23 17:08:54'),
(396, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '[]', '2020-06-23 17:08:55', '2020-06-23 17:08:55'),
(397, 1, 'ShowTvAdmin/programs/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 17:10:07', '2020-06-23 17:10:07'),
(398, 1, 'ShowTvAdmin/programs', 'POST', '127.0.0.1', '{\"program_type_id\":\"1\",\"title\":\"Steve harfy\",\"description\":\"FWIW, CodeIgniter implements a special RANDOM sorting direction, which is replaced with the correct grammar when building query. Also it seems to be fairly easy to implement. Looks like we have a candidate for improving Laravel :)\",\"broadcast_start_date\":\"2020-06-23\",\"broadcast_start_time\":\"15:00:00\",\"broadcast_end_time\":\"16:00:00\",\"broadCastDaysProgram\":[\"0\",\"1\",\"2\",\"3\",\"4\",\"6\",null],\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"m3rXRSmKWQ36Y7csxQvllbXzcG5FkwSE8lOMB9jG\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/programs\"}', '2020-06-23 17:11:02', '2020-06-23 17:11:02'),
(399, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '[]', '2020-06-23 17:11:03', '2020-06-23 17:11:03'),
(400, 1, 'ShowTvAdmin/programs/7/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 17:34:16', '2020-06-23 17:34:16'),
(401, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 17:38:04', '2020-06-23 17:38:04'),
(402, 1, 'ShowTvAdmin/programs/6/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 17:38:08', '2020-06-23 17:38:08'),
(403, 1, 'ShowTvAdmin/programs/6', 'PUT', '127.0.0.1', '{\"program_type_id\":\"2\",\"title\":\"Sefrien Kizi\",\"description\":\"\\u0639\\u0631\\u0648\\u0636 \\u0643\\u0648\\u0645\\u064a\\u062f\\u064a\\u0627 \\u0625\\u0631\\u062a\\u062c\\u0627\\u0644\\u064a\\u0629 \\u064a\\u0624\\u062f\\u064a\\u0647\\u0627 \\u0639\\u0634\\u0631\\u0648\\u0646 \\u0643\\u0648\\u0645\\u064a\\u062f\\u064a\\u0627\\u064b \\u0639\\u0631\\u0628\\u064a\\u0627\\u064b \\u0645\\u0646 \\u062c\\u0645\\u064a\\u0639 \\u0623\\u0646\\u062d\\u0627\\u0621 \\u0627\\u0644\\u0639\\u0627\\u0644\\u0645\\u060c \\u0648\\u064a\\u0645\\u0646\\u062d \\\"\\u0645\\u0646\\u0627 \\u0648\\u0641\\u064a\\u0646\\u0627\\\" \\u0643\\u0644\\u0627\\u064b \\u0645\\u0646\\u0647\\u0645 \\u0641\\u0631\\u0635\\u0629 \\u0645\\u0645\\u064a\\u0632\\u0629 \\u0644\\u0644\\u062a\\u0623\\u0644\\u0642 \\u062e\\u0644\\u0627\\u0644 \\u0639\\u0631\\u0636\\u0647 \\u0627\\u0644\\u0630\\u064a \\u064a\\u0633\\u062a\\u0645\\u0631 \\u0644\\u0645\\u062f\\u0629 \\u0646\\u0635\\u0641 \\u0633\\u0627\\u0639\\u0629.\",\"broadcast_start_date\":\"2020-06-24\",\"broadcast_start_time\":\"10:00:00\",\"broadcast_end_time\":\"11:00:00\",\"broadCastDaysProgram\":[\"0\",\"1\",\"2\",\"3\",\"4\",null],\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"m3rXRSmKWQ36Y7csxQvllbXzcG5FkwSE8lOMB9jG\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/programs\"}', '2020-06-23 17:38:31', '2020-06-23 17:38:31'),
(404, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '[]', '2020-06-23 17:38:31', '2020-06-23 17:38:31'),
(405, 1, 'ShowTvAdmin/programs/6/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 17:38:35', '2020-06-23 17:38:35'),
(406, 1, 'ShowTvAdmin/programs/6/edit', 'GET', '127.0.0.1', '[]', '2020-06-23 17:39:08', '2020-06-23 17:39:08'),
(407, 1, 'ShowTvAdmin/programs/6', 'PUT', '127.0.0.1', '{\"program_type_id\":\"2\",\"title\":\"Sefrien Kizi\",\"description\":\"\\u0639\\u0631\\u0648\\u0636 \\u0643\\u0648\\u0645\\u064a\\u062f\\u064a\\u0627 \\u0625\\u0631\\u062a\\u062c\\u0627\\u0644\\u064a\\u0629 \\u064a\\u0624\\u062f\\u064a\\u0647\\u0627 \\u0639\\u0634\\u0631\\u0648\\u0646 \\u0643\\u0648\\u0645\\u064a\\u062f\\u064a\\u0627\\u064b \\u0639\\u0631\\u0628\\u064a\\u0627\\u064b \\u0645\\u0646 \\u062c\\u0645\\u064a\\u0639 \\u0623\\u0646\\u062d\\u0627\\u0621 \\u0627\\u0644\\u0639\\u0627\\u0644\\u0645\\u060c \\u0648\\u064a\\u0645\\u0646\\u062d \\\"\\u0645\\u0646\\u0627 \\u0648\\u0641\\u064a\\u0646\\u0627\\\" \\u0643\\u0644\\u0627\\u064b \\u0645\\u0646\\u0647\\u0645 \\u0641\\u0631\\u0635\\u0629 \\u0645\\u0645\\u064a\\u0632\\u0629 \\u0644\\u0644\\u062a\\u0623\\u0644\\u0642 \\u062e\\u0644\\u0627\\u0644 \\u0639\\u0631\\u0636\\u0647 \\u0627\\u0644\\u0630\\u064a \\u064a\\u0633\\u062a\\u0645\\u0631 \\u0644\\u0645\\u062f\\u0629 \\u0646\\u0635\\u0641 \\u0633\\u0627\\u0639\\u0629.\",\"broadcast_start_date\":\"2020-06-24\",\"broadcast_start_time\":\"10:00:00\",\"broadcast_end_time\":\"11:00:00\",\"broadCastDaysProgram\":[\"1\",\"2\",\"3\",\"4\",\"5\",null],\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"m3rXRSmKWQ36Y7csxQvllbXzcG5FkwSE8lOMB9jG\",\"_method\":\"PUT\"}', '2020-06-23 17:39:12', '2020-06-23 17:39:12'),
(408, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '[]', '2020-06-23 17:39:13', '2020-06-23 17:39:13'),
(409, 1, 'ShowTvAdmin/programs/7/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 17:39:30', '2020-06-23 17:39:30'),
(410, 1, 'ShowTvAdmin/programs/7', 'PUT', '127.0.0.1', '{\"program_type_id\":\"1\",\"title\":\"Steve harfy\",\"description\":\"FWIW, CodeIgniter implements a special RANDOM sorting direction, which is replaced with the correct grammar when building query. Also it seems to be fairly easy to implement. Looks like we have a candidate for improving Laravel :)\",\"broadcast_start_date\":\"2020-06-23\",\"broadcast_start_time\":\"15:00:00\",\"broadcast_end_time\":\"16:00:00\",\"broadCastDaysProgram\":[\"1\",\"2\",\"3\",\"4\",\"6\",\"7\",null],\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"m3rXRSmKWQ36Y7csxQvllbXzcG5FkwSE8lOMB9jG\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/programs\"}', '2020-06-23 17:39:41', '2020-06-23 17:39:41'),
(411, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '[]', '2020-06-23 17:39:41', '2020-06-23 17:39:41'),
(412, 1, 'ShowTvAdmin/programs/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-23 18:01:49', '2020-06-23 18:01:49'),
(413, 1, 'ShowTvAdmin/programs', 'POST', '127.0.0.1', '{\"program_type_id\":\"2\",\"title\":\"Game Of Thrones\",\"description\":\"daljsanl\\r\\nsadlknsalka\",\"broadcast_start_date\":\"2020-06-23\",\"broadcast_start_time\":\"22:00:00\",\"broadcast_end_time\":\"23:00:00\",\"broadCastDaysProgram\":[\"1\",\"3\",\"4\",\"6\",\"7\",null],\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"m3rXRSmKWQ36Y7csxQvllbXzcG5FkwSE8lOMB9jG\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/programs\"}', '2020-06-23 18:03:32', '2020-06-23 18:03:32'),
(414, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '[]', '2020-06-23 18:03:33', '2020-06-23 18:03:33'),
(415, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-24 07:42:35', '2020-06-24 07:42:35'),
(416, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-24 07:42:40', '2020-06-24 07:42:40'),
(417, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-24 07:42:44', '2020-06-24 07:42:44'),
(418, 1, 'ShowTvAdmin/programs/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-24 07:42:46', '2020-06-24 07:42:46'),
(419, 1, 'ShowTvAdmin/programs', 'POST', '127.0.0.1', '{\"program_type_id\":\"1\",\"title\":\"dasdsa\",\"description\":\"asddas\",\"broadcast_start_date\":\"2020-06-24\",\"broadcast_start_time\":\"10:42:00\",\"broadcast_end_time\":\"10:42:00\",\"broadCastDaysProgram\":[\"2\",null],\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"vhNXlXRXcBIRjkIy217CvdGQutNJm8EBcdKhH2WI\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/programs\"}', '2020-06-24 07:45:14', '2020-06-24 07:45:14'),
(420, 1, 'ShowTvAdmin/programs/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-24 07:55:22', '2020-06-24 07:55:22'),
(421, 1, 'ShowTvAdmin/programs/create', 'GET', '127.0.0.1', '[]', '2020-06-24 07:55:58', '2020-06-24 07:55:58'),
(422, 1, 'ShowTvAdmin/programs/create', 'GET', '127.0.0.1', '[]', '2020-06-24 07:56:28', '2020-06-24 07:56:28'),
(423, 1, 'ShowTvAdmin/programs/create', 'GET', '127.0.0.1', '[]', '2020-06-24 07:57:17', '2020-06-24 07:57:17'),
(424, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-25 10:13:43', '2020-06-25 10:13:43'),
(425, 1, 'ShowTvAdmin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-25 10:13:48', '2020-06-25 10:13:48'),
(426, 1, 'ShowTvAdmin/users', 'GET', '127.0.0.1', '[]', '2020-06-25 10:13:52', '2020-06-25 10:13:52'),
(427, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-25 10:13:59', '2020-06-25 10:13:59'),
(428, 1, 'ShowTvAdmin/programs/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-25 10:14:05', '2020-06-25 10:14:05'),
(429, 1, 'ShowTvAdmin/programs', 'POST', '127.0.0.1', '{\"program_type_id\":\"1\",\"title\":\"zft\",\"description\":\"2zft\",\"broadcast_start_date\":\"2020-06-25\",\"broadcast_start_time\":\"13:15:00\",\"broadcast_end_time\":\"13:14:00\",\"broadCastDaysProgram\":[\"1\",\"2\",\"3\",\"5\",\"6\",null],\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"Mst0dLMj4XxKr2KAKmvHDEQFKlzEGhENMhtffAKY\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/programs\"}', '2020-06-25 10:15:20', '2020-06-25 10:15:20'),
(430, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '[]', '2020-06-25 10:15:21', '2020-06-25 10:15:21'),
(431, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-25 10:15:35', '2020-06-25 10:15:35'),
(432, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-25 10:15:41', '2020-06-25 10:15:41'),
(433, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-25 10:15:50', '2020-06-25 10:15:50'),
(434, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/9', 'GET', '127.0.0.1', '[]', '2020-06-25 10:15:53', '2020-06-25 10:15:53'),
(435, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"9\",\"broadcast_day_id\":\"5\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"5\",\"title\":\"7l8t zft\",\"description\":\"dsadasdasda\",\"_token\":\"Mst0dLMj4XxKr2KAKmvHDEQFKlzEGhENMhtffAKY\"}', '2020-06-25 10:17:02', '2020-06-25 10:17:02'),
(436, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-25 10:17:04', '2020-06-25 10:17:04'),
(437, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-26 08:34:35', '2020-06-26 08:34:35'),
(438, 1, 'ShowTvAdmin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 08:34:59', '2020-06-26 08:34:59'),
(439, 1, 'ShowTvAdmin/users', 'GET', '127.0.0.1', '[]', '2020-06-26 08:35:53', '2020-06-26 08:35:53'),
(440, 1, 'ShowTvAdmin/users', 'GET', '127.0.0.1', '[]', '2020-06-26 08:38:48', '2020-06-26 08:38:48'),
(441, 1, 'ShowTvAdmin/users', 'GET', '127.0.0.1', '[]', '2020-06-26 08:41:25', '2020-06-26 08:41:25'),
(442, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 08:41:59', '2020-06-26 08:41:59'),
(443, 1, 'ShowTvAdmin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 08:43:33', '2020-06-26 08:43:33'),
(444, 1, 'ShowTvAdmin/users', 'GET', '127.0.0.1', '[]', '2020-06-26 08:43:37', '2020-06-26 08:43:37'),
(445, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-26 08:44:58', '2020-06-26 08:44:58'),
(446, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 08:45:00', '2020-06-26 08:45:00'),
(447, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 08:45:05', '2020-06-26 08:45:05'),
(448, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 08:45:07', '2020-06-26 08:45:07'),
(449, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 08:46:22', '2020-06-26 08:46:22'),
(450, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/6', 'GET', '127.0.0.1', '[]', '2020-06-26 08:46:24', '2020-06-26 08:46:24'),
(451, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 08:46:28', '2020-06-26 08:46:28'),
(452, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/7', 'GET', '127.0.0.1', '[]', '2020-06-26 08:46:30', '2020-06-26 08:46:30'),
(453, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 08:46:33', '2020-06-26 08:46:33'),
(454, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/6', 'GET', '127.0.0.1', '[]', '2020-06-26 08:46:35', '2020-06-26 08:46:35'),
(455, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/8', 'GET', '127.0.0.1', '[]', '2020-06-26 08:46:45', '2020-06-26 08:46:45'),
(456, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/7', 'GET', '127.0.0.1', '[]', '2020-06-26 08:46:47', '2020-06-26 08:46:47'),
(457, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 08:54:04', '2020-06-26 08:54:04'),
(458, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 08:54:06', '2020-06-26 08:54:06'),
(459, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 08:54:13', '2020-06-26 08:54:13'),
(460, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 08:54:14', '2020-06-26 08:54:14'),
(461, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-24\",\"episode_no\":\"1\",\"title\":\"\\u0627\\u0644\\u062d\\u0644\\u0642\\u0629 \\u0627\\u0644\\u0627\\u0648\\u0644\\u0649\",\"description\":\"\\u0634\\u064a\\u0633\\u0634\\u064a\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-26 08:54:41', '2020-06-26 08:54:41'),
(462, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 08:54:42', '2020-06-26 08:54:42'),
(463, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/6', 'GET', '127.0.0.1', '[]', '2020-06-26 08:55:08', '2020-06-26 08:55:08'),
(464, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 08:55:10', '2020-06-26 08:55:10'),
(465, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-24\",\"episode_no\":\"1\",\"title\":\"\\u0627\\u0644\\u062d\\u0644\\u0642\\u0629 \\u0627\\u0644\\u0627\\u0648\\u0644\\u0649\",\"description\":\"\\u0634\\u064a\\u0633\\u0634\\u064a\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 08:55:25', '2020-06-26 08:55:25'),
(466, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 08:55:25', '2020-06-26 08:55:25'),
(467, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/5', 'GET', '127.0.0.1', '[]', '2020-06-26 08:55:58', '2020-06-26 08:55:58'),
(468, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 08:55:59', '2020-06-26 08:55:59'),
(469, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-24\",\"episode_no\":\"1\",\"title\":\"\\u0627\\u0644\\u062d\\u0644\\u0642\\u0629 \\u0627\\u0644\\u0627\\u0648\\u0644\\u0649\",\"description\":\"\\u0634\\u064a\\u0633\\u0634\\u064a\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 08:56:11', '2020-06-26 08:56:11'),
(470, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 08:56:12', '2020-06-26 08:56:12'),
(471, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/6', 'GET', '127.0.0.1', '[]', '2020-06-26 08:56:30', '2020-06-26 08:56:30'),
(472, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 08:56:31', '2020-06-26 08:56:31'),
(473, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 08:56:35', '2020-06-26 08:56:35'),
(474, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/5', 'GET', '127.0.0.1', '[]', '2020-06-26 08:56:37', '2020-06-26 08:56:37'),
(475, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 08:56:38', '2020-06-26 08:56:38'),
(476, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"saddsa\",\"description\":\"dasdsa\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 08:56:43', '2020-06-26 08:56:43'),
(477, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 08:56:43', '2020-06-26 08:56:43'),
(478, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/7', 'GET', '127.0.0.1', '[]', '2020-06-26 08:56:58', '2020-06-26 08:56:58'),
(479, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 08:56:59', '2020-06-26 08:56:59'),
(480, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"saddsa\",\"description\":\"dasdsa\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 08:57:01', '2020-06-26 08:57:01'),
(481, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 08:57:01', '2020-06-26 08:57:01'),
(482, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/7', 'GET', '127.0.0.1', '[]', '2020-06-26 08:57:23', '2020-06-26 08:57:23'),
(483, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 08:57:24', '2020-06-26 08:57:24'),
(484, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"saddsa\",\"description\":\"dasdsa\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 08:57:25', '2020-06-26 08:57:25'),
(485, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 08:57:25', '2020-06-26 08:57:25'),
(486, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/7', 'GET', '127.0.0.1', '[]', '2020-06-26 08:57:35', '2020-06-26 08:57:35'),
(487, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 08:57:36', '2020-06-26 08:57:36'),
(488, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"saddsa\",\"description\":\"dasdsa\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 08:57:38', '2020-06-26 08:57:38'),
(489, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 08:57:38', '2020-06-26 08:57:38'),
(490, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-26 08:57:59', '2020-06-26 08:57:59'),
(491, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/5', 'GET', '127.0.0.1', '[]', '2020-06-26 08:58:11', '2020-06-26 08:58:11'),
(492, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 08:58:11', '2020-06-26 08:58:11'),
(493, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"saddsa\",\"description\":\"dasdsa\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 08:58:14', '2020-06-26 08:58:14'),
(494, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 08:58:14', '2020-06-26 08:58:14'),
(495, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/8', 'GET', '127.0.0.1', '[]', '2020-06-26 08:58:30', '2020-06-26 08:58:30'),
(496, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 08:58:31', '2020-06-26 08:58:31'),
(497, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"saddsa\",\"description\":\"dasdsa\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 08:58:43', '2020-06-26 08:58:43'),
(498, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-26 08:58:43', '2020-06-26 08:58:43'),
(499, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 08:59:11', '2020-06-26 08:59:11'),
(500, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 08:59:13', '2020-06-26 08:59:13'),
(501, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"sss\",\"description\":\"11\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-26 08:59:26', '2020-06-26 08:59:26'),
(502, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-26 08:59:27', '2020-06-26 08:59:27'),
(503, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 08:59:46', '2020-06-26 08:59:46'),
(504, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 08:59:48', '2020-06-26 08:59:48'),
(505, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"1\",\"description\":\"1111\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-26 08:59:59', '2020-06-26 08:59:59'),
(506, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 08:59:59', '2020-06-26 08:59:59'),
(507, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:00:54', '2020-06-26 09:00:54'),
(508, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:00:56', '2020-06-26 09:00:56'),
(509, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"22\",\"description\":\"22\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-26 09:01:01', '2020-06-26 09:01:01'),
(510, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:01:01', '2020-06-26 09:01:01'),
(511, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/5', 'GET', '127.0.0.1', '[]', '2020-06-26 09:01:25', '2020-06-26 09:01:25'),
(512, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:01:26', '2020-06-26 09:01:26'),
(513, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"22\",\"description\":\"22\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:01:28', '2020-06-26 09:01:28'),
(514, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:01:28', '2020-06-26 09:01:28'),
(515, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/6', 'GET', '127.0.0.1', '[]', '2020-06-26 09:01:37', '2020-06-26 09:01:37'),
(516, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:01:38', '2020-06-26 09:01:38'),
(517, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"22\",\"description\":\"22\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:01:41', '2020-06-26 09:01:41'),
(518, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:01:41', '2020-06-26 09:01:41'),
(519, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/5', 'GET', '127.0.0.1', '[]', '2020-06-26 09:01:49', '2020-06-26 09:01:49'),
(520, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:01:50', '2020-06-26 09:01:50'),
(521, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"22\",\"description\":\"22\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:01:51', '2020-06-26 09:01:51'),
(522, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:01:51', '2020-06-26 09:01:51'),
(523, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/5', 'GET', '127.0.0.1', '[]', '2020-06-26 09:02:01', '2020-06-26 09:02:01'),
(524, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:02:01', '2020-06-26 09:02:01'),
(525, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"22\",\"description\":\"22\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:02:03', '2020-06-26 09:02:03'),
(526, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:02:03', '2020-06-26 09:02:03'),
(527, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/5', 'GET', '127.0.0.1', '[]', '2020-06-26 09:02:17', '2020-06-26 09:02:17'),
(528, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:02:18', '2020-06-26 09:02:18'),
(529, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"22\",\"description\":\"22\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:02:19', '2020-06-26 09:02:19'),
(530, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:02:19', '2020-06-26 09:02:19'),
(531, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:02:33', '2020-06-26 09:02:33'),
(532, 1, 'ShowTvAdmin/_handle_action_', 'POST', '127.0.0.1', '{\"_key\":\"5\",\"_model\":\"App_ProgramEpisode\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_action\":\"Encore_Admin_Grid_Actions_Delete\",\"_input\":\"true\"}', '2020-06-26 09:02:39', '2020-06-26 09:02:39'),
(533, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:02:39', '2020-06-26 09:02:39'),
(534, 1, 'ShowTvAdmin/program-episodes/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:02:43', '2020-06-26 09:02:43'),
(535, 1, 'ShowTvAdmin/program-episodes/1', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"2\",\"broadcast_date\":\"2020-06-22\",\"episode_no\":\"1\",\"title\":\"\\u0627\\u0644\\u062d\\u0644\\u0642\\u0629 \\u0627\\u0644\\u0627\\u0648\\u0644\\u0649\",\"description\":\"\\u0639\\u0631\\u0648\\u0636 \\u0643\\u0648\\u0645\\u064a\\u062f\\u064a\\u0627 \\u0625\\u0631\\u062a\\u062c\\u0627\\u0644\\u064a\\u0629 \\u064a\\u0624\\u062f\\u064a\\u0647\\u0627 \\u0639\\u0634\\u0631\\u0648\\u0646 \\u0643\\u0648\\u0645\\u064a\\u062f\\u064a\\u0627\\u064b \\u0639\\u0631\\u0628\\u064a\\u0627\\u064b \\u0645\\u0646 \\u062c\\u0645\\u064a\\u0639 \\u0623\\u0646\\u062d\\u0627\\u0621 \\u0627\\u0644\\u0639\\u0627\\u0644\\u0645\\u060c \\u0648\\u064a\\u0645\\u0646\\u062d \\\"\\u0645\\u0646\\u0627 \\u0648\\u0641\\u064a\\u0646\\u0627\\\" \\u0643\\u0644\\u0627\\u064b \\u0645\\u0646\\u0647\\u0645 \\u0641\\u0631\\u0635\\u0629 \\u0645\\u0645\\u064a\\u0632\\u0629 \\u0644\\u0644\\u062a\\u0623\\u0644\\u0642 \\u062e\\u0644\\u0627\\u0644 \\u0639\\u0631\\u0636\\u0647 \\u0627\\u0644\\u0630\\u064a \\u064a\\u0633\\u062a\\u0645\\u0631 \\u0644\\u0645\\u062f\\u0629 \\u0646\\u0635\\u0641 \\u0633\\u0627\\u0639\\u0629.\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-26 09:02:45', '2020-06-26 09:02:45'),
(536, 1, 'ShowTvAdmin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 09:02:45', '2020-06-26 09:02:45'),
(537, 1, 'ShowTvAdmin/program-episodes/1', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"2\",\"broadcast_date\":\"2020-06-22\",\"episode_no\":\"1\",\"title\":\"\\u0627\\u0644\\u062d\\u0644\\u0642\\u0629 \\u0627\\u0644\\u0627\\u0648\\u0644\\u0649\",\"description\":\"\\u0639\\u0631\\u0648\\u0636 \\u0643\\u0648\\u0645\\u064a\\u062f\\u064a\\u0627 \\u0625\\u0631\\u062a\\u062c\\u0627\\u0644\\u064a\\u0629 \\u064a\\u0624\\u062f\\u064a\\u0647\\u0627 \\u0639\\u0634\\u0631\\u0648\\u0646 \\u0643\\u0648\\u0645\\u064a\\u062f\\u064a\\u0627\\u064b \\u0639\\u0631\\u0628\\u064a\\u0627\\u064b \\u0645\\u0646 \\u062c\\u0645\\u064a\\u0639 \\u0623\\u0646\\u062d\\u0627\\u0621 \\u0627\\u0644\\u0639\\u0627\\u0644\\u0645\\u060c \\u0648\\u064a\\u0645\\u0646\\u062d \\\"\\u0645\\u0646\\u0627 \\u0648\\u0641\\u064a\\u0646\\u0627\\\" \\u0643\\u0644\\u0627\\u064b \\u0645\\u0646\\u0647\\u0645 \\u0641\\u0631\\u0635\\u0629 \\u0645\\u0645\\u064a\\u0632\\u0629 \\u0644\\u0644\\u062a\\u0623\\u0644\\u0642 \\u062e\\u0644\\u0627\\u0644 \\u0639\\u0631\\u0636\\u0647 \\u0627\\u0644\\u0630\\u064a \\u064a\\u0633\\u062a\\u0645\\u0631 \\u0644\\u0645\\u062f\\u0629 \\u0646\\u0635\\u0641 \\u0633\\u0627\\u0639\\u0629.\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 09:03:21', '2020-06-26 09:03:21'),
(538, 1, 'ShowTvAdmin/program-episodes/1/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 09:03:22', '2020-06-26 09:03:22'),
(539, 1, 'ShowTvAdmin/program-episodes/1', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"2\",\"broadcast_date\":\"2020-06-22\",\"episode_no\":\"1\",\"title\":\"\\u0627\\u0644\\u062d\\u0644\\u0642\\u0629 \\u0627\\u0644\\u0627\\u0648\\u0644\\u0649\",\"description\":\"\\u0639\\u0631\\u0648\\u0636 \\u0643\\u0648\\u0645\\u064a\\u062f\\u064a\\u0627 \\u0625\\u0631\\u062a\\u062c\\u0627\\u0644\\u064a\\u0629 \\u064a\\u0624\\u062f\\u064a\\u0647\\u0627 \\u0639\\u0634\\u0631\\u0648\\u0646 \\u0643\\u0648\\u0645\\u064a\\u062f\\u064a\\u0627\\u064b \\u0639\\u0631\\u0628\\u064a\\u0627\\u064b \\u0645\\u0646 \\u062c\\u0645\\u064a\\u0639 \\u0623\\u0646\\u062d\\u0627\\u0621 \\u0627\\u0644\\u0639\\u0627\\u0644\\u0645\\u060c \\u0648\\u064a\\u0645\\u0646\\u062d \\\"\\u0645\\u0646\\u0627 \\u0648\\u0641\\u064a\\u0646\\u0627\\\" \\u0643\\u0644\\u0627\\u064b \\u0645\\u0646\\u0647\\u0645 \\u0641\\u0631\\u0635\\u0629 \\u0645\\u0645\\u064a\\u0632\\u0629 \\u0644\\u0644\\u062a\\u0623\\u0644\\u0642 \\u062e\\u0644\\u0627\\u0644 \\u0639\\u0631\\u0636\\u0647 \\u0627\\u0644\\u0630\\u064a \\u064a\\u0633\\u062a\\u0645\\u0631 \\u0644\\u0645\\u062f\\u0629 \\u0646\\u0635\\u0641 \\u0633\\u0627\\u0639\\u0629.\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 09:03:32', '2020-06-26 09:03:32'),
(540, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-26 09:03:33', '2020-06-26 09:03:33'),
(541, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:03:36', '2020-06-26 09:03:36'),
(542, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/6', 'GET', '127.0.0.1', '[]', '2020-06-26 09:03:38', '2020-06-26 09:03:38'),
(543, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"6\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"1\",\"description\":\"1\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-26 09:03:43', '2020-06-26 09:03:43'),
(544, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:03:43', '2020-06-26 09:03:43'),
(545, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:05:21', '2020-06-26 09:05:21'),
(546, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"1\",\"description\":\"1\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:05:23', '2020-06-26 09:05:23'),
(547, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:05:23', '2020-06-26 09:05:23'),
(548, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/6', 'GET', '127.0.0.1', '[]', '2020-06-26 09:05:33', '2020-06-26 09:05:33'),
(549, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:05:34', '2020-06-26 09:05:34'),
(550, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"1\",\"description\":\"1\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:05:36', '2020-06-26 09:05:36'),
(551, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:05:36', '2020-06-26 09:05:36'),
(552, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/7', 'GET', '127.0.0.1', '[]', '2020-06-26 09:05:40', '2020-06-26 09:05:40'),
(553, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"7\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"1\",\"description\":\"1\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:05:43', '2020-06-26 09:05:43'),
(554, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:05:44', '2020-06-26 09:05:44'),
(555, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/8', 'GET', '127.0.0.1', '[]', '2020-06-26 09:05:47', '2020-06-26 09:05:47'),
(556, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"8\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"1\",\"description\":\"1\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:05:50', '2020-06-26 09:05:50'),
(557, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:05:50', '2020-06-26 09:05:50'),
(558, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/5', 'GET', '127.0.0.1', '[]', '2020-06-26 09:06:04', '2020-06-26 09:06:04'),
(559, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"5\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"1\",\"description\":\"1\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:06:07', '2020-06-26 09:06:07'),
(560, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:06:07', '2020-06-26 09:06:07'),
(561, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:06:10', '2020-06-26 09:06:10'),
(562, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"1\",\"description\":\"1\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:06:12', '2020-06-26 09:06:12'),
(563, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:06:13', '2020-06-26 09:06:13'),
(564, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:14:15', '2020-06-26 09:14:15'),
(565, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:14:18', '2020-06-26 09:14:18'),
(566, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:14:19', '2020-06-26 09:14:19');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(567, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:14:21', '2020-06-26 09:14:21'),
(568, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"dasdsa\",\"description\":\"asdadsda\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:14:36', '2020-06-26 09:14:36'),
(569, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-26 09:14:37', '2020-06-26 09:14:37'),
(570, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-26 09:14:58', '2020-06-26 09:14:58'),
(571, 1, 'ShowTvAdmin/program-episodes/6/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:15:02', '2020-06-26 09:15:02'),
(572, 1, 'ShowTvAdmin/program-episodes/6', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"dasdsa\",\"description\":\"asdadsda\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-26 09:15:03', '2020-06-26 09:15:03'),
(573, 1, 'ShowTvAdmin/program-episodes/6', 'GET', '127.0.0.1', '[]', '2020-06-26 09:15:13', '2020-06-26 09:15:13'),
(574, 1, 'ShowTvAdmin/program-episodes/6/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:15:15', '2020-06-26 09:15:15'),
(575, 1, 'ShowTvAdmin/program-episodes/6', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"dasdsa\",\"description\":\"asdadsda\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 09:15:19', '2020-06-26 09:15:19'),
(576, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-26 09:15:19', '2020-06-26 09:15:19'),
(577, 1, 'ShowTvAdmin/program-episodes/6/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:15:31', '2020-06-26 09:15:31'),
(578, 1, 'ShowTvAdmin/program-episodes/6', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"dasdsa\",\"description\":\"asdadsda\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-26 09:15:33', '2020-06-26 09:15:33'),
(579, 1, 'ShowTvAdmin/program-episodes/6', 'GET', '127.0.0.1', '[]', '2020-06-26 09:15:34', '2020-06-26 09:15:34'),
(580, 1, 'ShowTvAdmin/program-episodes/6/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 09:15:36', '2020-06-26 09:15:36'),
(581, 1, 'ShowTvAdmin/program-episodes/6/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 09:15:38', '2020-06-26 09:15:38'),
(582, 1, 'ShowTvAdmin/program-episodes/6', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"dasdsa\",\"description\":\"asdadsda\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\\/create\"}', '2020-06-26 09:15:41', '2020-06-26 09:15:41'),
(583, 1, 'ShowTvAdmin/program-episodes/6', 'GET', '127.0.0.1', '[]', '2020-06-26 09:15:41', '2020-06-26 09:15:41'),
(584, 1, 'ShowTvAdmin/program-episodes/6', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"dasdsa\",\"description\":\"asdadsda\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\\/create\"}', '2020-06-26 09:15:52', '2020-06-26 09:15:52'),
(585, 1, 'ShowTvAdmin/program-episodes/6', 'GET', '127.0.0.1', '[]', '2020-06-26 09:15:52', '2020-06-26 09:15:52'),
(586, 1, 'ShowTvAdmin/program-episodes/6', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"dasdsa\",\"description\":\"asdadsda\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\\/create\"}', '2020-06-26 09:16:14', '2020-06-26 09:16:14'),
(587, 1, 'ShowTvAdmin/program-episodes/6/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:16:17', '2020-06-26 09:16:17'),
(588, 1, 'ShowTvAdmin/program-episodes/6', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"dasdsa\",\"description\":\"asdadsda\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 09:16:56', '2020-06-26 09:16:56'),
(589, 1, 'ShowTvAdmin/program-episodes/6/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:16:58', '2020-06-26 09:16:58'),
(590, 1, 'ShowTvAdmin/program-episodes/6', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"dasdsa\",\"description\":\"asdadsda\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 09:17:11', '2020-06-26 09:17:11'),
(591, 1, 'ShowTvAdmin/program-episodes/6/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:17:14', '2020-06-26 09:17:14'),
(592, 1, 'ShowTvAdmin/program-episodes/6', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"dasdsa\",\"description\":\"asdadsda\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 09:17:47', '2020-06-26 09:17:47'),
(593, 1, 'ShowTvAdmin/program-episodes/6', 'GET', '127.0.0.1', '[]', '2020-06-26 09:17:47', '2020-06-26 09:17:47'),
(594, 1, 'ShowTvAdmin/program-episodes/6', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"dasdsa\",\"description\":\"asdadsda\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\\/create\"}', '2020-06-26 09:17:59', '2020-06-26 09:17:59'),
(595, 1, 'ShowTvAdmin/program-episodes/6', 'GET', '127.0.0.1', '[]', '2020-06-26 09:17:59', '2020-06-26 09:17:59'),
(596, 1, 'ShowTvAdmin/program-episodes/6/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 09:18:01', '2020-06-26 09:18:01'),
(597, 1, 'ShowTvAdmin/program-episodes/6', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"dasdsa\",\"description\":\"asdadsda\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\\/create\"}', '2020-06-26 09:18:03', '2020-06-26 09:18:03'),
(598, 1, 'ShowTvAdmin/program-episodes/6', 'GET', '127.0.0.1', '[]', '2020-06-26 09:18:03', '2020-06-26 09:18:03'),
(599, 1, 'ShowTvAdmin/program-episodes/6', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"dasdsa\",\"description\":\"asdadsda\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\\/create\"}', '2020-06-26 09:18:13', '2020-06-26 09:18:13'),
(600, 1, 'ShowTvAdmin/program-episodes/6/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:18:16', '2020-06-26 09:18:16'),
(601, 1, 'ShowTvAdmin/program-episodes/6', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"dasdsa\",\"description\":\"asdadsda\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 09:19:32', '2020-06-26 09:19:32'),
(602, 1, 'ShowTvAdmin/program-episodes/6/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:19:40', '2020-06-26 09:19:40'),
(603, 1, 'ShowTvAdmin/program-episodes/6', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"dasdsa\",\"description\":\"asdadsda\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 09:19:42', '2020-06-26 09:19:42'),
(604, 1, 'ShowTvAdmin/program-episodes/6/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:19:54', '2020-06-26 09:19:54'),
(605, 1, 'ShowTvAdmin/program-episodes/6', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"dasdsa\",\"description\":\"asdadsda\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 09:19:56', '2020-06-26 09:19:56'),
(606, 1, 'ShowTvAdmin/program-episodes/6/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 09:26:19', '2020-06-26 09:26:19'),
(607, 1, 'ShowTvAdmin/program-episodes/6/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 09:26:19', '2020-06-26 09:26:19'),
(608, 1, 'ShowTvAdmin/program-episodes/6', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"dasdsa\",\"description\":\"asdadsda\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\\/6\"}', '2020-06-26 09:26:23', '2020-06-26 09:26:23'),
(609, 1, 'ShowTvAdmin/program-episodes/6', 'GET', '127.0.0.1', '[]', '2020-06-26 09:26:23', '2020-06-26 09:26:23'),
(610, 1, 'ShowTvAdmin/program-episodes/6/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:26:26', '2020-06-26 09:26:26'),
(611, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-26 09:26:28', '2020-06-26 09:26:28'),
(612, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:26:31', '2020-06-26 09:26:31'),
(613, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:26:42', '2020-06-26 09:26:42'),
(614, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/5', 'GET', '127.0.0.1', '[]', '2020-06-26 09:26:44', '2020-06-26 09:26:44'),
(615, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:26:46', '2020-06-26 09:26:46'),
(616, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:27:03', '2020-06-26 09:27:03'),
(617, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:27:05', '2020-06-26 09:27:05'),
(618, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:27:10', '2020-06-26 09:27:10'),
(619, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:27:10', '2020-06-26 09:27:10'),
(620, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/6', 'GET', '127.0.0.1', '[]', '2020-06-26 09:28:13', '2020-06-26 09:28:13'),
(621, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:28:14', '2020-06-26 09:28:14'),
(622, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:28:15', '2020-06-26 09:28:15'),
(623, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:28:15', '2020-06-26 09:28:15'),
(624, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:32:18', '2020-06-26 09:32:18'),
(625, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:32:21', '2020-06-26 09:32:21'),
(626, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:32:38', '2020-06-26 09:32:38'),
(627, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:32:38', '2020-06-26 09:32:38'),
(628, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/5', 'GET', '127.0.0.1', '[]', '2020-06-26 09:32:54', '2020-06-26 09:32:54'),
(629, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:32:55', '2020-06-26 09:32:55'),
(630, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:33:04', '2020-06-26 09:33:04'),
(631, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-26 09:33:04', '2020-06-26 09:33:04'),
(632, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:33:20', '2020-06-26 09:33:20'),
(633, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-26 09:33:21', '2020-06-26 09:33:21'),
(634, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-26 09:33:21', '2020-06-26 09:33:21'),
(635, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:33:33', '2020-06-26 09:33:33'),
(636, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-26 09:33:34', '2020-06-26 09:33:34'),
(637, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-26 09:33:34', '2020-06-26 09:33:34'),
(638, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:33:41', '2020-06-26 09:33:41'),
(639, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-26 09:33:43', '2020-06-26 09:33:43'),
(640, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-26 09:33:43', '2020-06-26 09:33:43'),
(641, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:33:54', '2020-06-26 09:33:54'),
(642, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-26 09:33:55', '2020-06-26 09:33:55'),
(643, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-26 09:33:55', '2020-06-26 09:33:55'),
(644, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:34:12', '2020-06-26 09:34:12'),
(645, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-26 09:34:14', '2020-06-26 09:34:14'),
(646, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 09:34:14', '2020-06-26 09:34:14'),
(647, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 09:34:32', '2020-06-26 09:34:32'),
(648, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 09:34:32', '2020-06-26 09:34:32'),
(649, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 09:34:35', '2020-06-26 09:34:35'),
(650, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 09:34:35', '2020-06-26 09:34:35'),
(651, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 09:35:06', '2020-06-26 09:35:06'),
(652, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:35:12', '2020-06-26 09:35:12'),
(653, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 09:35:18', '2020-06-26 09:35:18'),
(654, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:35:23', '2020-06-26 09:35:23'),
(655, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 09:35:30', '2020-06-26 09:35:30'),
(656, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:35:41', '2020-06-26 09:35:41'),
(657, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:35:41', '2020-06-26 09:35:41'),
(658, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:35:43', '2020-06-26 09:35:43'),
(659, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:35:44', '2020-06-26 09:35:44'),
(660, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:35:47', '2020-06-26 09:35:47'),
(661, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:35:51', '2020-06-26 09:35:51'),
(662, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"Test\",\"description\":\"aa\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-26 09:36:07', '2020-06-26 09:36:07'),
(663, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:38:51', '2020-06-26 09:38:51'),
(664, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:38:51', '2020-06-26 09:38:51'),
(665, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:38:51', '2020-06-26 09:38:51'),
(666, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:38:51', '2020-06-26 09:38:51'),
(667, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:38:51', '2020-06-26 09:38:51'),
(668, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:38:52', '2020-06-26 09:38:52'),
(669, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:38:52', '2020-06-26 09:38:52'),
(670, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:38:52', '2020-06-26 09:38:52'),
(671, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:38:52', '2020-06-26 09:38:52'),
(672, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:38:52', '2020-06-26 09:38:52'),
(673, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:38:52', '2020-06-26 09:38:52'),
(674, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:38:52', '2020-06-26 09:38:52'),
(675, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:38:52', '2020-06-26 09:38:52'),
(676, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:38:53', '2020-06-26 09:38:53'),
(677, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:38:53', '2020-06-26 09:38:53'),
(678, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:38:53', '2020-06-26 09:38:53'),
(679, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:38:53', '2020-06-26 09:38:53'),
(680, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:38:53', '2020-06-26 09:38:53'),
(681, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:38:53', '2020-06-26 09:38:53'),
(682, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:38:53', '2020-06-26 09:38:53'),
(683, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:38:54', '2020-06-26 09:38:54'),
(684, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:38:54', '2020-06-26 09:38:54'),
(685, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:38:56', '2020-06-26 09:38:56'),
(686, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:39:27', '2020-06-26 09:39:27'),
(687, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:39:45', '2020-06-26 09:39:45'),
(688, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:39:48', '2020-06-26 09:39:48'),
(689, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"jgh\",\"description\":\"hfvj\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-26 09:40:07', '2020-06-26 09:40:07'),
(690, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:43:11', '2020-06-26 09:43:11'),
(691, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:43:14', '2020-06-26 09:43:14'),
(692, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"Test\",\"description\":\"aaa\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:43:30', '2020-06-26 09:43:30'),
(693, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:43:35', '2020-06-26 09:43:35'),
(694, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:45:48', '2020-06-26 09:45:48'),
(695, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"dfsfsdfs\",\"description\":\"dad\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:46:01', '2020-06-26 09:46:01'),
(696, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:46:01', '2020-06-26 09:46:01'),
(697, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/5', 'GET', '127.0.0.1', '[]', '2020-06-26 09:46:08', '2020-06-26 09:46:08'),
(698, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:46:09', '2020-06-26 09:46:09'),
(699, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"2\",\"title\":\"dfsfsdfs\",\"description\":\"dad\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:46:22', '2020-06-26 09:46:22'),
(700, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:46:22', '2020-06-26 09:46:22'),
(701, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/5', 'GET', '127.0.0.1', '[]', '2020-06-26 09:46:48', '2020-06-26 09:46:48'),
(702, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:46:49', '2020-06-26 09:46:49'),
(703, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"55\",\"title\":\"dfsfsdfs\",\"description\":\"dad\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:46:50', '2020-06-26 09:46:50'),
(704, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:46:51', '2020-06-26 09:46:51'),
(705, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-26 09:46:57', '2020-06-26 09:46:57'),
(706, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/7', 'GET', '127.0.0.1', '[]', '2020-06-26 09:47:17', '2020-06-26 09:47:17'),
(707, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:47:18', '2020-06-26 09:47:18'),
(708, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"55\",\"title\":\"dfsfsdfs\",\"description\":\"dad\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:47:35', '2020-06-26 09:47:35'),
(709, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:47:50', '2020-06-26 09:47:50'),
(710, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:48:06', '2020-06-26 09:48:06'),
(711, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"11\",\"title\":\"1\",\"description\":\"1\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:48:19', '2020-06-26 09:48:19'),
(712, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:48:19', '2020-06-26 09:48:19'),
(713, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/6', 'GET', '127.0.0.1', '[]', '2020-06-26 09:48:50', '2020-06-26 09:48:50'),
(714, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:48:51', '2020-06-26 09:48:51'),
(715, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"11\",\"title\":\"1\",\"description\":\"1\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:48:53', '2020-06-26 09:48:53'),
(716, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:48:53', '2020-06-26 09:48:53'),
(717, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:49:14', '2020-06-26 09:49:14'),
(718, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:49:16', '2020-06-26 09:49:16'),
(719, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"22\",\"description\":\"222\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-26 09:49:22', '2020-06-26 09:49:22'),
(720, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-26 09:49:23', '2020-06-26 09:49:23'),
(721, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:49:32', '2020-06-26 09:49:32'),
(722, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:49:57', '2020-06-26 09:49:57'),
(723, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:49:59', '2020-06-26 09:49:59'),
(724, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"22\",\"description\":\"222\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:50:05', '2020-06-26 09:50:05'),
(725, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:50:06', '2020-06-26 09:50:06'),
(726, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/5', 'GET', '127.0.0.1', '[]', '2020-06-26 09:50:10', '2020-06-26 09:50:10'),
(727, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:50:11', '2020-06-26 09:50:11'),
(728, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1111\",\"title\":\"22\",\"description\":\"222\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:50:13', '2020-06-26 09:50:13'),
(729, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:50:14', '2020-06-26 09:50:14'),
(730, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/5', 'GET', '127.0.0.1', '[]', '2020-06-26 09:50:26', '2020-06-26 09:50:26'),
(731, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:50:27', '2020-06-26 09:50:27'),
(732, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1111\",\"title\":\"22\",\"description\":\"222\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:50:29', '2020-06-26 09:50:29'),
(733, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:50:29', '2020-06-26 09:50:29'),
(734, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/5', 'GET', '127.0.0.1', '[]', '2020-06-26 09:50:45', '2020-06-26 09:50:45'),
(735, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:50:46', '2020-06-26 09:50:46'),
(736, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1111\",\"title\":\"22\",\"description\":\"222\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:50:48', '2020-06-26 09:50:48'),
(737, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:50:48', '2020-06-26 09:50:48'),
(738, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/6', 'GET', '127.0.0.1', '[]', '2020-06-26 09:50:53', '2020-06-26 09:50:53'),
(739, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:50:54', '2020-06-26 09:50:54'),
(740, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1111\",\"title\":\"1\",\"description\":\"222\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:50:56', '2020-06-26 09:50:56'),
(741, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:50:56', '2020-06-26 09:50:56'),
(742, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/6', 'GET', '127.0.0.1', '[]', '2020-06-26 09:51:02', '2020-06-26 09:51:02'),
(743, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:51:05', '2020-06-26 09:51:05'),
(744, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"1\",\"description\":\"222\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:51:06', '2020-06-26 09:51:06'),
(745, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:51:06', '2020-06-26 09:51:06'),
(746, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/5', 'GET', '127.0.0.1', '[]', '2020-06-26 09:51:27', '2020-06-26 09:51:27'),
(747, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"5\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"1\",\"description\":\"222\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:51:31', '2020-06-26 09:51:31'),
(748, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:51:31', '2020-06-26 09:51:31'),
(749, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:51:35', '2020-06-26 09:51:35'),
(750, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"111\",\"title\":\"1\",\"description\":\"222\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:51:39', '2020-06-26 09:51:39'),
(751, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:51:40', '2020-06-26 09:51:40'),
(752, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:52:20', '2020-06-26 09:52:20'),
(753, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:52:57', '2020-06-26 09:52:57'),
(754, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:53:00', '2020-06-26 09:53:00'),
(755, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"ssks\",\"description\":\"asdsa\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:53:05', '2020-06-26 09:53:05'),
(756, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:53:06', '2020-06-26 09:53:06'),
(757, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/5', 'GET', '127.0.0.1', '[]', '2020-06-26 09:53:28', '2020-06-26 09:53:28'),
(758, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:53:29', '2020-06-26 09:53:29'),
(759, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"ssks\",\"description\":\"asdsa\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:53:31', '2020-06-26 09:53:31'),
(760, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:53:31', '2020-06-26 09:53:31'),
(761, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:53:36', '2020-06-26 09:53:36'),
(762, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:54:17', '2020-06-26 09:54:17'),
(763, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:54:20', '2020-06-26 09:54:20'),
(764, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"sdadsa\",\"description\":\"dasda\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:54:27', '2020-06-26 09:54:27'),
(765, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:54:27', '2020-06-26 09:54:27'),
(766, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/7', 'GET', '127.0.0.1', '[]', '2020-06-26 09:54:56', '2020-06-26 09:54:56'),
(767, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:54:57', '2020-06-26 09:54:57'),
(768, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"12222\",\"title\":\"sdadsa\",\"description\":\"dasda\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:54:58', '2020-06-26 09:54:58'),
(769, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:54:59', '2020-06-26 09:54:59'),
(770, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/6', 'GET', '127.0.0.1', '[]', '2020-06-26 09:55:13', '2020-06-26 09:55:13'),
(771, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:55:14', '2020-06-26 09:55:14'),
(772, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"12222\",\"title\":\"sdadsa\",\"description\":\"dasda\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:55:17', '2020-06-26 09:55:17'),
(773, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:55:17', '2020-06-26 09:55:17'),
(774, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/6', 'GET', '127.0.0.1', '[]', '2020-06-26 09:55:24', '2020-06-26 09:55:24'),
(775, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:55:25', '2020-06-26 09:55:25'),
(776, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"12222\",\"title\":\"sdadsa\",\"description\":\"dasda\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:55:27', '2020-06-26 09:55:27'),
(777, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:55:27', '2020-06-26 09:55:27'),
(778, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/5', 'GET', '127.0.0.1', '[]', '2020-06-26 09:55:33', '2020-06-26 09:55:33'),
(779, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:55:34', '2020-06-26 09:55:34'),
(780, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"12222\",\"title\":\"sdadsa\",\"description\":\"dasda\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:55:36', '2020-06-26 09:55:36'),
(781, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:55:36', '2020-06-26 09:55:36'),
(782, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/6', 'GET', '127.0.0.1', '[]', '2020-06-26 09:56:03', '2020-06-26 09:56:03'),
(783, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:56:04', '2020-06-26 09:56:04'),
(784, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"12222\",\"title\":\"sdadsa\",\"description\":\"dasda\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:56:06', '2020-06-26 09:56:06'),
(785, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:56:15', '2020-06-26 09:56:15'),
(786, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/5', 'GET', '127.0.0.1', '[]', '2020-06-26 09:56:17', '2020-06-26 09:56:17'),
(787, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:56:18', '2020-06-26 09:56:18'),
(788, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"22\",\"title\":\"22\",\"description\":\"sss\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:56:25', '2020-06-26 09:56:25'),
(789, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:56:40', '2020-06-26 09:56:40'),
(790, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:56:51', '2020-06-26 09:56:51'),
(791, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"sss\",\"description\":\"sss\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:56:58', '2020-06-26 09:56:58'),
(792, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:57:10', '2020-06-26 09:57:10'),
(793, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:57:11', '2020-06-26 09:57:11'),
(794, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"22\",\"description\":\"22\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:57:16', '2020-06-26 09:57:16'),
(795, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:57:24', '2020-06-26 09:57:24'),
(796, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:57:26', '2020-06-26 09:57:26'),
(797, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"22\",\"description\":\"222\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:57:32', '2020-06-26 09:57:32'),
(798, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:57:41', '2020-06-26 09:57:41'),
(799, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:57:43', '2020-06-26 09:57:43'),
(800, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"2\",\"title\":\"dasad\",\"description\":\"dasdsa\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:57:50', '2020-06-26 09:57:50'),
(801, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:57:52', '2020-06-26 09:57:52'),
(802, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:58:08', '2020-06-26 09:58:08'),
(803, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"4334\",\"description\":\"13232\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:58:15', '2020-06-26 09:58:15'),
(804, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:58:17', '2020-06-26 09:58:17'),
(805, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:58:34', '2020-06-26 09:58:34'),
(806, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"ss\",\"description\":\"ww\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:58:39', '2020-06-26 09:58:39'),
(807, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:58:39', '2020-06-26 09:58:39'),
(808, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/5', 'GET', '127.0.0.1', '[]', '2020-06-26 09:58:41', '2020-06-26 09:58:41'),
(809, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:58:43', '2020-06-26 09:58:43'),
(810, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1111\",\"title\":\"ss\",\"description\":\"ww\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:58:51', '2020-06-26 09:58:51'),
(811, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:58:51', '2020-06-26 09:58:51'),
(812, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/5', 'GET', '127.0.0.1', '[]', '2020-06-26 09:59:28', '2020-06-26 09:59:28'),
(813, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:59:29', '2020-06-26 09:59:29'),
(814, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1111\",\"title\":\"ss\",\"description\":\"ww\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:59:32', '2020-06-26 09:59:32'),
(815, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 09:59:43', '2020-06-26 09:59:43'),
(816, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:59:43', '2020-06-26 09:59:43'),
(817, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:59:43', '2020-06-26 09:59:43'),
(818, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:59:44', '2020-06-26 09:59:44'),
(819, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:59:44', '2020-06-26 09:59:44'),
(820, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:59:44', '2020-06-26 09:59:44'),
(821, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:59:44', '2020-06-26 09:59:44'),
(822, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:59:44', '2020-06-26 09:59:44'),
(823, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:59:44', '2020-06-26 09:59:44'),
(824, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:59:44', '2020-06-26 09:59:44'),
(825, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:59:44', '2020-06-26 09:59:44'),
(826, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:59:45', '2020-06-26 09:59:45'),
(827, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:59:45', '2020-06-26 09:59:45'),
(828, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:59:45', '2020-06-26 09:59:45'),
(829, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:59:45', '2020-06-26 09:59:45'),
(830, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:59:45', '2020-06-26 09:59:45'),
(831, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:59:45', '2020-06-26 09:59:45'),
(832, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:59:45', '2020-06-26 09:59:45'),
(833, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:59:45', '2020-06-26 09:59:45'),
(834, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:59:45', '2020-06-26 09:59:45'),
(835, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:59:45', '2020-06-26 09:59:45'),
(836, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:59:46', '2020-06-26 09:59:46'),
(837, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:59:47', '2020-06-26 09:59:47'),
(838, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 09:59:50', '2020-06-26 09:59:50'),
(839, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"ss\",\"description\":\"ssdsd\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 09:59:55', '2020-06-26 09:59:55'),
(840, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 09:59:55', '2020-06-26 09:59:55'),
(841, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/6', 'GET', '127.0.0.1', '[]', '2020-06-26 10:00:02', '2020-06-26 10:00:02'),
(842, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 10:00:03', '2020-06-26 10:00:03');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(843, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"ss\",\"description\":\"ssdsd\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 10:00:05', '2020-06-26 10:00:05'),
(844, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:00:38', '2020-06-26 10:00:38'),
(845, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 10:00:40', '2020-06-26 10:00:40'),
(846, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"dsada\",\"description\":\"asdda\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 10:00:55', '2020-06-26 10:00:55'),
(847, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:01:12', '2020-06-26 10:01:12'),
(848, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 10:01:14', '2020-06-26 10:01:14'),
(849, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 10:01:17', '2020-06-26 10:01:17'),
(850, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/5', 'GET', '127.0.0.1', '[]', '2020-06-26 10:01:22', '2020-06-26 10:01:22'),
(851, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"5\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"\\u0627\\u0644\\u0628\\u0631\\u064a\\u062f \\u0627\\u0644\\u0645\\u0633\\u062c\\u0644\",\"description\":\"ss\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 10:01:28', '2020-06-26 10:01:28'),
(852, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 10:01:29', '2020-06-26 10:01:29'),
(853, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:01:30', '2020-06-26 10:01:30'),
(854, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 10:01:43', '2020-06-26 10:01:43'),
(855, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 10:01:47', '2020-06-26 10:01:47'),
(856, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 10:01:51', '2020-06-26 10:01:51'),
(857, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"\\u0627\\u0644\\u0628\\u0631\\u064a\\u062f \\u0627\\u0644\\u0645\\u0633\\u062c\\u0644\",\"description\":\"ss\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 10:01:53', '2020-06-26 10:01:53'),
(858, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 10:01:53', '2020-06-26 10:01:53'),
(859, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/5', 'GET', '127.0.0.1', '[]', '2020-06-26 10:02:18', '2020-06-26 10:02:18'),
(860, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 10:02:19', '2020-06-26 10:02:19'),
(861, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"\\u0627\\u0644\\u0628\\u0631\\u064a\\u062f \\u0627\\u0644\\u0645\\u0633\\u062c\\u0644\",\"description\":\"ss\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 10:02:21', '2020-06-26 10:02:21'),
(862, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 10:02:21', '2020-06-26 10:02:21'),
(863, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 10:02:29', '2020-06-26 10:02:29'),
(864, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 10:02:32', '2020-06-26 10:02:32'),
(865, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 10:02:40', '2020-06-26 10:02:40'),
(866, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"awweq\",\"description\":\"dwq\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 10:02:46', '2020-06-26 10:02:46'),
(867, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 10:02:46', '2020-06-26 10:02:46'),
(868, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-26 10:03:57', '2020-06-26 10:03:57'),
(869, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:03:59', '2020-06-26 10:03:59'),
(870, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 10:04:41', '2020-06-26 10:04:41'),
(871, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 10:04:44', '2020-06-26 10:04:44'),
(872, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"ss\",\"description\":\"ss\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-26 10:04:51', '2020-06-26 10:04:51'),
(873, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 10:04:51', '2020-06-26 10:04:51'),
(874, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/5', 'GET', '127.0.0.1', '[]', '2020-06-26 10:05:09', '2020-06-26 10:05:09'),
(875, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 10:05:10', '2020-06-26 10:05:10'),
(876, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"ss\",\"description\":\"ss\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 10:05:13', '2020-06-26 10:05:13'),
(877, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-26 10:05:13', '2020-06-26 10:05:13'),
(878, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:05:15', '2020-06-26 10:05:15'),
(879, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 10:05:33', '2020-06-26 10:05:33'),
(880, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/5', 'GET', '127.0.0.1', '[]', '2020-06-26 10:05:35', '2020-06-26 10:05:35'),
(881, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 10:05:36', '2020-06-26 10:05:36'),
(882, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"dsadsa\",\"description\":\"dada\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 10:05:42', '2020-06-26 10:05:42'),
(883, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 10:05:42', '2020-06-26 10:05:42'),
(884, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/5', 'GET', '127.0.0.1', '[]', '2020-06-26 10:05:51', '2020-06-26 10:05:51'),
(885, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 10:05:53', '2020-06-26 10:05:53'),
(886, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"dsadsa\",\"description\":\"dada\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 10:05:56', '2020-06-26 10:05:56'),
(887, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-26 10:05:56', '2020-06-26 10:05:56'),
(888, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:05:58', '2020-06-26 10:05:58'),
(889, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/5', 'GET', '127.0.0.1', '[]', '2020-06-26 10:06:10', '2020-06-26 10:06:10'),
(890, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 10:06:12', '2020-06-26 10:06:12'),
(891, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"dsadsadas\",\"description\":\"dasdas\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-26 10:06:17', '2020-06-26 10:06:17'),
(892, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:07:03', '2020-06-26 10:07:03'),
(893, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 10:07:05', '2020-06-26 10:07:05'),
(894, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 10:07:07', '2020-06-26 10:07:07'),
(895, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"das\",\"description\":\"ads\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-26 10:07:12', '2020-06-26 10:07:12'),
(896, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-26 10:07:41', '2020-06-26 10:07:41'),
(897, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:07:47', '2020-06-26 10:07:47'),
(898, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 10:07:50', '2020-06-26 10:07:50'),
(899, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"dsadas\",\"description\":\"adsdas\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-26 10:07:54', '2020-06-26 10:07:54'),
(900, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"dsadas\",\"description\":\"adsdas\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-26 10:08:41', '2020-06-26 10:08:41'),
(901, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-26 10:08:41', '2020-06-26 10:08:41'),
(902, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:08:50', '2020-06-26 10:08:50'),
(903, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 10:08:55', '2020-06-26 10:08:55'),
(904, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 10:08:59', '2020-06-26 10:08:59'),
(905, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"dsadsa\",\"description\":\"asddas\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-26 10:09:04', '2020-06-26 10:09:04'),
(906, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 10:09:04', '2020-06-26 10:09:04'),
(907, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-26 10:09:33', '2020-06-26 10:09:33'),
(908, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:09:44', '2020-06-26 10:09:44'),
(909, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 10:09:48', '2020-06-26 10:09:48'),
(910, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"dsa\",\"description\":\"ad\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-26 10:09:53', '2020-06-26 10:09:53'),
(911, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 10:09:54', '2020-06-26 10:09:54'),
(912, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-26 10:10:57', '2020-06-26 10:10:57'),
(913, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:11:01', '2020-06-26 10:11:01'),
(914, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/5', 'GET', '127.0.0.1', '[]', '2020-06-26 10:11:04', '2020-06-26 10:11:04'),
(915, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 10:11:06', '2020-06-26 10:11:06'),
(916, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 10:12:14', '2020-06-26 10:12:14'),
(917, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 10:12:16', '2020-06-26 10:12:16'),
(918, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"Test\",\"description\":\"test22\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-26 10:12:37', '2020-06-26 10:12:37'),
(919, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-26 10:12:37', '2020-06-26 10:12:37'),
(920, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/5', 'GET', '127.0.0.1', '[]', '2020-06-26 10:13:00', '2020-06-26 10:13:00'),
(921, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 10:13:01', '2020-06-26 10:13:01'),
(922, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"22\",\"title\":\"Test\",\"description\":\"test22\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\"}', '2020-06-26 10:13:03', '2020-06-26 10:13:03'),
(923, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-26 10:13:03', '2020-06-26 10:13:03'),
(924, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:13:06', '2020-06-26 10:13:06'),
(925, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:14:58', '2020-06-26 10:14:58'),
(926, 1, 'ShowTvAdmin/program-episodes/8/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:15:02', '2020-06-26 10:15:02'),
(927, 1, 'ShowTvAdmin/program-episodes/8', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"22\",\"title\":\"Test\",\"description\":\"test22\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-26 10:15:04', '2020-06-26 10:15:04'),
(928, 1, 'ShowTvAdmin/program-episodes/8/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:15:04', '2020-06-26 10:15:04'),
(929, 1, 'ShowTvAdmin/program-episodes/8', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"22\",\"title\":\"Test\",\"description\":\"test22\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 10:15:16', '2020-06-26 10:15:16'),
(930, 1, 'ShowTvAdmin/program-episodes/8/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:15:17', '2020-06-26 10:15:17'),
(931, 1, 'ShowTvAdmin/program-episodes/8', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"22\",\"title\":\"Test\",\"description\":\"test22\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 10:15:20', '2020-06-26 10:15:20'),
(932, 1, 'ShowTvAdmin/program-episodes/8/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:15:20', '2020-06-26 10:15:20'),
(933, 1, 'ShowTvAdmin/program-episodes/8', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"22\",\"title\":\"Test\",\"description\":\"test22\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 10:15:34', '2020-06-26 10:15:34'),
(934, 1, 'ShowTvAdmin/program-episodes/8', 'GET', '127.0.0.1', '[]', '2020-06-26 10:15:34', '2020-06-26 10:15:34'),
(935, 1, 'ShowTvAdmin/program-episodes/8/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:15:42', '2020-06-26 10:15:42'),
(936, 1, 'ShowTvAdmin/program-episodes/8', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"22\",\"title\":\"Test\",\"description\":\"test22\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-26 10:15:44', '2020-06-26 10:15:44'),
(937, 1, 'ShowTvAdmin/program-episodes/8', 'GET', '127.0.0.1', '[]', '2020-06-26 10:16:00', '2020-06-26 10:16:00'),
(938, 1, 'ShowTvAdmin/program-episodes/8/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:16:02', '2020-06-26 10:16:02'),
(939, 1, 'ShowTvAdmin/program-episodes/8/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:16:02', '2020-06-26 10:16:02'),
(940, 1, 'ShowTvAdmin/program-episodes/8/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:16:02', '2020-06-26 10:16:02'),
(941, 1, 'ShowTvAdmin/program-episodes/8/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:16:02', '2020-06-26 10:16:02'),
(942, 1, 'ShowTvAdmin/program-episodes/8/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:16:02', '2020-06-26 10:16:02'),
(943, 1, 'ShowTvAdmin/program-episodes/8/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:16:02', '2020-06-26 10:16:02'),
(944, 1, 'ShowTvAdmin/program-episodes/8/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:16:02', '2020-06-26 10:16:02'),
(945, 1, 'ShowTvAdmin/program-episodes/8/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:16:03', '2020-06-26 10:16:03'),
(946, 1, 'ShowTvAdmin/program-episodes/8/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:16:03', '2020-06-26 10:16:03'),
(947, 1, 'ShowTvAdmin/program-episodes/8/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:16:03', '2020-06-26 10:16:03'),
(948, 1, 'ShowTvAdmin/program-episodes/8/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:16:03', '2020-06-26 10:16:03'),
(949, 1, 'ShowTvAdmin/program-episodes/8/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:16:03', '2020-06-26 10:16:03'),
(950, 1, 'ShowTvAdmin/program-episodes/8/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:16:03', '2020-06-26 10:16:03'),
(951, 1, 'ShowTvAdmin/program-episodes/8/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:16:13', '2020-06-26 10:16:13'),
(952, 1, 'ShowTvAdmin/program-episodes/8/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:16:17', '2020-06-26 10:16:17'),
(953, 1, 'ShowTvAdmin/program-episodes/8', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"22\",\"title\":\"Test\",\"description\":\"test22\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-26 10:16:20', '2020-06-26 10:16:20'),
(954, 1, 'ShowTvAdmin/program-episodes/8', 'GET', '127.0.0.1', '[]', '2020-06-26 10:16:36', '2020-06-26 10:16:36'),
(955, 1, 'ShowTvAdmin/program-episodes/8/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:16:38', '2020-06-26 10:16:38'),
(956, 1, 'ShowTvAdmin/program-episodes/8/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:16:40', '2020-06-26 10:16:40'),
(957, 1, 'ShowTvAdmin/program-episodes/8/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:16:43', '2020-06-26 10:16:43'),
(958, 1, 'ShowTvAdmin/program-episodes/8/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:17:09', '2020-06-26 10:17:09'),
(959, 1, 'ShowTvAdmin/program-episodes/8/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:17:14', '2020-06-26 10:17:14'),
(960, 1, 'ShowTvAdmin/program-episodes/8/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:18:34', '2020-06-26 10:18:34'),
(961, 1, 'ShowTvAdmin/program-episodes/8/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:20:19', '2020-06-26 10:20:19'),
(962, 1, 'ShowTvAdmin/program-episodes/8/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:20:56', '2020-06-26 10:20:56'),
(963, 1, 'ShowTvAdmin/program-episodes/8/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:21:14', '2020-06-26 10:21:14'),
(964, 1, 'ShowTvAdmin/program-episodes/8', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"22\",\"title\":\"Test\",\"description\":\"test22\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-26 10:21:17', '2020-06-26 10:21:17'),
(965, 1, 'ShowTvAdmin/program-episodes/8', 'GET', '127.0.0.1', '[]', '2020-06-26 10:21:24', '2020-06-26 10:21:24'),
(966, 1, 'ShowTvAdmin/program-episodes/8/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:21:26', '2020-06-26 10:21:26'),
(967, 1, 'ShowTvAdmin/program-episodes/8', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"22\",\"title\":\"Test\",\"description\":\"test22\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 10:21:28', '2020-06-26 10:21:28'),
(968, 1, 'ShowTvAdmin/program-episodes/8/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:21:34', '2020-06-26 10:21:34'),
(969, 1, 'ShowTvAdmin/program-episodes/8', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"22\",\"title\":\"Test\",\"description\":\"test22\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 10:21:36', '2020-06-26 10:21:36'),
(970, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-26 10:21:36', '2020-06-26 10:21:36'),
(971, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:21:41', '2020-06-26 10:21:41'),
(972, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-26 10:21:43', '2020-06-26 10:21:43'),
(973, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:21:43', '2020-06-26 10:21:43'),
(974, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:30:23', '2020-06-26 10:30:23'),
(975, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-26 10:33:16', '2020-06-26 10:33:16'),
(976, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:33:48', '2020-06-26 10:33:48'),
(977, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 10:33:53', '2020-06-26 10:33:53'),
(978, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:33:53', '2020-06-26 10:33:53'),
(979, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 10:36:01', '2020-06-26 10:36:01'),
(980, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:36:01', '2020-06-26 10:36:01'),
(981, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 10:36:40', '2020-06-26 10:36:40'),
(982, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:36:40', '2020-06-26 10:36:40'),
(983, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-28\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 10:36:55', '2020-06-26 10:36:55'),
(984, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:36:56', '2020-06-26 10:36:56'),
(985, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-28\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 10:37:21', '2020-06-26 10:37:21'),
(986, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:37:23', '2020-06-26 10:37:23'),
(987, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 10:37:38', '2020-06-26 10:37:38'),
(988, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:39:03', '2020-06-26 10:39:03'),
(989, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 10:39:05', '2020-06-26 10:39:05'),
(990, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:39:07', '2020-06-26 10:39:07'),
(991, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 10:39:15', '2020-06-26 10:39:15'),
(992, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:39:21', '2020-06-26 10:39:21'),
(993, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:39:21', '2020-06-26 10:39:21'),
(994, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-26 10:39:34', '2020-06-26 10:39:34'),
(995, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:39:34', '2020-06-26 10:39:34'),
(996, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-28\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 10:39:40', '2020-06-26 10:39:40'),
(997, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:39:40', '2020-06-26 10:39:40'),
(998, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-28\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 10:40:02', '2020-06-26 10:40:02'),
(999, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:40:05', '2020-06-26 10:40:05'),
(1000, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-28\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 10:40:10', '2020-06-26 10:40:10'),
(1001, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:40:13', '2020-06-26 10:40:13'),
(1002, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-27\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 10:40:16', '2020-06-26 10:40:16'),
(1003, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:40:21', '2020-06-26 10:40:21'),
(1004, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-28\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 10:41:51', '2020-06-26 10:41:51'),
(1005, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:41:56', '2020-06-26 10:41:56'),
(1006, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 10:41:58', '2020-06-26 10:41:58'),
(1007, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:42:00', '2020-06-26 10:42:00'),
(1008, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 10:42:47', '2020-06-26 10:42:47'),
(1009, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:42:49', '2020-06-26 10:42:49'),
(1010, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:43:28', '2020-06-26 10:43:28'),
(1011, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-26 10:43:31', '2020-06-26 10:43:31'),
(1012, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:43:31', '2020-06-26 10:43:31'),
(1013, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-28\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 10:43:40', '2020-06-26 10:43:40'),
(1014, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:43:40', '2020-06-26 10:43:40'),
(1015, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-28\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 10:43:45', '2020-06-26 10:43:45'),
(1016, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:43:45', '2020-06-26 10:43:45'),
(1017, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-28\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 10:44:02', '2020-06-26 10:44:02'),
(1018, 1, 'ShowTvAdmin/program-episodes/7', 'GET', '127.0.0.1', '[]', '2020-06-26 10:44:02', '2020-06-26 10:44:02'),
(1019, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:44:21', '2020-06-26 10:44:21'),
(1020, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-28\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-26 10:44:27', '2020-06-26 10:44:27'),
(1021, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:44:37', '2020-06-26 10:44:37'),
(1022, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 10:44:39', '2020-06-26 10:44:39'),
(1023, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:44:41', '2020-06-26 10:44:41'),
(1024, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-28\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 10:44:45', '2020-06-26 10:44:45'),
(1025, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:44:55', '2020-06-26 10:44:55'),
(1026, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-26\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 10:44:57', '2020-06-26 10:44:57'),
(1027, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:44:57', '2020-06-26 10:44:57'),
(1028, 1, 'ShowTvAdmin/program-episodes/7', 'PUT', '127.0.0.1', '{\"program_id\":\"4\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-28\",\"episode_no\":\"1\",\"title\":\"s\",\"description\":\"s\",\"_token\":\"HAJhwFiKsI5Px4UcXkUU75b2JwJaLDVSawmJg1iJ\",\"_method\":\"PUT\"}', '2020-06-26 10:45:06', '2020-06-26 10:45:06'),
(1029, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:45:06', '2020-06-26 10:45:06'),
(1030, 1, 'ShowTvAdmin/program-episodes/7/edit', 'GET', '127.0.0.1', '[]', '2020-06-26 10:45:34', '2020-06-26 10:45:34'),
(1031, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:53:09', '2020-06-26 10:53:09'),
(1032, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 10:53:34', '2020-06-26 10:53:34'),
(1033, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 10:53:44', '2020-06-26 10:53:44'),
(1034, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:54:18', '2020-06-26 10:54:18'),
(1035, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:55:37', '2020-06-26 10:55:37'),
(1036, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:55:44', '2020-06-26 10:55:44'),
(1037, 1, 'ShowTvAdmin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:55:45', '2020-06-26 10:55:45'),
(1038, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:55:47', '2020-06-26 10:55:47'),
(1039, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:55:48', '2020-06-26 10:55:48'),
(1040, 1, 'ShowTvAdmin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:55:49', '2020-06-26 10:55:49'),
(1041, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 10:55:58', '2020-06-26 10:55:58'),
(1042, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-26 18:22:58', '2020-06-26 18:22:58'),
(1043, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 18:23:11', '2020-06-26 18:23:11'),
(1044, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:23:20', '2020-06-26 18:23:20'),
(1045, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:23:27', '2020-06-26 18:23:27'),
(1046, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:28:19', '2020-06-26 18:28:19'),
(1047, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:28:44', '2020-06-26 18:28:44'),
(1048, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:28:46', '2020-06-26 18:28:46'),
(1049, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:28:47', '2020-06-26 18:28:47'),
(1050, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:28:50', '2020-06-26 18:28:50'),
(1051, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:28:57', '2020-06-26 18:28:57'),
(1052, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:29:07', '2020-06-26 18:29:07'),
(1053, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:29:20', '2020-06-26 18:29:20'),
(1054, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:30:34', '2020-06-26 18:30:34'),
(1055, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:31:27', '2020-06-26 18:31:27'),
(1056, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:32:07', '2020-06-26 18:32:07'),
(1057, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:32:23', '2020-06-26 18:32:23'),
(1058, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:32:35', '2020-06-26 18:32:35'),
(1059, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:33:08', '2020-06-26 18:33:08'),
(1060, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:33:13', '2020-06-26 18:33:13'),
(1061, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:34:49', '2020-06-26 18:34:49'),
(1062, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:34:53', '2020-06-26 18:34:53'),
(1063, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:35:04', '2020-06-26 18:35:04'),
(1064, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:35:17', '2020-06-26 18:35:17'),
(1065, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:35:52', '2020-06-26 18:35:52'),
(1066, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:35:58', '2020-06-26 18:35:58'),
(1067, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:36:11', '2020-06-26 18:36:11'),
(1068, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:36:20', '2020-06-26 18:36:20'),
(1069, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:38:45', '2020-06-26 18:38:45'),
(1070, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:38:58', '2020-06-26 18:38:58'),
(1071, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:39:08', '2020-06-26 18:39:08'),
(1072, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:39:20', '2020-06-26 18:39:20'),
(1073, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 18:39:26', '2020-06-26 18:39:26'),
(1074, 1, 'ShowTvAdmin/programs/4', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 18:39:30', '2020-06-26 18:39:30'),
(1075, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 18:39:42', '2020-06-26 18:39:42'),
(1076, 1, 'ShowTvAdmin/programs/4', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 18:39:42', '2020-06-26 18:39:42'),
(1077, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 18:39:44', '2020-06-26 18:39:44'),
(1078, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:39:55', '2020-06-26 18:39:55'),
(1079, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:43:17', '2020-06-26 18:43:17'),
(1080, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:45:15', '2020-06-26 18:45:15'),
(1081, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:46:58', '2020-06-26 18:46:58'),
(1082, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:47:40', '2020-06-26 18:47:40'),
(1083, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:48:01', '2020-06-26 18:48:01'),
(1084, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:48:09', '2020-06-26 18:48:09'),
(1085, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:48:24', '2020-06-26 18:48:24'),
(1086, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:48:28', '2020-06-26 18:48:28'),
(1087, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:49:14', '2020-06-26 18:49:14'),
(1088, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:49:50', '2020-06-26 18:49:50'),
(1089, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:50:20', '2020-06-26 18:50:20'),
(1090, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:50:35', '2020-06-26 18:50:35'),
(1091, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:50:41', '2020-06-26 18:50:41'),
(1092, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:51:08', '2020-06-26 18:51:08'),
(1093, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:51:45', '2020-06-26 18:51:45'),
(1094, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:52:40', '2020-06-26 18:52:40'),
(1095, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:52:51', '2020-06-26 18:52:51'),
(1096, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:53:06', '2020-06-26 18:53:06'),
(1097, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:53:40', '2020-06-26 18:53:40'),
(1098, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:54:21', '2020-06-26 18:54:21'),
(1099, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:54:24', '2020-06-26 18:54:24'),
(1100, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:54:24', '2020-06-26 18:54:24'),
(1101, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:55:14', '2020-06-26 18:55:14'),
(1102, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:55:23', '2020-06-26 18:55:23'),
(1103, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:56:44', '2020-06-26 18:56:44'),
(1104, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 18:56:49', '2020-06-26 18:56:49'),
(1105, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 19:00:30', '2020-06-26 19:00:30'),
(1106, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 19:01:42', '2020-06-26 19:01:42'),
(1107, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 19:01:42', '2020-06-26 19:01:42'),
(1108, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 19:01:43', '2020-06-26 19:01:43'),
(1109, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 19:01:43', '2020-06-26 19:01:43'),
(1110, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 19:01:44', '2020-06-26 19:01:44'),
(1111, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 19:01:44', '2020-06-26 19:01:44'),
(1112, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 19:01:44', '2020-06-26 19:01:44'),
(1113, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 19:01:56', '2020-06-26 19:01:56'),
(1114, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 19:02:00', '2020-06-26 19:02:00'),
(1115, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 19:02:10', '2020-06-26 19:02:10'),
(1116, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 19:03:33', '2020-06-26 19:03:33'),
(1117, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 19:07:55', '2020-06-26 19:07:55'),
(1118, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 19:08:13', '2020-06-26 19:08:13'),
(1119, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 19:11:40', '2020-06-26 19:11:40'),
(1120, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 19:11:52', '2020-06-26 19:11:52'),
(1121, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 19:12:20', '2020-06-26 19:12:20'),
(1122, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 19:12:28', '2020-06-26 19:12:28'),
(1123, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-26 19:14:35', '2020-06-26 19:14:35'),
(1124, 1, 'ShowTvAdmin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 19:15:25', '2020-06-26 19:15:25'),
(1125, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 19:15:26', '2020-06-26 19:15:26'),
(1126, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 19:15:26', '2020-06-26 19:15:26'),
(1127, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 19:15:32', '2020-06-26 19:15:32'),
(1128, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/5', 'GET', '127.0.0.1', '[]', '2020-06-26 19:15:33', '2020-06-26 19:15:33'),
(1129, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/4', 'GET', '127.0.0.1', '[]', '2020-06-26 19:15:35', '2020-06-26 19:15:35'),
(1130, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/7', 'GET', '127.0.0.1', '[]', '2020-06-26 19:15:37', '2020-06-26 19:15:37'),
(1131, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 19:17:14', '2020-06-26 19:17:14'),
(1132, 1, 'ShowTvAdmin/auth/permissions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 19:17:17', '2020-06-26 19:17:17'),
(1133, 1, 'ShowTvAdmin/auth/roles', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 19:17:21', '2020-06-26 19:17:21'),
(1134, 1, 'ShowTvAdmin/auth/roles/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 19:17:25', '2020-06-26 19:17:25'),
(1135, 1, 'ShowTvAdmin/auth/permissions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 19:17:29', '2020-06-26 19:17:29'),
(1136, 1, 'ShowTvAdmin/auth/permissions/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 19:17:30', '2020-06-26 19:17:30'),
(1137, 1, 'ShowTvAdmin/auth/roles', 'GET', '127.0.0.1', '[]', '2020-06-26 19:17:44', '2020-06-26 19:17:44'),
(1138, 1, 'ShowTvAdmin/auth/permissions', 'GET', '127.0.0.1', '[]', '2020-06-26 19:17:47', '2020-06-26 19:17:47'),
(1139, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '[]', '2020-06-26 19:18:05', '2020-06-26 19:18:05'),
(1140, 1, 'ShowTvAdmin/auth/permissions', 'POST', '127.0.0.1', '{\"slug\":\"CRUD on programs\",\"name\":\"Crud on programs\",\"http_method\":[null],\"http_path\":\"\\/ShowTvAdmin\\/programs*\",\"_token\":\"rqru3Qe5jLxaZl82qQ1oPph6P8I0OIEMr38gP76P\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/auth\\/permissions\"}', '2020-06-26 19:18:39', '2020-06-26 19:18:39'),
(1141, 1, 'ShowTvAdmin/auth/permissions', 'GET', '127.0.0.1', '[]', '2020-06-26 19:18:40', '2020-06-26 19:18:40'),
(1142, 1, 'ShowTvAdmin/auth/permissions/6/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 19:18:44', '2020-06-26 19:18:44'),
(1143, 1, 'ShowTvAdmin/auth/permissions/6', 'PUT', '127.0.0.1', '{\"slug\":\"CRUD on programs\",\"name\":\"Crud on programs\",\"http_method\":[null],\"http_path\":\"programs*\",\"_token\":\"rqru3Qe5jLxaZl82qQ1oPph6P8I0OIEMr38gP76P\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/auth\\/permissions\"}', '2020-06-26 19:18:50', '2020-06-26 19:18:50'),
(1144, 1, 'ShowTvAdmin/auth/permissions', 'GET', '127.0.0.1', '[]', '2020-06-26 19:18:50', '2020-06-26 19:18:50'),
(1145, 1, 'ShowTvAdmin/auth/permissions/6/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 19:18:56', '2020-06-26 19:18:56'),
(1146, 1, 'ShowTvAdmin/auth/permissions/6', 'PUT', '127.0.0.1', '{\"slug\":\"CRUD on programs\",\"name\":\"Crud on programs\",\"http_method\":[null],\"http_path\":\"\\/programs*\",\"_token\":\"rqru3Qe5jLxaZl82qQ1oPph6P8I0OIEMr38gP76P\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/auth\\/permissions\"}', '2020-06-26 19:19:00', '2020-06-26 19:19:00'),
(1147, 1, 'ShowTvAdmin/auth/permissions', 'GET', '127.0.0.1', '[]', '2020-06-26 19:19:00', '2020-06-26 19:19:00'),
(1148, 1, 'ShowTvAdmin/auth/permissions/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 19:19:10', '2020-06-26 19:19:10');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(1149, 1, 'ShowTvAdmin/users', 'GET', '127.0.0.1', '[]', '2020-06-26 19:19:26', '2020-06-26 19:19:26'),
(1150, 1, 'ShowTvAdmin/auth/permissions', 'POST', '127.0.0.1', '{\"slug\":\"can view users\",\"name\":\"can view user\",\"http_method\":[\"GET\",null],\"http_path\":\"\\/users*\",\"_token\":\"rqru3Qe5jLxaZl82qQ1oPph6P8I0OIEMr38gP76P\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/auth\\/permissions\"}', '2020-06-26 19:19:38', '2020-06-26 19:19:38'),
(1151, 1, 'ShowTvAdmin/auth/permissions', 'GET', '127.0.0.1', '[]', '2020-06-26 19:19:38', '2020-06-26 19:19:38'),
(1152, 1, 'ShowTvAdmin/auth/permissions/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 19:19:42', '2020-06-26 19:19:42'),
(1153, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-26 19:19:56', '2020-06-26 19:19:56'),
(1154, 1, 'ShowTvAdmin/auth/permissions', 'POST', '127.0.0.1', '{\"slug\":\"CRUD on Episodes\",\"name\":\"CRUD on Episodes\",\"http_method\":[null],\"http_path\":\"\\/program-episodes*\",\"_token\":\"rqru3Qe5jLxaZl82qQ1oPph6P8I0OIEMr38gP76P\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/auth\\/permissions\"}', '2020-06-26 19:20:07', '2020-06-26 19:20:07'),
(1155, 1, 'ShowTvAdmin/auth/permissions', 'GET', '127.0.0.1', '[]', '2020-06-26 19:20:08', '2020-06-26 19:20:08'),
(1156, 1, 'ShowTvAdmin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 19:20:11', '2020-06-26 19:20:11'),
(1157, 1, 'ShowTvAdmin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 19:20:14', '2020-06-26 19:20:14'),
(1158, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-26 19:20:17', '2020-06-26 19:20:17'),
(1159, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-27 07:09:42', '2020-06-27 07:09:42'),
(1160, 1, 'ShowTvAdmin/program-episodes/1,2,3,4,6,7,8', 'DELETE', '127.0.0.1', '{\"_method\":\"delete\",\"_token\":\"bTXRE7wKuJ5QeZ0lu6gGOU77QENIjB9eIgEcKGE9\"}', '2020-06-27 07:11:31', '2020-06-27 07:11:31'),
(1161, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 07:11:37', '2020-06-27 07:11:37'),
(1162, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 07:11:40', '2020-06-27 07:11:40'),
(1163, 1, 'ShowTvAdmin/programs/4,5,6,7,8,9', 'DELETE', '127.0.0.1', '{\"_method\":\"delete\",\"_token\":\"bTXRE7wKuJ5QeZ0lu6gGOU77QENIjB9eIgEcKGE9\"}', '2020-06-27 07:11:45', '2020-06-27 07:11:45'),
(1164, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 07:11:45', '2020-06-27 07:11:45'),
(1165, 1, 'ShowTvAdmin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 07:11:48', '2020-06-27 07:11:48'),
(1166, 1, 'ShowTvAdmin/users', 'GET', '127.0.0.1', '[]', '2020-06-27 07:14:41', '2020-06-27 07:14:41'),
(1167, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 07:14:44', '2020-06-27 07:14:44'),
(1168, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 07:14:46', '2020-06-27 07:14:46'),
(1169, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '[]', '2020-06-27 07:15:07', '2020-06-27 07:15:07'),
(1170, 1, 'ShowTvAdmin/programs/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 07:15:08', '2020-06-27 07:15:08'),
(1171, 1, 'ShowTvAdmin/programs', 'POST', '127.0.0.1', '{\"program_type_id\":\"1\",\"title\":\"Caravan - \\u0643\\u0631\\u0641\\u0627\\u0646\",\"description\":\"A paragraph that focuses on fashion and fashion and ways to coordinate clothes, Roya channel\",\"broadcast_start_date\":\"2020-06-27\",\"broadcast_start_time\":\"10:00:00\",\"broadcast_end_time\":\"11:00:00\",\"broadCastDaysProgram\":[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",null],\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"bTXRE7wKuJ5QeZ0lu6gGOU77QENIjB9eIgEcKGE9\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/programs\"}', '2020-06-27 07:17:04', '2020-06-27 07:17:04'),
(1172, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '[]', '2020-06-27 07:17:04', '2020-06-27 07:17:04'),
(1173, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:17:10', '2020-06-27 07:17:10'),
(1174, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:18:04', '2020-06-27 07:18:04'),
(1175, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:18:08', '2020-06-27 07:18:08'),
(1176, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:18:08', '2020-06-27 07:18:08'),
(1177, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:18:12', '2020-06-27 07:18:12'),
(1178, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:18:13', '2020-06-27 07:18:13'),
(1179, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:18:14', '2020-06-27 07:18:14'),
(1180, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:18:25', '2020-06-27 07:18:25'),
(1181, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:18:35', '2020-06-27 07:18:35'),
(1182, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:18:39', '2020-06-27 07:18:39'),
(1183, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:18:41', '2020-06-27 07:18:41'),
(1184, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:18:41', '2020-06-27 07:18:41'),
(1185, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:18:43', '2020-06-27 07:18:43'),
(1186, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:18:45', '2020-06-27 07:18:45'),
(1187, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:18:59', '2020-06-27 07:18:59'),
(1188, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:19:01', '2020-06-27 07:19:01'),
(1189, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-27 07:19:04', '2020-06-27 07:19:04'),
(1190, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 07:19:06', '2020-06-27 07:19:06'),
(1191, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-27 07:19:06', '2020-06-27 07:19:06'),
(1192, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 07:19:09', '2020-06-27 07:19:09'),
(1193, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-27 07:19:09', '2020-06-27 07:19:09'),
(1194, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-27 07:19:22', '2020-06-27 07:19:22'),
(1195, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 07:19:23', '2020-06-27 07:19:23'),
(1196, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-27 07:19:23', '2020-06-27 07:19:23'),
(1197, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 07:19:25', '2020-06-27 07:19:25'),
(1198, 1, 'ShowTvAdmin/programs/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 07:19:41', '2020-06-27 07:19:41'),
(1199, 1, 'ShowTvAdmin/programs', 'POST', '127.0.0.1', '{\"program_type_id\":\"1\",\"title\":\"steve harvey Show\",\"description\":\"Broderick Stephen Harvey is an American comedian, businessman and entertainer. He hosts The Steve Harvey Morning Show, Family Feud, Celebrity Family Feud, the Miss Universe competition and Fox\'s New Year\'s Eve. Harvey began his career as a comedian.\",\"broadcast_start_date\":\"2020-06-27\",\"broadcast_start_time\":\"12:00:00\",\"broadcast_end_time\":\"13:00:00\",\"broadCastDaysProgram\":[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",null],\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"bTXRE7wKuJ5QeZ0lu6gGOU77QENIjB9eIgEcKGE9\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/programs\"}', '2020-06-27 07:20:45', '2020-06-27 07:20:45'),
(1200, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '[]', '2020-06-27 07:20:46', '2020-06-27 07:20:46'),
(1201, 1, 'ShowTvAdmin/programs/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 07:21:00', '2020-06-27 07:21:00'),
(1202, 1, 'ShowTvAdmin/programs', 'POST', '127.0.0.1', '{\"program_type_id\":\"2\",\"title\":\"Sham Perfume\",\"description\":\"The work embodies the 1920s in Damascus, when it deals with social life at that point. What happened during the period of the French occupation through interesting stories and tales that combines betrayal, gallantry, love, conflict, good and evil.\",\"broadcast_start_date\":\"2020-06-27\",\"broadcast_start_time\":\"20:00:00\",\"broadcast_end_time\":\"21:00:00\",\"broadCastDaysProgram\":[\"1\",\"2\",\"3\",\"5\",null],\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"bTXRE7wKuJ5QeZ0lu6gGOU77QENIjB9eIgEcKGE9\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/programs\"}', '2020-06-27 07:22:21', '2020-06-27 07:22:21'),
(1203, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '[]', '2020-06-27 07:22:22', '2020-06-27 07:22:22'),
(1204, 1, 'ShowTvAdmin/programs/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 07:22:45', '2020-06-27 07:22:45'),
(1205, 1, 'ShowTvAdmin/programs', 'POST', '127.0.0.1', '{\"program_type_id\":\"2\",\"title\":\"Game Of Thrones\",\"description\":\"Game of Thrones is an American fantasy drama television series created by David Benioff and D. B. Weiss for HBO. It is an adaptation of A Song of Ice and Fire, George R. R. Martin\'s series of fantasy novels, the first of which is A Game of Thrones. The show was both produced and filmed in Belfast and elsewhere in the United Kingdom. Filming locations also included Canada, Croatia, Iceland, Malta, Morocco, and Spain. The series premiered on HBO in the United States on April 17, 2011, and concluded on May 19, 2019, with 73 episodes broadcast over eight seasons.\",\"broadcast_start_date\":\"2020-06-27\",\"broadcast_start_time\":\"20:00:00\",\"broadcast_end_time\":\"23:00:00\",\"broadCastDaysProgram\":[\"5\",null],\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"bTXRE7wKuJ5QeZ0lu6gGOU77QENIjB9eIgEcKGE9\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/programs\"}', '2020-06-27 07:23:37', '2020-06-27 07:23:37'),
(1206, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '[]', '2020-06-27 07:23:37', '2020-06-27 07:23:37'),
(1207, 1, 'ShowTvAdmin/programs/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 07:23:50', '2020-06-27 07:23:50'),
(1208, 1, 'ShowTvAdmin/programs', 'POST', '127.0.0.1', '{\"program_type_id\":\"1\",\"title\":\"News\",\"description\":\"News is information about current events. This may be provided through many different media: word of mouth, printing, postal systems, broadcasting, electronic communication, or through the testimony of observers and witnesses to events.\",\"broadcast_start_date\":\"2020-06-28\",\"broadcast_start_time\":\"21:00:00\",\"broadcast_end_time\":\"21:30:00\",\"broadCastDaysProgram\":[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",null],\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"bTXRE7wKuJ5QeZ0lu6gGOU77QENIjB9eIgEcKGE9\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/programs\"}', '2020-06-27 07:25:10', '2020-06-27 07:25:10'),
(1209, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '[]', '2020-06-27 07:25:10', '2020-06-27 07:25:10'),
(1210, 1, 'ShowTvAdmin/programs/13/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 07:25:19', '2020-06-27 07:25:19'),
(1211, 1, 'ShowTvAdmin/programs/13', 'PUT', '127.0.0.1', '{\"program_type_id\":\"2\",\"title\":\"Game Of Thrones\",\"description\":\"Game of Thrones is an American fantasy drama television series created by David Benioff and D. B. Weiss for HBO. It is an adaptation of A Song of Ice and Fire, George R. R. Martin\'s series of fantasy novels, the first of which is A Game of Thrones. The show was both produced and filmed in Belfast and elsewhere in the United Kingdom. Filming locations also included Canada, Croatia, Iceland, Malta, Morocco, and Spain. The series premiered on HBO in the United States on April 17, 2011, and concluded on May 19, 2019, with 73 episodes broadcast over eight seasons.\",\"broadcast_start_date\":\"2020-06-27\",\"broadcast_start_time\":\"22:00:00\",\"broadcast_end_time\":\"23:00:00\",\"broadCastDaysProgram\":[\"5\",null],\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"bTXRE7wKuJ5QeZ0lu6gGOU77QENIjB9eIgEcKGE9\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/programs\"}', '2020-06-27 07:25:27', '2020-06-27 07:25:27'),
(1212, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '[]', '2020-06-27 07:25:27', '2020-06-27 07:25:27'),
(1213, 1, 'ShowTvAdmin/programs/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 07:26:03', '2020-06-27 07:26:03'),
(1214, 1, 'ShowTvAdmin/programs', 'POST', '127.0.0.1', '{\"program_type_id\":\"2\",\"title\":\"Prison break\",\"description\":\"Prison Break is an American television serial drama created by Paul Scheuring for Fox. The series revolves around two brothers, Lincoln Burrows (Dominic Purcell) and Michael Scofield (Wentworth Miller); Burrows has been sentenced to death for a crime he did not commit, and Scofield devises an elaborate plan to help his brother escape prison and clear his name. The series was produced by Adelstein-Parouse Productions, in association with Original Television and 20th Century Fox Television. Along with creator Paul Scheuring, the series is executive produced by Matt Olmstead, Kevin Hooks, Marty Adelstein, Dawn Parouse, Neal H. Moritz, and Brett Ratner who directed the pilot episode. The series\' theme music, composed by Ramin Djawadi, was nominated for a Primetime Emmy Award in 2006.[1] Prison Break is a joint production between Original Film, Adelstein\\/Parouse Productions, and 20th Century Fox Television and syndicated by 20th Television.\",\"broadcast_start_date\":\"2020-06-27\",\"broadcast_start_time\":\"18:00:00\",\"broadcast_end_time\":\"19:00:00\",\"broadCastDaysProgram\":[\"4\",null],\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"bTXRE7wKuJ5QeZ0lu6gGOU77QENIjB9eIgEcKGE9\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/programs\"}', '2020-06-27 07:26:47', '2020-06-27 07:26:47'),
(1215, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '[]', '2020-06-27 07:26:48', '2020-06-27 07:26:48'),
(1216, 1, 'ShowTvAdmin/programs/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 07:27:04', '2020-06-27 07:27:04'),
(1217, 1, 'ShowTvAdmin/programs', 'POST', '127.0.0.1', '{\"program_type_id\":\"1\",\"title\":\"Donya Ya Donya\",\"description\":\"A varied morning social program broadcasted daily on the Ruya satellite channel from Saturday to Thursday, from 7:00 am to 11:00 am\",\"broadcast_start_date\":\"2020-06-27\",\"broadcast_start_time\":\"07:00:00\",\"broadcast_end_time\":\"11:00:00\",\"broadCastDaysProgram\":[\"1\",\"2\",\"3\",\"4\",\"5\",\"7\",null],\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"bTXRE7wKuJ5QeZ0lu6gGOU77QENIjB9eIgEcKGE9\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/programs\"}', '2020-06-27 07:28:46', '2020-06-27 07:28:46'),
(1218, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '[]', '2020-06-27 07:28:47', '2020-06-27 07:28:47'),
(1219, 1, 'ShowTvAdmin/programs/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 07:28:52', '2020-06-27 07:28:52'),
(1220, 1, 'ShowTvAdmin/programs', 'POST', '127.0.0.1', '{\"program_type_id\":\"1\",\"title\":\"Helwa ya donya\",\"description\":\"A morning program, presented by Fouad Al-Karsha and Mays Al-Noubani, broadcast on the Ruya TV channel. It will be screened on Friday at nine in the morning, as it will host many influential personalities locally, Arab and globally, and includes special reports from Jordan and Palestine.\",\"broadcast_start_date\":\"2020-06-27\",\"broadcast_start_time\":\"09:00:00\",\"broadcast_end_time\":\"12:00:00\",\"broadCastDaysProgram\":[\"6\",null],\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"bTXRE7wKuJ5QeZ0lu6gGOU77QENIjB9eIgEcKGE9\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/programs\"}', '2020-06-27 07:30:18', '2020-06-27 07:30:18'),
(1221, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '[]', '2020-06-27 07:30:19', '2020-06-27 07:30:19'),
(1222, 1, 'ShowTvAdmin/programs/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 07:30:45', '2020-06-27 07:30:45'),
(1223, 1, 'ShowTvAdmin/programs', 'POST', '127.0.0.1', '{\"program_type_id\":\"2\",\"title\":\"Sultana Al Mu\'izz\",\"description\":\"The story revolves around a virtuous person who suffered at the beginning of her life and took responsibility early, and she inherited from her father \\\"Arabiyeh Al-Kabdah\\\" and strives to teach her brother to become a famous lawyer, and improves her economic conditions and helps all the people of the neighborhood until a wealthy man comes claiming to do good and a relationship arises between them Then the truth\'s nightmare begins to unfold ...\",\"broadcast_start_date\":\"2020-06-27\",\"broadcast_start_time\":\"17:00:00\",\"broadcast_end_time\":\"18:00:00\",\"broadCastDaysProgram\":[\"1\",\"2\",\"3\",\"4\",\"5\",null],\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"bTXRE7wKuJ5QeZ0lu6gGOU77QENIjB9eIgEcKGE9\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/programs\"}', '2020-06-27 07:33:10', '2020-06-27 07:33:10'),
(1224, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '[]', '2020-06-27 07:33:11', '2020-06-27 07:33:11'),
(1225, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 07:33:15', '2020-06-27 07:33:15'),
(1226, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '[]', '2020-06-27 07:33:15', '2020-06-27 07:33:15'),
(1227, 1, 'ShowTvAdmin/programs/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 07:36:22', '2020-06-27 07:36:22'),
(1228, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 07:38:11', '2020-06-27 07:38:11'),
(1229, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 07:38:13', '2020-06-27 07:38:13'),
(1230, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/15', 'GET', '127.0.0.1', '[]', '2020-06-27 07:39:13', '2020-06-27 07:39:13'),
(1231, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"15\",\"broadcast_day_id\":\"4\",\"broadcast_date\":\"2020-06-27\",\"episode_no\":\"1\",\"title\":\"prison break\",\"description\":\"prison break\",\"_token\":\"bTXRE7wKuJ5QeZ0lu6gGOU77QENIjB9eIgEcKGE9\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-27 07:41:32', '2020-06-27 07:41:32'),
(1232, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 07:41:32', '2020-06-27 07:41:32'),
(1233, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/12', 'GET', '127.0.0.1', '[]', '2020-06-27 07:41:44', '2020-06-27 07:41:44'),
(1234, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/15', 'GET', '127.0.0.1', '[]', '2020-06-27 07:41:45', '2020-06-27 07:41:45'),
(1235, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"15\",\"broadcast_day_id\":\"4\",\"broadcast_date\":\"2020-07-01\",\"episode_no\":\"1\",\"title\":\"prison break\",\"description\":\"prison break\",\"_token\":\"bTXRE7wKuJ5QeZ0lu6gGOU77QENIjB9eIgEcKGE9\"}', '2020-06-27 07:42:25', '2020-06-27 07:42:25'),
(1236, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 07:42:26', '2020-06-27 07:42:26'),
(1237, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/11', 'GET', '127.0.0.1', '[]', '2020-06-27 07:42:36', '2020-06-27 07:42:36'),
(1238, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/15', 'GET', '127.0.0.1', '[]', '2020-06-27 07:42:38', '2020-06-27 07:42:38'),
(1239, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"15\",\"broadcast_day_id\":\"4\",\"broadcast_date\":\"2020-07-01\",\"episode_no\":\"1\",\"title\":\"prison break\",\"description\":\"prison break\",\"_token\":\"bTXRE7wKuJ5QeZ0lu6gGOU77QENIjB9eIgEcKGE9\"}', '2020-06-27 07:43:02', '2020-06-27 07:43:02'),
(1240, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 07:43:02', '2020-06-27 07:43:02'),
(1241, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 07:43:11', '2020-06-27 07:43:11'),
(1242, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/15', 'GET', '127.0.0.1', '[]', '2020-06-27 07:43:16', '2020-06-27 07:43:16'),
(1243, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"15\",\"broadcast_day_id\":\"4\",\"broadcast_date\":\"2020-07-01\",\"episode_no\":\"1\",\"title\":\"Prison break\",\"description\":\"Prison break\",\"_token\":\"bTXRE7wKuJ5QeZ0lu6gGOU77QENIjB9eIgEcKGE9\"}', '2020-06-27 07:43:34', '2020-06-27 07:43:34'),
(1244, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 07:43:34', '2020-06-27 07:43:34'),
(1245, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 07:43:38', '2020-06-27 07:43:38'),
(1246, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 07:43:41', '2020-06-27 07:43:41'),
(1247, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 07:43:42', '2020-06-27 07:43:42'),
(1248, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 07:43:42', '2020-06-27 07:43:42'),
(1249, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 07:43:43', '2020-06-27 07:43:43'),
(1250, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 07:43:43', '2020-06-27 07:43:43'),
(1251, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 07:43:43', '2020-06-27 07:43:43'),
(1252, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 07:43:43', '2020-06-27 07:43:43'),
(1253, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 07:43:43', '2020-06-27 07:43:43'),
(1254, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 07:43:43', '2020-06-27 07:43:43'),
(1255, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 07:43:44', '2020-06-27 07:43:44'),
(1256, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 07:43:44', '2020-06-27 07:43:44'),
(1257, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 07:43:50', '2020-06-27 07:43:50'),
(1258, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 07:44:17', '2020-06-27 07:44:17'),
(1259, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/15', 'GET', '127.0.0.1', '[]', '2020-06-27 07:44:20', '2020-06-27 07:44:20'),
(1260, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"15\",\"broadcast_day_id\":\"4\",\"broadcast_date\":\"2020-06-27\",\"episode_no\":\"1\",\"title\":\"Prison break\",\"description\":\"Prison break\",\"_token\":\"Z04UI0cJWDF5uGEFCusZgTAspHVb8mcPzsGsPGdm\"}', '2020-06-27 07:44:36', '2020-06-27 07:44:36'),
(1261, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 07:44:36', '2020-06-27 07:44:36'),
(1262, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 07:44:45', '2020-06-27 07:44:45'),
(1263, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 07:44:46', '2020-06-27 07:44:46'),
(1264, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 07:44:46', '2020-06-27 07:44:46'),
(1265, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 07:44:46', '2020-06-27 07:44:46'),
(1266, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 07:44:46', '2020-06-27 07:44:46'),
(1267, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 07:44:46', '2020-06-27 07:44:46'),
(1268, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 07:44:47', '2020-06-27 07:44:47'),
(1269, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 07:44:47', '2020-06-27 07:44:47'),
(1270, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 07:44:47', '2020-06-27 07:44:47'),
(1271, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 07:44:47', '2020-06-27 07:44:47'),
(1272, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 07:44:47', '2020-06-27 07:44:47'),
(1273, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 07:44:47', '2020-06-27 07:44:47'),
(1274, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 07:44:48', '2020-06-27 07:44:48'),
(1275, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 07:44:48', '2020-06-27 07:44:48'),
(1276, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 07:44:48', '2020-06-27 07:44:48'),
(1277, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 07:44:57', '2020-06-27 07:44:57'),
(1278, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/15', 'GET', '127.0.0.1', '[]', '2020-06-27 07:46:38', '2020-06-27 07:46:38'),
(1279, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"15\",\"broadcast_day_id\":\"4\",\"broadcast_date\":\"2020-07-01\",\"episode_no\":\"1\",\"title\":\"Prison break\",\"description\":\"Prison break\",\"_token\":\"bTXRE7wKuJ5QeZ0lu6gGOU77QENIjB9eIgEcKGE9\"}', '2020-06-27 07:46:47', '2020-06-27 07:46:47'),
(1280, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-27 07:46:48', '2020-06-27 07:46:48'),
(1281, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 07:48:14', '2020-06-27 07:48:14'),
(1282, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:49:00', '2020-06-27 07:49:00'),
(1283, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:49:02', '2020-06-27 07:49:02'),
(1284, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:49:18', '2020-06-27 07:49:18'),
(1285, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:50:08', '2020-06-27 07:50:08'),
(1286, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:50:11', '2020-06-27 07:50:11'),
(1287, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:50:18', '2020-06-27 07:50:18'),
(1288, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:50:20', '2020-06-27 07:50:20'),
(1289, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:50:20', '2020-06-27 07:50:20'),
(1290, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:50:20', '2020-06-27 07:50:20'),
(1291, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:50:20', '2020-06-27 07:50:20'),
(1292, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 07:50:36', '2020-06-27 07:50:36'),
(1293, 1, 'ShowTvAdmin/programs/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 07:50:37', '2020-06-27 07:50:37'),
(1294, 1, 'ShowTvAdmin/programs', 'POST', '127.0.0.1', '{\"program_type_id\":\"2\",\"title\":\"Noman Family\",\"description\":\"It is an interesting story that shows the effect of money and the souls of people when money controls their minds and their conscience dies. The events of the first part take place in the late seventies of the twentieth century, where we find the family of Haj Numan, a furniture dealer who has a single son (Khaled) who suffers from schizophrenia. Haj Numan seeks \\\"Salah Abdullah\\\" To make his son lead a normal life and marry him from his cousin to complete the divorce at a later stage, then he marries Radhia, \\\"Sahr Al-Sayegh\\\", who has a great role in stabilizing the psychological state.\\r\\nfamily\\u02c8fam(\\u0259)l\\u0113\\r\\nTranslations of family\\r\\nNounFrequency\\r\\n\\u0623\\u0633\\u0631\\u0629\\r\\nfamily, people, house, stock, belongings, folk\\r\\n\\u0639\\u0627\\u0626\\u0644\\u0629\\r\\nfamily, house\\r\\n\\u0641\\u0635\\u064a\\u0644\\u0629\\r\\nfamily\\r\\n\\u0639\\u0634\\u064a\\u0631\\u0629\\r\\nclan, tribe, family, kindred, kin, nation\\r\\n\\u0646\\u0633\\u0628 \\u0643\\u0631\\u064a\\u0645\\r\\nfamily\\r\\nAdjective\\r\\n\\u0639\\u0627\\u0626\\u0644\\u064a\\r\\nfamily, homely, familial, homelike\\r\\nDefinitions of family\\r\\nNoun\\r\\n1\\r\\na group consisting of parents and children living together in a household.\\r\\nSynonyms:\\r\\nhouseholdm\\u00e9nagenuclear familybrood\\r\\n2\\r\\nall the descendants of a common ancestor.\\r\\nthe house has been owned by the same family for 300 years\\r\\nSynonyms:\\r\\nfiliationstirpsancestryparentagebirthpedigreegenealogybackgroundfamily treedescentlineagelineline of descentbloodlinebloodextractionderivationracestrainstockbreeddynastyhouseforebearsforefathersantecedentsprogenitorsrootsorigins\\r\\nAdjective\\r\\n1\\r\\ndesigned to be suitable for children as well as adults.\\r\\na family newspaper\\r\\nExamples of family\\r\\nthe Sullivan family\\r\\n15 more examples\\r\\nSynonyms of family\\r\\nNoun\\r\\nfiliationhouseholdrelativesancestrypeoplechildrentaxonomic grouptaxonbroodfamkids\\r\\n69 more synonyms\",\"broadcast_start_date\":\"2020-06-27\",\"broadcast_start_time\":\"14:00:00\",\"broadcast_end_time\":\"15:00:00\",\"broadCastDaysProgram\":[\"1\",\"2\",\"3\",\"4\",\"5\",null],\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"AhzWiqPTdHxva1P1vzT5KSjAKI6OzOjFY5YsWz4L\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/programs\"}', '2020-06-27 07:51:51', '2020-06-27 07:51:51'),
(1295, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '[]', '2020-06-27 07:51:52', '2020-06-27 07:51:52'),
(1296, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '[]', '2020-06-27 07:53:19', '2020-06-27 07:53:19'),
(1297, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 07:53:20', '2020-06-27 07:53:20'),
(1298, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:54:00', '2020-06-27 07:54:00'),
(1299, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:54:02', '2020-06-27 07:54:02'),
(1300, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:54:07', '2020-06-27 07:54:07'),
(1301, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:54:07', '2020-06-27 07:54:07'),
(1302, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:54:08', '2020-06-27 07:54:08'),
(1303, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:54:08', '2020-06-27 07:54:08'),
(1304, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:54:08', '2020-06-27 07:54:08'),
(1305, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:54:08', '2020-06-27 07:54:08'),
(1306, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:54:08', '2020-06-27 07:54:08'),
(1307, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:55:34', '2020-06-27 07:55:34'),
(1308, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:55:48', '2020-06-27 07:55:48'),
(1309, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:55:50', '2020-06-27 07:55:50'),
(1310, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:55:52', '2020-06-27 07:55:52'),
(1311, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 07:56:26', '2020-06-27 07:56:26'),
(1312, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 07:56:27', '2020-06-27 07:56:27'),
(1313, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 07:56:29', '2020-06-27 07:56:29'),
(1314, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/10', 'GET', '127.0.0.1', '[]', '2020-06-27 07:56:31', '2020-06-27 07:56:31'),
(1315, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 07:57:12', '2020-06-27 07:57:12'),
(1316, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 07:57:22', '2020-06-27 07:57:22'),
(1317, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 07:58:00', '2020-06-27 07:58:00'),
(1318, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 07:58:32', '2020-06-27 07:58:32'),
(1319, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 07:58:33', '2020-06-27 07:58:33'),
(1320, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/10', 'GET', '127.0.0.1', '[]', '2020-06-27 07:58:35', '2020-06-27 07:58:35'),
(1321, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"10\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-28\",\"episode_no\":\"1\",\"title\":\"First Episode\",\"description\":\"\\u062c\\u0645\\u0639\\u0629 \\u0634\\u0628\\u0627\\u0628\\u064a\\u0629\\u060c \\u0631\\u0627\\u062d \\u064a\\u0627\\u062e\\u062f\\u0643\\u0645 \\u0641\\u064a\\u0647\\u0627 \\u0643\\u0631\\u0641\\u0627\\u0646 \\u0641\\u064a \\u0644\\u0641\\u0629 \\u0633\\u0631\\u064a\\u0639\\u0629 \\u0628\\u062a\\u062a\\u0648\\u0642\\u0641 \\u0639\\u0644\\u0649 \\u0643\\u0644 \\\"\\u0645\\u0627 \\u064a\\u0647\\u0645\\u0646\\u0627\\\" \\u0625\\u062d\\u0646\\u0627 \\u0627\\u0644\\u0634\\u0628\\u0627\\u0628 \\u0645\\u0646 \\u0623\\u062e\\u0628\\u0627\\u0631 \\u0645\\u0648\\u0627\\u0642\\u0639 \\u0627\\u0644\\u062a\\u0648\\u0627\\u0635\\u0644 \\u0627\\u0644\\u0625\\u062c\\u062a\\u0645\\u0627\\u0639\\u064a\\u060c \\u0644\\u0644\\u0645\\u0635\\u0627\\u0631\\u064a\\u0641 \\u0648\\u0647\\u0645\\u0647\\u0627\\u060c \\u0644\\u0644\\u0623\\u0643\\u0644 \\u0648\\u0635\\u062d\\u0627\\u0628 \\u0627\\u0644\\u0645\\u0632\\u0627\\u062c ...\",\"_token\":\"AhzWiqPTdHxva1P1vzT5KSjAKI6OzOjFY5YsWz4L\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-27 08:00:29', '2020-06-27 08:00:29'),
(1322, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-27 08:00:30', '2020-06-27 08:00:30'),
(1323, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 08:01:01', '2020-06-27 08:01:01'),
(1324, 1, 'ShowTvAdmin/programs/10/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 08:01:04', '2020-06-27 08:01:04'),
(1325, 1, 'ShowTvAdmin/programs/10', 'PUT', '127.0.0.1', '{\"program_type_id\":\"1\",\"title\":\"Caravan - \\u0643\\u0631\\u0641\\u0627\\u0646\",\"description\":\"A paragraph that focuses on fashion and fashion and ways to coordinate clothes\",\"broadcast_start_date\":\"2020-06-27\",\"broadcast_start_time\":\"10:00:00\",\"broadcast_end_time\":\"11:00:00\",\"broadCastDaysProgram\":[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",null],\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"AhzWiqPTdHxva1P1vzT5KSjAKI6OzOjFY5YsWz4L\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/programs\"}', '2020-06-27 08:02:07', '2020-06-27 08:02:07'),
(1326, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '[]', '2020-06-27 08:02:07', '2020-06-27 08:02:07'),
(1327, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 08:02:08', '2020-06-27 08:02:08'),
(1328, 1, 'ShowTvAdmin/program-episodes/10/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 08:02:11', '2020-06-27 08:02:11'),
(1329, 1, 'ShowTvAdmin/program-episodes/10', 'PUT', '127.0.0.1', '{\"program_id\":\"10\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-06-28\",\"episode_no\":\"1\",\"title\":\"First Episode\",\"description\":\"\\u062c\\u0645\\u0639\\u0629 \\u0634\\u0628\\u0627\\u0628\\u064a\\u0629\\u060c \\u0631\\u0627\\u062d \\u064a\\u0627\\u062e\\u062f\\u0643\\u0645 \\u0641\\u064a\\u0647\\u0627 \\u0643\\u0631\\u0641\\u0627\\u0646 \\u0641\\u064a \\u0644\\u0641\\u0629 \\u0633\\u0631\\u064a\\u0639\\u0629 \\u0628\\u062a\\u062a\\u0648\\u0642\\u0641 \\u0639\\u0644\\u0649 \\u0643\\u0644 \\\"\\u0645\\u0627 \\u064a\\u0647\\u0645\\u0646\\u0627\\\" \\u0625\\u062d\\u0646\\u0627 \\u0627\\u0644\\u0634\\u0628\\u0627\\u0628 \\u0645\\u0646 \\u0623\\u062e\\u0628\\u0627\\u0631 \\u0645\\u0648\\u0627\\u0642\\u0639 \\u0627\\u0644\\u062a\\u0648\\u0627\\u0635\\u0644 \\u0627\\u0644\\u0625\\u062c\\u062a\\u0645\\u0627\\u0639\\u064a\\u060c \\u0644\\u0644\\u0645\\u0635\\u0627\\u0631\\u064a\\u0641 \\u0648\\u0647\\u0645\\u0647\\u0627\\u060c \\u0644\\u0644\\u0623\\u0643\\u0644 \\u0648\\u0635\\u062d\\u0627\\u0628 \\u0627\\u0644\\u0645\\u0632\\u0627\\u062c ...\",\"_token\":\"AhzWiqPTdHxva1P1vzT5KSjAKI6OzOjFY5YsWz4L\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-27 08:02:18', '2020-06-27 08:02:18'),
(1330, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-27 08:02:18', '2020-06-27 08:02:18'),
(1331, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 08:10:15', '2020-06-27 08:10:15'),
(1332, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 08:14:02', '2020-06-27 08:14:02'),
(1333, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 08:14:51', '2020-06-27 08:14:51'),
(1334, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 08:14:52', '2020-06-27 08:14:52'),
(1335, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/11', 'GET', '127.0.0.1', '[]', '2020-06-27 08:14:55', '2020-06-27 08:14:55'),
(1336, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"11\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-07-05\",\"episode_no\":\"1\",\"title\":\"How Steve Harvey\'s Grandkids Convinced Him to Buy an $8,500 Teepee\",\"description\":\"Ellen\'s friend Steve Harvey told a hilarious story of how he ended up with an $8,500, 16-foot teepee in his backyard.\",\"_token\":\"AhzWiqPTdHxva1P1vzT5KSjAKI6OzOjFY5YsWz4L\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-27 08:17:20', '2020-06-27 08:17:20'),
(1337, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-27 08:17:21', '2020-06-27 08:17:21'),
(1338, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 08:18:25', '2020-06-27 08:18:25'),
(1339, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/12', 'GET', '127.0.0.1', '[]', '2020-06-27 08:18:28', '2020-06-27 08:18:28'),
(1340, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"12\",\"broadcast_day_id\":\"3\",\"broadcast_date\":\"2020-07-07\",\"episode_no\":\"1\",\"title\":\"\\u0627\\u0644\\u0642\\u0635\\u0629 \\u0643\\u0627\\u0645\\u0644\\u0629\",\"description\":\"\\u0627\\u0644\\u0645\\u0634\\u0647\\u062f \\u0627\\u0644\\u0627\\u0633\\u0637\\u0648\\u0631\\u064a \\u0645\\u0646 \\u0639\\u0637\\u0631 \\u0627\\u0644\\u0634\\u0627\\u0645 \\u0627\\u0628\\u0648 \\u0639\\u0627\\u0645\\u0631 \\u0639\\u0631\\u0641 \\u062d\\u0642\\u064a\\u0642\\u0629 \\u0645\\u0631\\u062a\\u0648 \\u0627\\u0644\\u062e\\u0627\\u064a\\u0646\\u0629 \\u0645\\u0639 \\u0634\\u063a\\u064a\\u0644\\u0648 \\u0648\\u0627\\u0644\\u0632\\u0631\\u0639 \\u0645\\u0648 \\u0632\\u0631\\u0639\\u0648!! \\u0627\\u0644\\u0642\\u0635\\u0629 \\u0643\\u0627\\u0645\\u0644\\u0629\",\"_token\":\"AhzWiqPTdHxva1P1vzT5KSjAKI6OzOjFY5YsWz4L\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-27 08:20:18', '2020-06-27 08:20:18'),
(1341, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-27 08:20:19', '2020-06-27 08:20:19'),
(1342, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 08:20:52', '2020-06-27 08:20:52'),
(1343, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/13', 'GET', '127.0.0.1', '[]', '2020-06-27 08:20:54', '2020-06-27 08:20:54'),
(1344, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"13\",\"broadcast_day_id\":\"5\",\"broadcast_date\":\"2020-06-30\",\"episode_no\":\"1\",\"title\":\"Game of Thrones - Main Theme (Extended) HD\",\"description\":\"Game of Thrones - Main Theme (Extended) HD\",\"_token\":\"AhzWiqPTdHxva1P1vzT5KSjAKI6OzOjFY5YsWz4L\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-27 08:26:10', '2020-06-27 08:26:10'),
(1345, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 08:26:10', '2020-06-27 08:26:10'),
(1346, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/10', 'GET', '127.0.0.1', '[]', '2020-06-27 08:26:13', '2020-06-27 08:26:13'),
(1347, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/13', 'GET', '127.0.0.1', '[]', '2020-06-27 08:26:14', '2020-06-27 08:26:14'),
(1348, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"13\",\"broadcast_day_id\":\"5\",\"broadcast_date\":\"2020-07-02\",\"episode_no\":\"1\",\"title\":\"Game of Thrones - Main Theme (Extended) HD\",\"description\":\"Game of Thrones - Main Theme (Extended) HD\",\"_token\":\"AhzWiqPTdHxva1P1vzT5KSjAKI6OzOjFY5YsWz4L\"}', '2020-06-27 08:26:51', '2020-06-27 08:26:51'),
(1349, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-27 08:26:52', '2020-06-27 08:26:52'),
(1350, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 08:27:20', '2020-06-27 08:27:20'),
(1351, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/14', 'GET', '127.0.0.1', '[]', '2020-06-27 08:27:22', '2020-06-27 08:27:22'),
(1352, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"14\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-07-05\",\"episode_no\":\"1\",\"title\":\"\\u0627\\u0644\\u0642\\u0631\\u062f\\u0629 \\u062a\\u063a\\u0632\\u0648 \\u0645\\u062f\\u064a\\u0646\\u0629 \\u0644\\u064a\\u0628\\u0648\\u0631\\u064a \\u0648\\u0627\\u0644\\u0633\\u0644\\u0637\\u0627\\u062a \\u062a\\u062d\\u0627\\u0648\\u0644 \\u0627\\u0644\\u0633\\u064a\\u0637\\u0631\\u0629 \\u0639\\u0644\\u064a\\u0647\\u0627 | 26-06-2020\",\"description\":\"\\u0627\\u0644\\u0642\\u0631\\u062f\\u0629 \\u062a\\u063a\\u0632\\u0648 \\u0645\\u062f\\u064a\\u0646\\u0629 \\u0644\\u064a\\u0628\\u0648\\u0631\\u064a \\u0648\\u0627\\u0644\\u0633\\u0644\\u0637\\u0627\\u062a \\u062a\\u062d\\u0627\\u0648\\u0644 \\u0627\\u0644\\u0633\\u064a\\u0637\\u0631\\u0629 \\u0639\\u0644\\u064a\\u0647\\u0627 | 26-06-2020\",\"_token\":\"AhzWiqPTdHxva1P1vzT5KSjAKI6OzOjFY5YsWz4L\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-27 08:28:29', '2020-06-27 08:28:29'),
(1353, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-27 08:28:29', '2020-06-27 08:28:29'),
(1354, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 08:28:54', '2020-06-27 08:28:54'),
(1355, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/16', 'GET', '127.0.0.1', '[]', '2020-06-27 08:29:00', '2020-06-27 08:29:00'),
(1356, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"16\",\"broadcast_day_id\":\"5\",\"broadcast_date\":\"2020-07-09\",\"episode_no\":\"1\",\"title\":\"\\u0627\\u0644\\u0646\\u0634\\u0631\\u0629 \\u0627\\u0644\\u062c\\u0648\\u064a\\u0629 27\\/6\\/2020\",\"description\":\"\\u0627\\u0644\\u0646\\u0634\\u0631\\u0629 \\u0627\\u0644\\u062c\\u0648\\u064a\\u0629 27\\/6\\/2020\",\"_token\":\"AhzWiqPTdHxva1P1vzT5KSjAKI6OzOjFY5YsWz4L\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-27 08:30:07', '2020-06-27 08:30:07'),
(1357, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-27 08:30:08', '2020-06-27 08:30:08'),
(1358, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 08:30:11', '2020-06-27 08:30:11'),
(1359, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/17', 'GET', '127.0.0.1', '[]', '2020-06-27 08:30:18', '2020-06-27 08:30:18'),
(1360, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"17\",\"broadcast_day_id\":\"6\",\"broadcast_date\":\"2020-07-10\",\"episode_no\":\"1\",\"title\":\"\\u0634\\u0627\\u0634\\u0629 The Frame - \\u0641\\u0627\\u062f\\u064a \\u0623\\u0628\\u0648 \\u0647\\u0648\\u0644\\u0629 | \\u062d\\u0644\\u0648\\u0629 \\u064a\\u0627 \\u062f\\u0646\\u064a\\u0627\",\"description\":\"\\u0634\\u0627\\u0634\\u0629 The Frame - \\u0641\\u0627\\u062f\\u064a \\u0623\\u0628\\u0648 \\u0647\\u0648\\u0644\\u0629 | \\u062d\\u0644\\u0648\\u0629 \\u064a\\u0627 \\u062f\\u0646\\u064a\\u0627\",\"_token\":\"AhzWiqPTdHxva1P1vzT5KSjAKI6OzOjFY5YsWz4L\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-27 08:31:48', '2020-06-27 08:31:48'),
(1361, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-27 08:31:48', '2020-06-27 08:31:48'),
(1362, 1, 'ShowTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 08:31:54', '2020-06-27 08:31:54'),
(1363, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 08:31:59', '2020-06-27 08:31:59'),
(1364, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/18', 'GET', '127.0.0.1', '[]', '2020-06-27 08:32:02', '2020-06-27 08:32:02'),
(1365, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"18\",\"broadcast_day_id\":\"3\",\"broadcast_date\":\"2020-06-30\",\"episode_no\":\"1\",\"title\":\"Episode 01 - Sultanat Al Moez Series | \\u0645\\u0644\\u062e\\u0635 \\u0627\\u0644\\u062d\\u0644\\u0642\\u0629 \\u0627\\u0644\\u0623\\u0648\\u0644\\u064a - \\u0645\\u0633\\u0644\\u0633\\u0644 \\u0633\\u0644\\u0637\\u0627\\u0646\\u0629 \\u0627\\u0644\\u0645\\u0639\\u0632\",\"description\":\"Episode 01 - Sultanat Al Moez Series | \\u0645\\u0644\\u062e\\u0635 \\u0627\\u0644\\u062d\\u0644\\u0642\\u0629 \\u0627\\u0644\\u0623\\u0648\\u0644\\u064a - \\u0645\\u0633\\u0644\\u0633\\u0644 \\u0633\\u0644\\u0637\\u0627\\u0646\\u0629 \\u0627\\u0644\\u0645\\u0639\\u0632\",\"_token\":\"AhzWiqPTdHxva1P1vzT5KSjAKI6OzOjFY5YsWz4L\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-27 08:33:18', '2020-06-27 08:33:18'),
(1366, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-27 08:33:18', '2020-06-27 08:33:18'),
(1367, 1, 'ShowTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 08:33:22', '2020-06-27 08:33:22'),
(1368, 1, 'ShowTvAdmin/programs/getProgramBroadcastTimes/19', 'GET', '127.0.0.1', '[]', '2020-06-27 08:33:25', '2020-06-27 08:33:25'),
(1369, 1, 'ShowTvAdmin/program-episodes', 'POST', '127.0.0.1', '{\"program_id\":\"19\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-07-05\",\"episode_no\":\"1\",\"title\":\"\\u0645\\u0648\\u0627\\u062c\\u0647\\u0629 \\u0642\\u0648\\u064a\\u0629 \\u0628\\u064a\\u0646 \\u0647\\u0646\\u0627 \\u0648\\u0648\\u0627\\u0644\\u062f\\u0647\\u0627 \\u0627\\u0644\\u0634\\u064a\\u062e \\u0645\\u062e\\u0644\\u0648\\u0641\",\"description\":\"\\u0645\\u0648\\u0627\\u062c\\u0647\\u0629 \\u0642\\u0648\\u064a\\u0629 \\u0628\\u064a\\u0646 \\u0647\\u0646\\u0627 \\u0648\\u0648\\u0627\\u0644\\u062f\\u0647\\u0627 \\u0627\\u0644\\u0634\\u064a\\u062e \\u0645\\u062e\\u0644\\u0648\\u0641\",\"_token\":\"AhzWiqPTdHxva1P1vzT5KSjAKI6OzOjFY5YsWz4L\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-27 08:34:16', '2020-06-27 08:34:16'),
(1370, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-27 08:34:16', '2020-06-27 08:34:16'),
(1371, 1, 'ShowTvAdmin/program-episodes/14/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 08:34:28', '2020-06-27 08:34:28'),
(1372, 1, 'ShowTvAdmin/program-episodes/14', 'PUT', '127.0.0.1', '{\"program_id\":\"14\",\"broadcast_day_id\":\"1\",\"broadcast_date\":\"2020-07-05\",\"episode_no\":\"1\",\"title\":\"\\u0627\\u0644\\u0642\\u0631\\u062f\\u0629 \\u062a\\u063a\\u0632\\u0648 \\u0645\\u062f\\u064a\\u0646\\u0629 \\u0644\\u064a\\u0628\\u0648\\u0631\\u064a \\u0648\\u0627\\u0644\\u0633\\u0644\\u0637\\u0627\\u062a \\u062a\\u062d\\u0627\\u0648\\u0644 \\u0627\\u0644\\u0633\\u064a\\u0637\\u0631\\u0629 \\u0639\\u0644\\u064a\\u0647\\u0627 | 26-06-2020\",\"description\":\"\\u0627\\u0644\\u0642\\u0631\\u062f\\u0629 \\u062a\\u063a\\u0632\\u0648 \\u0645\\u062f\\u064a\\u0646\\u0629 \\u0644\\u064a\\u0628\\u0648\\u0631\\u064a \\u0648\\u0627\\u0644\\u0633\\u0644\\u0637\\u0627\\u062a \\u062a\\u062d\\u0627\\u0648\\u0644 \\u0627\\u0644\\u0633\\u064a\\u0637\\u0631\\u0629 \\u0639\\u0644\\u064a\\u0647\\u0627 | 26-06-2020\",\"_token\":\"AhzWiqPTdHxva1P1vzT5KSjAKI6OzOjFY5YsWz4L\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-27 08:35:02', '2020-06-27 08:35:02'),
(1373, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-27 08:35:02', '2020-06-27 08:35:02'),
(1374, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 08:35:04', '2020-06-27 08:35:04'),
(1375, 1, 'ShowTvAdmin/programs/14/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 08:35:12', '2020-06-27 08:35:12'),
(1376, 1, 'ShowTvAdmin/programs/14', 'PUT', '127.0.0.1', '{\"program_type_id\":\"1\",\"title\":\"News\",\"description\":\"News is information about current events. This may be provided through many different media: word of mouth, printing, postal systems, broadcasting, electronic communication, or through the testimony of observers and witnesses to events.\",\"broadcast_start_date\":\"2020-06-28\",\"broadcast_start_time\":\"21:00:00\",\"broadcast_end_time\":\"21:30:00\",\"broadCastDaysProgram\":[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",null],\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"AhzWiqPTdHxva1P1vzT5KSjAKI6OzOjFY5YsWz4L\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/programs\"}', '2020-06-27 08:35:20', '2020-06-27 08:35:20'),
(1377, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '[]', '2020-06-27 08:35:20', '2020-06-27 08:35:20');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(1378, 1, 'ShowTvAdmin/programs/17/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 08:35:34', '2020-06-27 08:35:34'),
(1379, 1, 'ShowTvAdmin/programs/17', 'PUT', '127.0.0.1', '{\"program_type_id\":\"1\",\"title\":\"Helwa ya donya\",\"description\":\"A morning program, presented by Fouad Al-Karsha and Mays Al-Noubani, broadcast on the Ruya TV channel. It will be screened on Friday at nine in the morning, as it will host many influential personalities locally, Arab and globally, and includes special reports from Jordan and Palestine.\",\"broadcast_start_date\":\"2020-06-27\",\"broadcast_start_time\":\"09:00:00\",\"broadcast_end_time\":\"12:00:00\",\"broadCastDaysProgram\":[\"6\",null],\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"AhzWiqPTdHxva1P1vzT5KSjAKI6OzOjFY5YsWz4L\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/programs\"}', '2020-06-27 08:35:41', '2020-06-27 08:35:41'),
(1380, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '[]', '2020-06-27 08:35:41', '2020-06-27 08:35:41'),
(1381, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 08:35:43', '2020-06-27 08:35:43'),
(1382, 1, 'ShowTvAdmin/program-episodes/15/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 08:35:48', '2020-06-27 08:35:48'),
(1383, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 08:35:55', '2020-06-27 08:35:55'),
(1384, 1, 'ShowTvAdmin/program-episodes/16/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 08:36:06', '2020-06-27 08:36:06'),
(1385, 1, 'ShowTvAdmin/program-episodes/16', 'PUT', '127.0.0.1', '{\"program_id\":\"17\",\"broadcast_day_id\":\"6\",\"broadcast_date\":\"2020-07-10\",\"episode_no\":\"1\",\"title\":\"\\u0634\\u0627\\u0634\\u0629 The Frame - \\u0641\\u0627\\u062f\\u064a \\u0623\\u0628\\u0648 \\u0647\\u0648\\u0644\\u0629 | \\u062d\\u0644\\u0648\\u0629 \\u064a\\u0627 \\u062f\\u0646\\u064a\\u0627\",\"description\":\"\\u0634\\u0627\\u0634\\u0629 The Frame - \\u0641\\u0627\\u062f\\u064a \\u0623\\u0628\\u0648 \\u0647\\u0648\\u0644\\u0629 | \\u062d\\u0644\\u0648\\u0629 \\u064a\\u0627 \\u062f\\u0646\\u064a\\u0627\",\"_token\":\"AhzWiqPTdHxva1P1vzT5KSjAKI6OzOjFY5YsWz4L\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/program-episodes\"}', '2020-06-27 08:36:12', '2020-06-27 08:36:12'),
(1386, 1, 'ShowTvAdmin/program-episodes', 'GET', '127.0.0.1', '[]', '2020-06-27 08:36:12', '2020-06-27 08:36:12'),
(1387, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 08:37:30', '2020-06-27 08:37:30'),
(1388, 1, 'ShowTvAdmin/programs/19/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 08:37:35', '2020-06-27 08:37:35'),
(1389, 1, 'ShowTvAdmin/programs/19', 'PUT', '127.0.0.1', '{\"program_type_id\":\"2\",\"title\":\"Noman Family\",\"description\":\"It is an interesting story that shows the effect of money and the souls of people when money controls their minds and their conscience dies. The events of the first part take place in the late seventies of the twentieth century, where we find the family of Haj Numan, a furniture dealer who has a single son (Khaled) who suffers from schizophrenia. Haj Numan seeks \\\"Salah Abdullah\\\" To make his son lead a normal life and marry him from his cousin to complete the divorce at a later stage, then he marries Radhia, \\\"Sahr Al-Sayegh\\\", who has a great role in stabilizing the psychological state.\",\"broadcast_start_date\":\"2020-06-27\",\"broadcast_start_time\":\"14:00:00\",\"broadcast_end_time\":\"15:00:00\",\"broadCastDaysProgram\":[\"1\",\"2\",\"3\",\"4\",\"5\",null],\"on_air\":\"on\",\"active\":\"on\",\"_token\":\"AhzWiqPTdHxva1P1vzT5KSjAKI6OzOjFY5YsWz4L\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/ShowTvAdmin\\/programs\"}', '2020-06-27 08:37:50', '2020-06-27 08:37:50'),
(1390, 1, 'ShowTvAdmin/programs', 'GET', '127.0.0.1', '[]', '2020-06-27 08:37:50', '2020-06-27 08:37:50'),
(1391, 1, 'showTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 08:42:14', '2020-06-27 08:42:14'),
(1392, 1, 'showTvAdmin/programs', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 08:42:21', '2020-06-27 08:42:21'),
(1393, 1, 'showTvAdmin/program-episodes', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 08:42:23', '2020-06-27 08:42:23'),
(1394, 1, 'showTvAdmin/programs', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 08:42:24', '2020-06-27 08:42:24'),
(1395, 1, 'showTvAdmin/program-episodes', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 08:42:27', '2020-06-27 08:42:27'),
(1396, 1, 'showTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 08:42:28', '2020-06-27 08:42:28'),
(1397, 1, 'showTvAdmin/programs/getProgramBroadcastTimes/11', 'GET', '127.0.0.1', '[]', '2020-06-27 08:42:30', '2020-06-27 08:42:30'),
(1398, 1, 'showTvAdmin/program-episodes/create', 'GET', '127.0.0.1', '[]', '2020-06-27 08:42:32', '2020-06-27 08:42:32'),
(1399, 1, 'showTvAdmin', 'GET', '127.0.0.1', '[]', '2020-06-27 08:42:36', '2020-06-27 08:42:36'),
(1400, 1, 'showTvAdmin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 08:44:01', '2020-06-27 08:44:01'),
(1401, 1, 'showTvAdmin/users', 'GET', '127.0.0.1', '[]', '2020-06-27 08:51:11', '2020-06-27 08:51:11'),
(1402, 1, 'showTvAdmin/auth/roles', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 08:51:15', '2020-06-27 08:51:15'),
(1403, 1, 'showTvAdmin/auth/roles/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 08:51:17', '2020-06-27 08:51:17'),
(1404, 1, 'showTvAdmin/auth/roles', 'POST', '127.0.0.1', '{\"slug\":\"episode permissions\",\"name\":\"episode permissions\",\"permissions\":[\"8\",null],\"_token\":\"AhzWiqPTdHxva1P1vzT5KSjAKI6OzOjFY5YsWz4L\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/showTvAdmin\\/auth\\/roles\"}', '2020-06-27 08:51:37', '2020-06-27 08:51:37'),
(1405, 1, 'showTvAdmin/auth/roles', 'GET', '127.0.0.1', '[]', '2020-06-27 08:51:38', '2020-06-27 08:51:38'),
(1406, 1, 'showTvAdmin/auth/roles/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 08:51:40', '2020-06-27 08:51:40'),
(1407, 1, 'showTvAdmin/auth/roles', 'POST', '127.0.0.1', '{\"slug\":\"view users\",\"name\":\"view users\",\"permissions\":[\"7\",null],\"_token\":\"AhzWiqPTdHxva1P1vzT5KSjAKI6OzOjFY5YsWz4L\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/showTvAdmin\\/auth\\/roles\"}', '2020-06-27 08:51:51', '2020-06-27 08:51:51'),
(1408, 1, 'showTvAdmin/auth/roles', 'GET', '127.0.0.1', '[]', '2020-06-27 08:51:52', '2020-06-27 08:51:52'),
(1409, 1, 'showTvAdmin/auth/roles/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 08:51:53', '2020-06-27 08:51:53'),
(1410, 1, 'showTvAdmin/auth/roles', 'POST', '127.0.0.1', '{\"slug\":\"programs permissions\",\"name\":\"programs permissions\",\"permissions\":[\"6\",null],\"_token\":\"AhzWiqPTdHxva1P1vzT5KSjAKI6OzOjFY5YsWz4L\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/showTvAdmin\\/auth\\/roles\"}', '2020-06-27 08:52:05', '2020-06-27 08:52:05'),
(1411, 1, 'showTvAdmin/auth/roles', 'GET', '127.0.0.1', '[]', '2020-06-27 08:52:05', '2020-06-27 08:52:05'),
(1412, 1, 'showTvAdmin/programs', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-06-27 08:52:08', '2020-06-27 08:52:08');

-- --------------------------------------------------------

--
-- Table structure for table `admin_permissions`
--

CREATE TABLE `admin_permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `http_method` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `http_path` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_permissions`
--

INSERT INTO `admin_permissions` (`id`, `name`, `slug`, `http_method`, `http_path`, `created_at`, `updated_at`) VALUES
(1, 'All permission', '*', '', '*', NULL, NULL),
(2, 'Dashboard', 'dashboard', 'GET', '/', NULL, NULL),
(3, 'Login', 'auth.login', '', '/auth/login\r\n/auth/logout', NULL, NULL),
(4, 'User setting', 'auth.setting', 'GET,PUT', '/auth/setting', NULL, NULL),
(5, 'Auth management', 'auth.management', '', '/auth/roles\r\n/auth/permissions\r\n/auth/menu\r\n/auth/logs', NULL, NULL),
(6, 'Crud on programs', 'CRUD on programs', '', '/programs*', '2020-06-26 19:18:40', '2020-06-26 19:19:00'),
(7, 'can view user', 'can view users', 'GET', '/users*', '2020-06-26 19:19:38', '2020-06-26 19:19:38'),
(8, 'CRUD on Episodes', 'CRUD on Episodes', '', '/program-episodes*', '2020-06-26 19:20:08', '2020-06-26 19:20:08');

-- --------------------------------------------------------

--
-- Table structure for table `admin_roles`
--

CREATE TABLE `admin_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_roles`
--

INSERT INTO `admin_roles` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'administrator', '2020-06-22 18:24:54', '2020-06-22 18:24:54'),
(2, 'episode permissions', 'episode permissions', '2020-06-27 08:51:38', '2020-06-27 08:51:38'),
(3, 'view users', 'view users', '2020-06-27 08:51:51', '2020-06-27 08:51:51'),
(4, 'programs permissions', 'programs permissions', '2020-06-27 08:52:05', '2020-06-27 08:52:05');

-- --------------------------------------------------------

--
-- Table structure for table `admin_role_menu`
--

CREATE TABLE `admin_role_menu` (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_role_menu`
--

INSERT INTO `admin_role_menu` (`role_id`, `menu_id`, `created_at`, `updated_at`) VALUES
(1, 2, NULL, NULL),
(1, 8, NULL, NULL),
(1, 9, NULL, NULL),
(1, 10, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_role_permissions`
--

CREATE TABLE `admin_role_permissions` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_role_permissions`
--

INSERT INTO `admin_role_permissions` (`role_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(2, 8, NULL, NULL),
(3, 7, NULL, NULL),
(4, 6, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_role_users`
--

CREATE TABLE `admin_role_users` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_role_users`
--

INSERT INTO `admin_role_users` (`role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_users`
--

CREATE TABLE `admin_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_users`
--

INSERT INTO `admin_users` (`id`, `username`, `password`, `name`, `avatar`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', '$2y$10$uOGsYEjmTJ1XcnR4xMecP.eq02Jhq8zo80sIpiV43wBn92yBv8hbe', 'Administrator', NULL, '0UYElV5JW7lYJs0DHFHxoeRJrXErh491N1AuL6vElnoP7Qdi9C8oQkxqsfeG', '2020-06-22 18:24:54', '2020-06-22 18:24:54');

-- --------------------------------------------------------

--
-- Table structure for table `admin_user_permissions`
--

CREATE TABLE `admin_user_permissions` (
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `broadcast_days`
--

CREATE TABLE `broadcast_days` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `broadcast_day` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `broadcast_days`
--

INSERT INTO `broadcast_days` (`id`, `broadcast_day`, `created_at`, `updated_at`) VALUES
(1, 'Sunday', '2020-06-22 18:36:16', '2020-06-22 18:36:16'),
(2, 'Monday', '2020-06-22 18:36:16', '2020-06-22 18:36:16'),
(3, 'Tuesday', '2020-06-22 18:36:16', '2020-06-22 18:36:16'),
(4, 'Wednesday', '2020-06-22 18:36:17', '2020-06-22 18:36:17'),
(5, 'Thursday', '2020-06-22 18:36:17', '2020-06-22 18:36:17'),
(6, 'Friday', '2020-06-22 18:36:17', '2020-06-22 18:36:17'),
(7, 'Saturday', '2020-06-22 18:36:17', '2020-06-22 18:36:17');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(5, '2020_06_22_192005_create_broad_cast_days_table', 2),
(10, '2014_10_12_000000_create_users_table', 3),
(11, '2014_10_12_100000_create_password_resets_table', 3),
(12, '2016_01_04_173148_create_admin_tables', 3),
(13, '2020_06_22_191921_create_programs_table', 3),
(14, '2020_06_22_192024_create_program_episodes_table', 3),
(15, '2020_06_22_192048_create_user_fav_episodes_table', 3),
(16, '2020_06_22_192100_create_user_flollow_shows_table', 3),
(17, '2020_06_22_212122_create_broadcast_days_table', 3),
(18, '2020_06_22_212340_create_program_broadcast_days_table', 3),
(19, '2020_06_23_090344_create_program_types_table', 4),
(20, '2020_06_23_090434_add_program_type_id', 4),
(21, '2020_06_26_153001_add_like_dislike_actions', 5);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `programs`
--

CREATE TABLE `programs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `broadcast_start_date` date NOT NULL,
  `broadcast_start_time` time NOT NULL,
  `broadcast_end_time` time NOT NULL,
  `on_air` tinyint(1) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `main_image` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `program_type_id` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `programs`
--

INSERT INTO `programs` (`id`, `title`, `description`, `broadcast_start_date`, `broadcast_start_time`, `broadcast_end_time`, `on_air`, `active`, `main_image`, `created_at`, `updated_at`, `program_type_id`) VALUES
(10, 'Caravan - كرفان', 'A paragraph that focuses on fashion and fashion and ways to coordinate clothes', '2020-06-27', '10:00:00', '11:00:00', 1, 1, 'images/imgpsh_fullsize_anim.jpeg', '2020-06-27 07:17:04', '2020-06-27 08:02:07', 1),
(11, 'steve harvey Show', 'Broderick Stephen Harvey is an American comedian, businessman and entertainer. He hosts The Steve Harvey Morning Show, Family Feud, Celebrity Family Feud, the Miss Universe competition and Fox\'s New Year\'s Eve. Harvey began his career as a comedian.', '2020-06-27', '12:00:00', '13:00:00', 1, 1, 'images/steve-harvey-talk-show-cancelled.jpg', '2020-06-27 07:20:45', '2020-06-27 07:20:45', 1),
(12, 'Sham Perfume', 'The work embodies the 1920s in Damascus, when it deals with social life at that point. What happened during the period of the French occupation through interesting stories and tales that combines betrayal, gallantry, love, conflict, good and evil.', '2020-06-27', '20:00:00', '21:00:00', 1, 1, 'images/Gj2LTL1YTalL7dh39Y2TnaxkcAOIfkFB17q91JFr.jpeg', '2020-06-27 07:22:22', '2020-06-27 07:22:22', 2),
(13, 'Game Of Thrones', 'Game of Thrones is an American fantasy drama television series created by David Benioff and D. B. Weiss for HBO. It is an adaptation of A Song of Ice and Fire, George R. R. Martin\'s series of fantasy novels, the first of which is A Game of Thrones. The show was both produced and filmed in Belfast and elsewhere in the United Kingdom. Filming locations also included Canada, Croatia, Iceland, Malta, Morocco, and Spain. The series premiered on HBO in the United States on April 17, 2011, and concluded on May 19, 2019, with 73 episodes broadcast over eight seasons.', '2020-06-27', '22:00:00', '23:00:00', 1, 1, 'images/GameofThrones-_S8E4.jpg', '2020-06-27 07:23:37', '2020-06-27 07:25:27', 2),
(14, 'News', 'News is information about current events. This may be provided through many different media: word of mouth, printing, postal systems, broadcasting, electronic communication, or through the testimony of observers and witnesses to events.', '2020-06-28', '21:00:00', '21:30:00', 1, 1, 'images/d7e90d824635d8d03326bb327dcb7d94.jpeg', '2020-06-27 07:25:10', '2020-06-27 08:35:20', 1),
(15, 'Prison break', 'Prison Break is an American television serial drama created by Paul Scheuring for Fox. The series revolves around two brothers, Lincoln Burrows (Dominic Purcell) and Michael Scofield (Wentworth Miller); Burrows has been sentenced to death for a crime he did not commit, and Scofield devises an elaborate plan to help his brother escape prison and clear his name. The series was produced by Adelstein-Parouse Productions, in association with Original Television and 20th Century Fox Television. Along with creator Paul Scheuring, the series is executive produced by Matt Olmstead, Kevin Hooks, Marty Adelstein, Dawn Parouse, Neal H. Moritz, and Brett Ratner who directed the pilot episode. The series\' theme music, composed by Ramin Djawadi, was nominated for a Primetime Emmy Award in 2006.[1] Prison Break is a joint production between Original Film, Adelstein/Parouse Productions, and 20th Century Fox Television and syndicated by 20th Television.', '2020-06-27', '18:00:00', '19:00:00', 1, 1, 'images/C158flZVQAAzP8y.jpg', '2020-06-27 07:26:47', '2020-06-27 07:26:47', 2),
(16, 'Donya Ya Donya', 'A varied morning social program broadcasted daily on the Ruya satellite channel from Saturday to Thursday, from 7:00 am to 11:00 am', '2020-06-27', '07:00:00', '11:00:00', 1, 1, 'images/banner icon1.png', '2020-06-27 07:28:46', '2020-06-27 07:28:46', 1),
(17, 'Helwa ya donya', 'A morning program, presented by Fouad Al-Karsha and Mays Al-Noubani, broadcast on the Ruya TV channel. It will be screened on Friday at nine in the morning, as it will host many influential personalities locally, Arab and globally, and includes special reports from Jordan and Palestine.', '2020-06-27', '09:00:00', '12:00:00', 1, 1, 'images/Qgvzj4XABPvLFXfUwW4ghQmYKPr9Uw7E45Jp7fEd.jpeg', '2020-06-27 07:30:18', '2020-06-27 08:35:41', 1),
(18, 'Sultana Al Mu\'izz', 'The story revolves around a virtuous person who suffered at the beginning of her life and took responsibility early, and she inherited from her father \"Arabiyeh Al-Kabdah\" and strives to teach her brother to become a famous lawyer, and improves her economic conditions and helps all the people of the neighborhood until a wealthy man comes claiming to do good and a relationship arises between them Then the truth\'s nightmare begins to unfold ...', '2020-06-27', '17:00:00', '18:00:00', 1, 1, 'images/8FRESAnCN76shAO3r9vnFS6vLGxU6xiwnMcGL6Vd.jpeg', '2020-06-27 07:33:10', '2020-06-27 07:33:10', 2),
(19, 'Noman Family', 'It is an interesting story that shows the effect of money and the souls of people when money controls their minds and their conscience dies. The events of the first part take place in the late seventies of the twentieth century, where we find the family of Haj Numan, a furniture dealer who has a single son (Khaled) who suffers from schizophrenia. Haj Numan seeks \"Salah Abdullah\" To make his son lead a normal life and marry him from his cousin to complete the divorce at a later stage, then he marries Radhia, \"Sahr Al-Sayegh\", who has a great role in stabilizing the psychological state.', '2020-06-27', '14:00:00', '15:00:00', 1, 1, 'images/5MVA0LFTN2vOmRfbjyE9WQuDcqNibzVOcTHRgD8v.jpeg', '2020-06-27 07:51:52', '2020-06-27 08:37:50', 2);

-- --------------------------------------------------------

--
-- Table structure for table `program_broadcast_days`
--

CREATE TABLE `program_broadcast_days` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `program_id` int(11) NOT NULL,
  `day_id` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `program_broadcast_days`
--

INSERT INTO `program_broadcast_days` (`id`, `program_id`, `day_id`, `created_at`, `updated_at`) VALUES
(1, 10, 1, NULL, NULL),
(2, 10, 2, NULL, NULL),
(3, 10, 3, NULL, NULL),
(4, 10, 4, NULL, NULL),
(5, 10, 5, NULL, NULL),
(6, 10, 6, NULL, NULL),
(7, 10, 7, NULL, NULL),
(8, 11, 1, NULL, NULL),
(9, 11, 2, NULL, NULL),
(10, 11, 3, NULL, NULL),
(11, 11, 4, NULL, NULL),
(12, 11, 5, NULL, NULL),
(13, 11, 6, NULL, NULL),
(14, 11, 7, NULL, NULL),
(15, 12, 1, NULL, NULL),
(16, 12, 2, NULL, NULL),
(17, 12, 3, NULL, NULL),
(18, 12, 5, NULL, NULL),
(19, 13, 5, NULL, NULL),
(20, 14, 1, NULL, NULL),
(21, 14, 2, NULL, NULL),
(22, 14, 3, NULL, NULL),
(23, 14, 4, NULL, NULL),
(24, 14, 5, NULL, NULL),
(25, 14, 6, NULL, NULL),
(26, 14, 7, NULL, NULL),
(27, 15, 4, NULL, NULL),
(28, 16, 1, NULL, NULL),
(29, 16, 2, NULL, NULL),
(30, 16, 3, NULL, NULL),
(31, 16, 4, NULL, NULL),
(32, 16, 5, NULL, NULL),
(33, 16, 7, NULL, NULL),
(34, 17, 6, NULL, NULL),
(35, 18, 1, NULL, NULL),
(36, 18, 2, NULL, NULL),
(37, 18, 3, NULL, NULL),
(38, 18, 4, NULL, NULL),
(39, 18, 5, NULL, NULL),
(40, 19, 1, NULL, NULL),
(41, 19, 2, NULL, NULL),
(42, 19, 3, NULL, NULL),
(43, 19, 4, NULL, NULL),
(44, 19, 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `program_episodes`
--

CREATE TABLE `program_episodes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `program_id` int(11) NOT NULL,
  `broadcast_day_id` int(11) NOT NULL,
  `broadcast_date` date NOT NULL,
  `episode_no` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumbnail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `video` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `program_episodes`
--

INSERT INTO `program_episodes` (`id`, `program_id`, `broadcast_day_id`, `broadcast_date`, `episode_no`, `title`, `description`, `duration`, `thumbnail`, `video`, `created_at`, `updated_at`) VALUES
(9, 15, 4, '2020-07-01', 1, 'Prison break', 'Prison break', '00:04:02', 'images/download (1).jpeg', 'files/PRISON BREAK 6 _ FINAL SEASON 2020 _ TRAILER-tcwJrzT7i7A.mp4', '2020-06-27 07:46:47', '2020-06-27 07:46:48'),
(10, 10, 1, '2020-06-28', 1, 'First Episode', 'جمعة شبابية، راح ياخدكم فيها كرفان في لفة سريعة بتتوقف على كل \"ما يهمنا\" إحنا الشباب من أخبار مواقع التواصل الإجتماعي، للمصاريف وهمها، للأكل وصحاب المزاج ...', '00:07:35', 'images/e65a4a6dec227e31c28f85553376fdf9.jpeg', 'files/كيف انتشرت رغدة كيومجيان على اليوتيوب بشهرين فقط! - ضيف الخميس - كرفان-yJJaYQiVSGE.mp4', '2020-06-27 08:00:30', '2020-06-27 08:02:18'),
(11, 11, 1, '2020-07-05', 1, 'How Steve Harvey\'s Grandkids Convinced Him to Buy an $8,500 Teepee', 'Ellen\'s friend Steve Harvey told a hilarious story of how he ended up with an $8,500, 16-foot teepee in his backyard.', '00:06:18', 'images/2b66c6d1ae672d7ac18b6275062909ee.jpg', 'files/How Steve Harvey\'s Grandkids Convinced Him to Buy an $8,500 Teepee-j5SKmUoL9Tg.mp4', '2020-06-27 08:17:21', '2020-06-27 08:17:21'),
(12, 12, 3, '2020-07-07', 1, 'القصة كاملة', 'المشهد الاسطوري من عطر الشام ابو عامر عرف حقيقة مرتو الخاينة مع شغيلو والزرع مو زرعو!! القصة كاملة', '00:22:42', 'images/1a2f5d60be84b7e268d749fbc21274a7.jpeg', 'files/المشهد الاسطوري من عطر الشام ابو عامر عرف حقيقة مرتو الخاينة مع شغيلو والزرع مو زرعو!! القصة كاملة-GDl8kjMvI9E.mp4', '2020-06-27 08:20:18', '2020-06-27 08:20:19'),
(13, 13, 5, '2020-07-02', 1, 'Game of Thrones - Main Theme (Extended) HD', 'Game of Thrones - Main Theme (Extended) HD', '00:04:15', 'images/d4845f6e5292d9ed2a9a164c4d12f346.jpg', 'files/Game of Thrones - Main Theme (Extended) HD-AdQ3JDLlmPI.mp4', '2020-06-27 08:26:51', '2020-06-27 08:26:52'),
(14, 14, 1, '2020-07-05', 1, 'القردة تغزو مدينة ليبوري والسلطات تحاول السيطرة عليها | 26-06-2020', 'القردة تغزو مدينة ليبوري والسلطات تحاول السيطرة عليها | 26-06-2020', '00:01:48', 'images/Y6dCWfwWr6hpHYNCEDfg1eaPiOwSTNj4Uks7J8AP.jpeg', 'files/القردة تغزو مدينة ليبوري والسلطات تحاول السيطرة عليها _ 26-06-2020-TJ1f8FkwKms.mp4', '2020-06-27 08:28:29', '2020-06-27 08:35:02'),
(15, 16, 5, '2020-07-09', 1, 'النشرة الجوية 27/6/2020', 'النشرة الجوية 27/6/2020', '00:02:13', 'images/b97ae23ab8b46d99ac6e7797eb9b6f37.png', 'files/النشرة الجوية  27_6_2020-fHo2LaMehTk.mp4', '2020-06-27 08:30:07', '2020-06-27 08:30:08'),
(16, 17, 6, '2020-07-10', 1, 'شاشة The Frame - فادي أبو هولة | حلوة يا دنيا', 'شاشة The Frame - فادي أبو هولة | حلوة يا دنيا', '00:07:49', 'images/1e6cd27fe0476be75793e3a14203ded0.jpeg', 'files/شاشة The Frame - فادي أبو هولة _ حلوة يا دنيا-ko5khir5iu0.mp4', '2020-06-27 08:31:48', '2020-06-27 08:36:12'),
(17, 18, 3, '2020-06-30', 1, 'Episode 01 - Sultanat Al Moez Series | ملخص الحلقة الأولي - مسلسل سلطانة المعز', 'Episode 01 - Sultanat Al Moez Series | ملخص الحلقة الأولي - مسلسل سلطانة المعز', '00:08:59', 'images/9fcb3c6b91a2a521b484f8ca302eeb5b.jpeg', 'files/Episode 01 - Sultanat Al Moez Series _ ملخص الحلقة الأولي - مسلسل سلطانة المعز-tK8kOgX9OXQ.mp4', '2020-06-27 08:33:18', '2020-06-27 08:33:18'),
(18, 19, 1, '2020-07-05', 1, 'مواجهة قوية بين هنا ووالدها الشيخ مخلوف', 'مواجهة قوية بين هنا ووالدها الشيخ مخلوف', '00:00:57', 'images/e75d44ef7131ac197ea3d47b73f0ea79.jpeg', 'files/مواجهة قوية بين هنا ووالدها الشيخ مخلوف-GxCuu1k5uDk.mp4', '2020-06-27 08:34:16', '2020-06-27 08:34:16');

-- --------------------------------------------------------

--
-- Table structure for table `program_types`
--

CREATE TABLE `program_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `programs_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `program_types`
--

INSERT INTO `program_types` (`id`, `programs_type`, `created_at`, `updated_at`) VALUES
(1, 'TV show', '2020-06-23 06:11:26', '2020-06-23 06:11:26'),
(2, 'Series', '2020-06-23 06:11:26', '2020-06-23 06:11:26');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_image` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `user_image`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'ahmad', 'ahmad.ghuneim@hotmail.com', NULL, '$2y$10$QDuvjklj4mm6SILUKF14MOIQ0bjFBto3yQ.EVjsDwmjqgXN4fVEXK', 'images/1dbd5c84eebfd37287f4bd2086219e941593254269052293800509.jpg', NULL, '2020-06-27 07:37:49', '2020-06-27 07:37:49');

-- --------------------------------------------------------

--
-- Table structure for table `user_fav_episodes`
--

CREATE TABLE `user_fav_episodes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `episode_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `like_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0:dislike 1:like'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_fav_episodes`
--

INSERT INTO `user_fav_episodes` (`id`, `user_id`, `episode_id`, `created_at`, `updated_at`, `like_status`) VALUES
(1, 1, 9, '2020-06-27 07:47:03', '2020-06-27 08:13:37', 0),
(2, 1, 10, '2020-06-27 08:00:46', '2020-06-27 08:02:29', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_follow_shows`
--

CREATE TABLE `user_follow_shows` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `program_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_follow_shows`
--

INSERT INTO `user_follow_shows` (`id`, `user_id`, `program_id`, `created_at`, `updated_at`) VALUES
(1, 1, 15, '2020-06-27 07:37:56', '2020-06-27 07:37:56'),
(2, 1, 11, '2020-06-27 07:38:02', '2020-06-27 07:38:02'),
(3, 1, 13, '2020-06-27 07:38:05', '2020-06-27 07:38:05'),
(4, 1, 12, '2020-06-27 07:38:06', '2020-06-27 07:38:06'),
(5, 1, 10, '2020-06-27 07:38:08', '2020-06-27 07:38:08'),
(7, 1, 16, '2020-06-27 08:10:48', '2020-06-27 08:10:48');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_menu`
--
ALTER TABLE `admin_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_operation_log`
--
ALTER TABLE `admin_operation_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_operation_log_user_id_index` (`user_id`);

--
-- Indexes for table `admin_permissions`
--
ALTER TABLE `admin_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_permissions_name_unique` (`name`),
  ADD UNIQUE KEY `admin_permissions_slug_unique` (`slug`);

--
-- Indexes for table `admin_roles`
--
ALTER TABLE `admin_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_roles_name_unique` (`name`),
  ADD UNIQUE KEY `admin_roles_slug_unique` (`slug`);

--
-- Indexes for table `admin_role_menu`
--
ALTER TABLE `admin_role_menu`
  ADD KEY `admin_role_menu_role_id_menu_id_index` (`role_id`,`menu_id`);

--
-- Indexes for table `admin_role_permissions`
--
ALTER TABLE `admin_role_permissions`
  ADD KEY `admin_role_permissions_role_id_permission_id_index` (`role_id`,`permission_id`);

--
-- Indexes for table `admin_role_users`
--
ALTER TABLE `admin_role_users`
  ADD KEY `admin_role_users_role_id_user_id_index` (`role_id`,`user_id`);

--
-- Indexes for table `admin_users`
--
ALTER TABLE `admin_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_users_username_unique` (`username`);

--
-- Indexes for table `admin_user_permissions`
--
ALTER TABLE `admin_user_permissions`
  ADD KEY `admin_user_permissions_user_id_permission_id_index` (`user_id`,`permission_id`);

--
-- Indexes for table `broadcast_days`
--
ALTER TABLE `broadcast_days`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `programs`
--
ALTER TABLE `programs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `program_broadcast_days`
--
ALTER TABLE `program_broadcast_days`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `program_episodes`
--
ALTER TABLE `program_episodes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `program_types`
--
ALTER TABLE `program_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_fav_episodes`
--
ALTER TABLE `user_fav_episodes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_follow_shows`
--
ALTER TABLE `user_follow_shows`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_menu`
--
ALTER TABLE `admin_menu`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `admin_operation_log`
--
ALTER TABLE `admin_operation_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1413;
--
-- AUTO_INCREMENT for table `admin_permissions`
--
ALTER TABLE `admin_permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `admin_roles`
--
ALTER TABLE `admin_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `admin_users`
--
ALTER TABLE `admin_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `broadcast_days`
--
ALTER TABLE `broadcast_days`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `programs`
--
ALTER TABLE `programs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `program_broadcast_days`
--
ALTER TABLE `program_broadcast_days`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `program_episodes`
--
ALTER TABLE `program_episodes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `program_types`
--
ALTER TABLE `program_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_fav_episodes`
--
ALTER TABLE `user_fav_episodes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user_follow_shows`
--
ALTER TABLE `user_follow_shows`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
